<?php
namespace Kodes\Www;

// ini_set('display_errors', 1);

/**
 * Member 클래스
 * 
 * @author  Kodes <kodesinfo@gmail.com>
 * @version 1.0
 * @license 해당 프로그램은 kodes에서 제작된 프로그램으로 저작원은 코드스(https://www.kode.co.kr)
 */
class Member
{
	/** const */
	const MEMBER_COLLECTION = 'member';
	// const KEY_PASS = '@KODES_2022';

	/** @var Class Common Class */
	protected $common;
	protected $json;
	protected $db;
	protected $log;

	/** @var variable */
	protected $coId;
	protected $ip;
	protected $company;
    protected $domain;

    var $member = [];

	/**
	 * 생성자
	 */
    public function __construct()
	{
		// class
		$this->common = new Common();
		$this->json = new Json();
		$this->db = new DB();
		$this->log = new Log();

		// variable
        $this->coId = $this->common->coId;
		$this->ip = $this->common->getRemoteAddr();
		$this->siteDocPath = $this->common->config['path']['data'].'/'.$this->coId;
		$this->company = $this->common->getCompany();
		$this->domain = empty($this->company['domain']['pc'])?'':$this->company['domain']['pc'];

		// 접속한 도메인으로 설정
		if (!empty($_SERVER['HTTP_HOST'])) {
			if (!empty($_SERVER['HTTP_X_FORWARDED_PROTO'])) {
				$this->domain = $_SERVER['HTTP_X_FORWARDED_PROTO'].'://'.$_SERVER['HTTP_HOST'];
			} else {
				$this->domain = $_SERVER['REQUEST_SCHEME'].'://'.$_SERVER['HTTP_HOST'];
			}
		}
    }

    /**
     * 비밀번호 암호화
     * SHA-512
     * 
     * @param String password
     * @param String salt
     * @return String 암호화된 문자열
     */
	protected function encryptPassword($password, $salt='')
	{
		return base64_encode(hash('sha512', $password.$salt, true));
	}

	/**
     * salt 생성
     */
    protected function getSalt()
    {
        return base64_encode(random_bytes(50));
    }

    /**
     * 암호화
     */
    // protected function encryptText($plainText)
    // {
    //     return base64_encode(openssl_encrypt($plainText, 'aes-256-cbc', KEY_PASS, OPENSSL_RAW_DATA, str_repeat(chr(0), 16)));
    // }

    /**
     * 복호화
     */
    // protected function decryptText($encrypted)
    // {
    //     return openssl_decrypt(base64_decode($encrypted), 'aes-256-cbc', $password, OPENSSL_RAW_DATA, str_repeat(chr(0), 16));
    // }

	/**
	 * 회원가입 step1
	 */
	public function join1()
	{
		$return = [];
		try {
			if ($_SESSION['memberId']) {
				header("Location: /");
			}

			// 이용약관
			// $content = file_get_contents("/webData/".$this->coId."/event/service.json");
			// $return['service'] = json_decode($content,true);
			// 개인정보취급방침
			// $content = file_get_contents("/webData/".$this->coId."/event/privacy.json");
			// $return['privacy'] = json_decode($content,true);
			// 제3자 정보제공동의
			// $content = file_get_contents("/webData/".$this->coId."/event/Provide.json");
			// $return['privacy'] = json_decode($content,true);

			// 소셜 api url
			$return['naverApiURL'] = $this->getNaverApiURL('join');
			$return['kakaoApiURL'] = $this->getKakaoApiURL('join');
			$return['googleApiURL'] = $this->getGoogleApiURL('join');
			// 소셜 api url : 간편가입
			// $return['naverApiURL'] = $this->getNaverApiURL('join', 'simpleJoin');
			// $return['kakaoApiURL'] = $this->getKakaoApiURL('join', 'simpleJoin');
			// $return['googleApiURL'] = $this->getGoogleApiURL('join', 'simpleJoin');

			// 가입처리용 세션 초기화
			$this->unsetSessionJoin();

        } catch(\Exception $e) {
            $return['msg'] = $this->common->getExceptionMessage($e);
        }
        
        return $return;
	}

	/**
	 * 회원가입 step2
	 */
	public function join2()
	{
		$return = [];
		try {
			if ($_SESSION['memberId']) {
				header("Location: /");
			}

			// 이미 가입한 정보인지 체크
			if ($_SESSION['joinProvider']) {
				$filter['provider'] = $_SESSION['joinProvider'];
				$filter['socialId'] = $_SESSION['joinSocialId'];
				if ($this->db->count(self::MEMBER_COLLECTION, $filter) > 0) {
					echo "<script>";
					echo "alert('이미 가입한 회원입니다.');";
					echo "location.replace('/login');";
					echo "</script>";
					exit;
				}
			}

			// 표출 정보
			if ($_SESSION['joinProvider'] == 'NAVER') {
				$return['joinProviderText'] = '네이버';
			} elseif ($_SESSION['joinProvider'] == 'KAKAO') {
				$return['joinProviderText'] = '카카오';
			} elseif ($_SESSION['joinProvider'] == 'GOOGLE') {
				$return['joinProviderText'] = '구글';
			}
		} catch(\Exception $e) {
			$return['msg'] = $this->common->getExceptionMessage($e);
		}
		
		return $return;
	}

	/**
	 * 아이디 중복체크 ajax
	 */
	public function validateId()
	{
		$return = [];
        try {
            // requestMethod 체크
            $this->common->checkRequestMethod('POST');

			$filter['coId'] = $_SESSION['coId'];
			$filter['id'] = $_POST['id'];
			$return['count'] = $this->db->count(self::MEMBER_COLLECTION, $filter);

		} catch(\Exception $e) {
			$return['msg'] = $this->common->getExceptionMessage($e);
		}

		die(json_encode(['result'=>$return]));
		// return $return;
	}

	/**
	 * 이메일 중복체크 ajax
	 */
	public function validateEmail()
	{
		$return = [];
        try {
            // requestMethod 체크
            $this->common->checkRequestMethod('POST');

			$filter['coId'] = $_SESSION['coId'];
			if ($_SESSION['memberId']) $filter['id'] = ['$ne' => $_SESSION['memberId']];
			$filter['email'] = $_POST['email'];
			$return['count'] = $this->db->count(self::MEMBER_COLLECTION, $filter);

		} catch(\Exception $e) {
			$return['msg'] = $this->common->getExceptionMessage($e);
		}

		die(json_encode(['result'=>$return]));
		// return $return;
	}

	/**
	 * 휴대폰 중복체크 ajax
	 */
	public function validatePhone()
	{
		$return = [];
        try {
            // requestMethod 체크
            $this->common->checkRequestMethod('POST');

			$filter['coId'] = $_SESSION['coId'];
			if ($_SESSION['memberId']) $filter['id'] = ['$ne' => $_SESSION['memberId']];
			$filter['phone'] = $_POST['phone'];
			$return['count'] = $this->db->count(self::MEMBER_COLLECTION, $filter);

		} catch(\Exception $e) {
			$return['msg'] = $this->common->getExceptionMessage($e);
		}

		die(json_encode(['result'=>$return]));
		// return $return;
	}

	/**
	 * 닉네임 중복체크 ajax
	 */
	public function validateNickname()
	{
		$return = [];
        try {
            // requestMethod 체크
            $this->common->checkRequestMethod('POST');

			$filter['coId'] = $_SESSION['coId'];
			if (!empty($_SESSION['memberId'])) $filter['id'] = ['$ne' => $_SESSION['memberId']];
			$filter['nickname'] = $_POST['nickname'];
			$return['count'] = $this->db->count(self::MEMBER_COLLECTION, $filter);

		} catch(\Exception $e) {
			$return['msg'] = $this->common->getExceptionMessage($e);
		}

		die(json_encode(['result'=>$return]));
		// return $return;
	}

	/**
	 * 회원가입 처리 ajax
	 */
    public function joinProc()
	{
		$return = [];
        try {
			if (!empty($_SESSION['memberId'])) {
				throw new \Exception("유효하지 않은 요청입니다.", 400);
			}
			
			if (count($this->member) == 0) {
				// requestMethod 체크
				$this->common->checkRequestMethod('POST');
	
				$this->member = $_POST;
			}
			$this->member['coId'] = $this->coId;

			if (empty($this->member['id'])) {
                throw new \Exception("ID를 입력하세요.", 400);
            }

			if (empty($this->member['name'])) {
                throw new \Exception("이름을 입력하세요.", 400);
            }

			if ($this->coId != "kpf" && empty($this->member['email'])) {
                throw new \Exception("이메일 주소를 입력하세요.", 400);
            }

			$filter = ['id' => $this->member['id']];
			if ($this->db->count(self::MEMBER_COLLECTION, $filter) > 0) {
				if (empty($this->member['socialId'])) {
					throw new \Exception("이미 사용중인 ID 입니다.", 400);
				} else {
					throw new \Exception("이미 가입된 계정입니다.", 400);
				}
			}

			$filter = ['email' => $this->member['email']];
			if ($this->coId != "kpf" && $this->db->count(self::MEMBER_COLLECTION, $filter) > 0) {
				throw new \Exception("이미 사용중인 이메일 주소 입니다.", 400);
			}

			if ($this->member['password']) {
				$this->member['salt'] = $this->getSalt();
				$this->member['password'] = $this->encryptPassword($this->member['password'], $this->member['salt']);
			}
			if ($this->member['birthday']) $this->member['birthday'] = date('Y-m-d',strtotime($this->member['birthday']));
			$this->member['status'] = 'normal';
			$this->member["joinDate"] = date('Y-m-d H:i:s');
			$this->member['joinIp'] = $this->ip;

			// agree
			$this->member['argeeService'] = $this->member['argeeService'] == 'Y'?true:false;
			$this->member['argeePrivacy'] = $this->member['argeePrivacy'] == 'Y'?true:false;
			$this->member['argeeThird'] = $this->member['argeeThird'] == 'Y'?true:false;
			$this->member['agreeLetter'] = $this->member['agreeLetter'] == 'Y'?true:false;

			$result = $this->db->insert(self::MEMBER_COLLECTION, $this->member);
			if ($result->getInsertedCount() == 1) {
				$this->recordHistory("회원가입");
				// $this->sessionLogin();	// 가입하면서 자동로그인 할 경우
				$return['result'] = 'success';
			}

			$return['msg'] = "회원 가입이 완료되었습니다.";
		} catch(\Exception $e) {
			$return['msg'] = $this->common->getExceptionMessage($e);
		}

		die(json_encode(['result'=>$return]));
		// return $return;
    }

	/**
	 * 로그인 회원정보 조회
	 */
	public function getMember()
	{
		if (empty($_SESSION['coId']) || empty($_SESSION['memberId'])) {
			return null;
		}

		$filter['coId'] = $_SESSION['coId'];
		$filter['id'] = $_SESSION['memberId'];
		$options = ["projection" => ['_id'=>0, 'history'=>0]];
		$item = $this->db->item(self::MEMBER_COLLECTION, $filter, $options);

		return $item;
	}

	/**
	 * 회원정보 수정 화면
	 */
	public function modify()
	{
		$result = [];
        try {
			if (empty($_SESSION['memberId'])) {
				header("Location: /login");
			}

			$result['item'] = $this->getMember();

		} catch(\Exception $e) {
			$result['msg'] = $this->common->getExceptionMessage($e);
		}

		return $result;
	}

	/**
	 * 회원정보 수정 처리
	 */
    public function modifyProc()
	{
		$return = [];
        try {
            // requestMethod 체크
            $this->common->checkRequestMethod('POST');

			// 필수 체크
			if (empty($_SESSION['coId']) || empty($_SESSION['memberId'])) {
				throw new \Exception("유효하지 않은 접근입니다.", 400);
			}

			$this->member = $_POST;
			// 수정 불가 필드
			unset($this->member['coId']);
			unset($this->member['provider']);
			unset($this->member['socialId']);
			unset($this->member['id']);
			unset($this->member['name']);
			// unset($this->member['birthday']);
			// unset($this->member['gender']);

			// password 처리
			if (!empty($this->member['currentPassword'])) {
				if (empty($this->member['password'])) {
					unset($this->member['password']);
				} else {
					$this->member['salt'] = $this->getSalt();
					$this->member['password'] = $this->encryptPassword($this->member['password'], $this->member['salt']);
				}
			}

			// agree
			$this->member['agreeLetter'] = $this->member['agreeLetter'] == 'Y'?true:false;
			
			$this->member['modifyDate'] = date('Y-m-d H:i:s');
			$this->member['modifyIp'] = $this->ip;

			$filter['coId'] = $_SESSION['coId'];
            $filter['id'] = $_SESSION['memberId'];
            $option = ['$set'=>$this->member];
			$result = $this->db->update(self::MEMBER_COLLECTION, $filter, $option);
			if ($result->getModifiedCount() == "1") {
				$this->recordHistory("회원정보수정");
			}

			$return['msg'] = "회원정보가 수정되었습니다.";
		} catch(\Exception $e) {
			$return['msg'] = $this->common->getExceptionMessage($e);
		}

		die(json_encode(['result'=>$return]));
		// return $return;
    }

	/**
	 * 탈퇴 처리
	 */
	public function withdrawProc()
	{
		$filter['coId'] = $this->coId;
		$filter['id'] = $_POST['id'];
		$filter['password'] = $this->encryptPassword($_POST['password']);

		$res = $this->db->count(self::MEMBER_COLLECTION, $filter);
	
		$cnt = 0;
		if ($res > 0) {
			$this->member['status'] = 'withdraw';
			$this->member['withdrawDate'] = date('Y-m-d H:i:s');

			$result = $this->db->update(self::MEMBER_COLLECTION, ['id'=>$_POST["id"],['password'] => $this->encryptPassword($_POST['password'])], ['$set'=>$this->member]);
			
			$cnt = $result->getModifiedCount();
		}

		if ($cnt > 0) {
			session_destroy();
		}

		echo $cnt;
	}

	/**
	 * 로그인 체크
	 */
	public function loginCheck()
	{
		$return = [];
        try {
			// requestMethod 체크
            $this->common->checkRequestMethod('POST');

			if (!empty($_SESSION['memberId'])) {
				throw new \Exception("유효하지 않은 요청입니다.", 400);
			}

			if (empty($_POST['id']) || empty($_POST['password'])) {
				throw new \Exception("ID 또는 패스워드를 입력하세요.", 400);
			}

			$filter['coId'] = $this->coId;
			$filter['id'] = $_POST['id'];
			$filter['password'] = $_POST['password'];

			if ($this->loginProc($filter)) {
				$return['result'] = 'success';
				// ID 기억하기
				if ($_POST['rememberId'] == 'Y') {
					setcookie('rememberId', $_POST['id'], time() + (86400 * 30), "/"); // 86400 = 1 day
				} else {
					setcookie('rememberId', '', time() - 3600, "/");
				}
			}

        } catch(\Exception $e) {
			$return['msg'] = $this->common->getExceptionMessage($e);
		}

		die(json_encode(['result'=>$return]));
        // return $result;
	}

	/**
	 * 로그인 처리
	 */
	protected function loginProc($filter, $provider='', $option='')
	{
		// 필수값
		if (empty($filter)) {
			throw new \Exception("로그인 실패하였습니다.(유효하지 않은 요청)", 400);
		}
		if (empty($filter['coId'])) {
			throw new \Exception("로그인 실패하였습니다.(회사코드가 없음)", 400);
		}
		if ((empty($filter['id']) || empty($filter['password'])) && (empty($filter['provider']) || empty($filter['socialId']))) {
			throw new \Exception("로그인 실패하였습니다.(유효하지 않은 요청)", 400);
		}
		$password = empty($filter['password'])?null:$filter['password'];
		if (!empty($password)) {
			unset($filter['password']);
		}

		// 회원 조회
		$options = ["projection" => ['_id'=>0, 'history'=>0]];
		$this->member = $this->db->item(self::MEMBER_COLLECTION, $filter, $options);
		if (empty($this->member)) {
			if (empty($provider)) {
				throw new \Exception("ID 또는 패스워드를 확인하세요.", 400);
			} else {
				if ($option == 'withoutJoin') {
					return false;
				} else {
					throw new \Exception("가입되지 않은 계정입니다.", 400);
				}
			}
		}
		// password 확인
		if (!empty($password)) {
			$password = $this->encryptPassword($password, $this->member['salt']);
			if ($password != $this->member['password']) {
				throw new \Exception("ID 또는 패스워드를 확인하세요.", 400);
			}
		}
		if (empty($provider) && $this->member['provider']) {
			throw new \Exception("로그인 실패하였습니다.\n ".$this->member['provider']." 계정으로 가입된 회원입니다.", 400);
		}

		// status
		if ($this->member['status'] == 'sleep') {
			throw new \Exception("휴면 상태의 계정입니다.", 400);
		} elseif ($this->member['status'] == 'block') {
			throw new \Exception("이용정지 상태의 계정입니다.", 400);
		} elseif ($this->member['status'] == 'withdraw') {
			throw new \Exception("탈퇴 상태의 계정입니다.", 400);
		}

		// 로그인
		$this->sessionLogin();
		$this->recordLogin();

		return true;
	}

	/**
	 * 히스토리 저장
	 */
	protected function recordHistory($job)
	{
		$filter['coId'] = $this->coId;
        $filter['id'] = $this->member['id'];
		$options = ['$push'=>["history"=>['date'=>date('Y-m-d H:i:s'), "ip"=>$this->ip, "job"=>$job]]];
		$result = $this->db->update(self::MEMBER_COLLECTION, $filter, $options);
	}

	/**
	 * 로그인 기록
	 */
	protected function recordLogin()
	{
		$filter['coId'] = $this->coId;
        $filter['id'] = $this->member['id'];
		$options = ['$set'=>['latestLoginDate'=>date('Y-m-d H:i:s'), "latestLoginIp"=>$this->ip]];
		$result = $this->db->update(self::MEMBER_COLLECTION, $filter, $options);
		$this->recordHistory("로그인");
	}

	/**
	 * 로그인 처리
	 */
	protected function sessionLogin()
	{
		$_SESSION["coId"] = $this->member['coId'];
		$_SESSION["memberId"] = $this->member['id'];
		$_SESSION["memberName"] = $this->member['name'];
		$_SESSION["provider"] = $this->member['provider'];
		$_SESSION["socialId"] = $this->member['socialId'];
		$_SESSION["nickname"] = $this->member['nickname'];
		$_SESSION["profileImage"] = $this->member['profileImage'];
		// $_SESSION["phone"] = $this->member['phone'];
		// $_SESSION["gender"] = $this->member['gender'];
		// $_SESSION["birthday"] = $this->member['birthday'];
	}

	/**
	 * 로그아웃
	 */
	public function logout()
	{
		session_unset();
		session_destroy();
		echo "<script>location.href=document.referrer;</script>";

	}

	/**
	 * ID 찾기
	 */
	public function findId()
	{
		$return = [];
        try {
			// requestMethod 체크
            $this->common->checkRequestMethod('POST');

			if (!empty($_SESSION['memberId'])) {
				throw new \Exception("유효하지 않은 요청입니다.", 400);
			}

			if (empty($_POST['name']) || empty($_POST['email'])) {
				throw new \Exception("이름 또는 이메일을 입력하세요.", 400);
			}

			$filter['coId'] = $this->coId;
			$filter['name'] = $_POST['name'];
			$filter['email'] = $_POST['email'];
			$options = ["projection" => ['_id'=>0, 'history'=>0]];
			$this->member = $this->db->item(self::MEMBER_COLLECTION, $filter, $options);

			if (empty($this->member['id'])) {
				throw new \Exception("입력하신 정보와 일치하는 회원이 없습니다.", 400);
			}

			$return['id'] = $this->member['id'];

		} catch(\Exception $e) {
			$return['msg'] = $this->common->getExceptionMessage($e);
		}

		die(json_encode(['result'=>$return]));
		// return $result;
	}

	/**
	 * 비밀번호 찾기 이메일 전송
	 */
	public function findPassword()
	{
		$return = [];
        try {
			// requestMethod 체크
            $this->common->checkRequestMethod('POST');

			if (!empty($_SESSION['memberId'])) {
				throw new \Exception("유효하지 않은 요청입니다.", 400);
			}

			if (empty($_POST['id']) || empty($_POST['email'])) {
				throw new \Exception("ID 또는 이메일을 입력하세요.", 400);
			}

			$filter['coId'] = $this->coId;
			$filter['id'] = $_POST['id'];
			$filter['email'] = $_POST['email'];
			$options = ["projection" => ['_id'=>0, 'history'=>0]];
			$member = $this->db->item(self::MEMBER_COLLECTION, $filter, $options);

			if (empty($member['id'])) {
				throw new \Exception("입력하신 정보와 일치하는 회원이 없습니다.", 400);
			}

			// 임시 비밀번호 생성
			$change_password = $this->generateRandomString(8);

			// 인증메일 전송
			$emailContent = $this->getCertEmailContent($member, $change_password);
			// print_r($emailContent);
			$sendMailData['sender'] = $this->company['manager']['email'];
			$sendMailData['senderName'] = $this->company['name'];
			$sendMailData['recipient'] = $member['email'];
			$sendMailData['subject'] = $emailContent['subject'];
			$sendMailData['bodyHtml'] = $emailContent['bodyHtml'];
			$sendMailData['bodyText'] = strip_tags($sendMailData['bodyHtml']);
			// print_r($sendMailData);
			$email = new Email();
			$result = $email->sendPHPMailer($sendMailData, $this->company);

			if ($result == 'success') {
				$filter['coId'] = $this->coId;
				$filter['id'] = $_POST['id'];
				$filter['email'] = $_POST['email'];
				$member['salt'] = $this->getSalt();
                $member['password'] = $this->encryptPassword($change_password, $member['salt']);
				$options = ['$set'=>$member];
				$result = $this->db->update(self::MEMBER_COLLECTION, $filter, $options);
				// $return['result'] = $result;
				$return['msg'] = "입력하신 이메일로 임시비밀번호가 발송되었습니다.\n메일을 확인 후 로그인하세요.";
			} else {
				throw new \Exception($result, 400);
			}

		} catch(\Exception $e) {
			$return['msg'] = $this->common->getExceptionMessage($e);
		}

		die(json_encode(['result'=>$return]));
		// print_r($this->member);
	}

	/**
	 * 이메일인증 메일내용
	 */
	protected function getCertEmailContent($member, $change_password)
	{
		$companyName = $this->company['name'];
		$domain = $this->domain;
		$login = $domain.'/login';

		$emailContent['subject'] = "[".$companyName."] 요청하신 회원정보 찾기 안내 메일입니다.";
		$content = "";
		$content .= '<div style="margin:30px auto;width:600px;border:10px solid #f7f7f7">'.PHP_EOL;
		$content .= '<div style="border:1px solid #dedede">'.PHP_EOL;
		$content .= '<h1 style="padding:30px 30px 0;background:#f7f7f7;color:#555;font-size:1.4em">'.PHP_EOL;
		$content .= '회원정보 찾기 안내'.PHP_EOL;
		$content .= '</h1>'.PHP_EOL;
		$content .= '<span style="display:block;padding:10px 30px 30px;background:#f7f7f7;text-align:right">'.PHP_EOL;
		$content .= '<a href="'.$domain.'" target="_blank">'.$companyName.'</a>'.PHP_EOL;
		$content .= '</span>'.PHP_EOL;
		$content .= '<p style="margin:20px 0 0;padding:30px 30px 30px;border-bottom:1px solid #eee;line-height:1.7em">'.PHP_EOL;
		$content .= addslashes($member['name'])." 회원님은 ".date('Y-m-d H:i:s')."에 임시 비밀번호 발급 요청을 하셨습니다.<br>".PHP_EOL;
		$content .= '<span style="color:#ff3061">임시 비밀번호를 확인하신 후, <a href="'.$login.'" target="_blank">'.$companyName.' 로그인</a>에서 로그인 하세요.</span><br>'.PHP_EOL;
		$content .= '로그인 후, 회원정보수정 메뉴에서 비밀번호를 변경해주시기 바랍니다.<br>'.PHP_EOL;
		$content .= '보낸메일주소는 발신 전용으로 회신에 답변이 불가능합니다.'.PHP_EOL;
		$content .= '</p>'.PHP_EOL;
		$content .= '<p style="margin:0;padding:30px 30px 30px;border-bottom:1px solid #eee;line-height:1.7em">'.PHP_EOL;
		$content .= '<span style="display:inline-block;width:100px">임시 비밀번호</span> <strong style="color:#ff3061">'.$change_password.'</strong>'.PHP_EOL;
		$content .= '</p>'.PHP_EOL;
		// $content .= '<a href="'.$login.'" target="_blank" style="display:block;padding:30px 0;background:#484848;color:#fff;text-decoration:none;text-align:center">'.$companyName.' 로그인</a>'.PHP_EOL;
		$content .= '</div>'.PHP_EOL;
		$content .= '</div>'.PHP_EOL;
		$emailContent['bodyHtml'] = $content;

		return $emailContent;
	}

	/**
	 * 랜덤 문자열 생성
	 */
	protected function generateRandomString($length = 10)
	{
		// $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$characters = '0123456789';
		$charactersLength = strlen($characters);
		$randomString = '';
		for ($i = 0; $i < $length; $i++) {
			$randomString .= $characters[mt_rand(0, $charactersLength - 1)];
		}
		return $randomString;
	}

	// ---- oauth 공통 함수 ------------------------------------------------------------------------------------------------------- //

	/**
	 * oauth stateToken 생성
	 */
	protected function generateState()
	{
		if ($_SESSION["stateToken"]) {
			return $_SESSION["stateToken"];
		} else {
			$mt = microtime();
			$rand = mt_rand();
			$state = md5($mt . $rand);
			$_SESSION["stateToken"] = $state;
			return $state;
		}
	}

	/**
	 * 공통 : api 요청
	 */
	protected function requestApi($url, $is_post=false, $headers=[])
	{
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POST, $is_post);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		if (count($headers)) {
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		}
		$result['response'] = curl_exec($ch);
		if ($result['response']) $result['response'] = json_decode($result['response'], true);
		$result['status_code'] = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		return $result;
	}

	/**
	 * 공통 : oauth 처리
	 */
	public function oauthProc($oauth_token_url, $user_api_url, $oauth_token_post=false, $user_api_post=false)
	{
		// -- API - 로그인 ---------------------------------
		$result = [];

		if (empty($oauth_token_url)) {
			$result['result'] = 'fail';
			$result['msg'] = '토큰 조회 url 없음';
			return $result;
		}
		if (empty($user_api_url)) {
			$result['result'] = 'fail';
			$result['msg'] = '사용자 api url 없음';
			return $result;
		}

		$result = $this->requestApi($oauth_token_url, $oauth_token_post);
		$token = null;
		if ($result['status_code'] == 200) {
			$token = $result['response']['access_token'];
			$_SESSION['access_token'] = $result['response']['access_token'];
			$_SESSION['refresh_token'] = $result['response']['refresh_token'];
		} else {
			$result['result'] = 'fail';
			$result['msg'] = '토큰 조회 실패 : '.$oauth_token_url;
			return $result;
		}

		// -- API - 회원프로필 조회 ---------------------------------
		if ($token) {
			$headers = array();
			$headers[] = "Authorization: Bearer ".$token; // Bearer 다음에 공백 추가
			$result = $this->requestApi($user_api_url, $user_api_post, $headers);
			if ($result['status_code'] == 200) {
				$result['result'] = 'success';
				return $result;
			} else {
				$result['result'] = 'fail';
				$result['msg'] = '로그인 API 조회 실패 : '.$user_api_url;;
				return $result;
			}
		}
	}

	/**
	 * 가입처리용 세션 초기화
	 */
	protected function unsetSessionJoin()
	{
		unset($_SESSION['joinProvider']);
		unset($_SESSION['joinSocialId']);
		unset($_SESSION['joinNickname']);
		unset($_SESSION['joinProfileImage']);
		unset($_SESSION['joinId']);
		unset($_SESSION['joinName']);
		unset($_SESSION['joinEmail']);
		unset($_SESSION['joinPhone']);
		unset($_SESSION['joinBirthday']);
		unset($_SESSION['joinBirthdayType']);
		unset($_SESSION['joinGender']);
	}

	// ---- 네이버 ------------------------------------------------------------------------------------------------------- //
    /**
	 * 네이버 api 정보
	 */
    protected function getNaverApiInfo($action='login', $option='')
	{
        $apiInfo = [];
		$apiInfo['provider'] = 'NAVER';
		$domain = $this->domain;
        // 운영
        $apiInfo['client_id'] = $this->company['naver']['oauthClientId'];
        $apiInfo['client_secret'] = $this->company['naver']['oauthClientSecret'];
        // 테스트
        // $apiInfo['client_id'] = "YitweNG__WNyLpMpqUFT";
        // $apiInfo['client_secret'] = "ipDItxWk7P";

		// 필수값 : client_id, client_secret
		if (empty($apiInfo['client_id']) || empty($apiInfo['client_secret'])) {
			return null;
		}

		// callback url
		if ($action == 'join') {
			if ($option == 'simpleJoin') {
				$apiInfo['redirect_uri'] = urlencode($domain."/member/naverJoin2");
			} else {
				$apiInfo['redirect_uri'] = urlencode($domain."/member/naverJoin");
			}
		} elseif ($action == 'login') {
			if ($option == 'withoutJoin') {
				$apiInfo['redirect_uri'] = urlencode($domain."/member/naverLogin2");
			} else {
				$apiInfo['redirect_uri'] = urlencode($domain."/member/naverLogin");
			}
		}

		// 로그인 API
		$apiInfo['oauth_authorize_url'] = "https://nid.naver.com/oauth2.0/authorize?response_type=code&client_id=".$apiInfo['client_id']."&redirect_uri=".$apiInfo['redirect_uri']."&state=".$this->generateState();
		// 토큰 API url
		$apiInfo['oauth_token_url'] = "https://nid.naver.com/oauth2.0/token?grant_type=authorization_code&client_id=".$apiInfo['client_id']."&client_secret=".$apiInfo['client_secret']."&redirect_uri=".$apiInfo['redirect_uri']."&code=".(empty($_GET["code"])?'':$_GET["code"])."&state=".(empty($_GET["state"])?'':$_GET["state"]);
		// 사용자 API url
		$apiInfo['user_api_url'] = "https://openapi.naver.com/v1/nid/me";
        return $apiInfo;
    }

	/**
	 * 네이버 api url 조회
	 */
	public function getNaverApiURL($action='login', $option='')
	{
        $apiInfo = $this->getNaverApiInfo($action, $option);
		$apiURL = $apiInfo['oauth_authorize_url'];
		return $apiURL;
	}

	/**
	 * 네이버 로그인 API : 로그인 Callback
	 * /member/naverLogin
	 */
	public function naverLogin()
	{
		$this->naverOauth('login');
    }

	/**
	 * 네이버 로그인 API : 가입없이 로그인 허용 Callback
	 * 댓글쓰기에서 사용
	 * /member/naverLogin2
	 */
	public function naverLogin2()
	{
		$this->naverOauth('login', 'withoutJoin');
	}

    /**
     * 네이버 로그인 API : 가입 Callback
	 * /member/naverJoin
     */
	public function naverJoin()
	{
		$this->naverOauth('join');
    }

	/**
     * 네이버 로그인 API : 간편가입 Callback
	 * /member/naverJoin2
     */
	public function naverJoin2()
	{
		$this->naverOauth('join', 'simpleJoin');
	}

	/**
	 * 네이버 로그인 API : Oauth 처리
	 */
    protected function naverOauth($action='login', $option='')
	{
		$result = [];
        try {
			// 토큰 조회 url
			$apiInfo = $this->getNaverApiInfo($action, $option);
			$provider = $apiInfo['provider'];
			$oauth_token_url = $apiInfo['oauth_token_url'];
			$user_api_url = $apiInfo['user_api_url'];

			// 콜백 처리
			$result = $this->oauthProc($oauth_token_url, $user_api_url, false, false);
			
			if ($result['result'] == 'success') {
				if ($result['response']['resultcode'] == '00') {
					// 인증 성공

					if ($action == 'login') {
						// 로그인
						$filter = [];
						$filter['coId'] = $this->coId;
						// $filter['status'] = 'normal';
						$filter['provider'] = $provider;
						$filter['socialId'] = trim($result['response']['response']['id']);
						if ($this->loginProc($filter, $provider, $option)) {
							echo "<script>";
							echo "opener.afterLogin();";
							echo "window.close();";
							echo "</script>";
							exit;
						} else {
							if ($option == 'withoutJoin') {
								// 가입 없이 로그인
								$_SESSION['provider'] = $provider;
								$_SESSION['socialId'] = $result['response']['response']['id'];
								$_SESSION['memberId'] = $provider.'_'.$result['response']['response']['id'];
								$_SESSION['email'] = $result['response']['response']['email'];
								$_SESSION['memberName'] = $result['response']['response']['name'];
								$_SESSION['nickname'] = $result['response']['response']['nickname'];
								$_SESSION['profileImage'] = $result['response']['response']['profile_image'];
								echo "<script>";
								echo "opener.afterLogin();";
								echo "window.close();";
								echo "</script>";
								exit;
							} else {
								throw new \Exception("가입되지 않은 회원입니다.1", 400);
							}
						}

					} elseif ($action == 'join') {
						// 가입
						if ($option == 'simpleJoin') {
							// 간편가입
							$this->member['provider'] = $provider;
							$this->member['socialId'] = $result['response']['response']['id'];
							$this->member['id'] = $provider.'_'.$result['response']['response']['id'];
							$this->member['email'] = $result['response']['response']['email'];
							$this->member['name'] = $result['response']['response']['name'];
							$this->member['nickname'] = $result['response']['response']['nickname'];
							$this->member['profileImage'] = $result['response']['response']['profile_image'];
							// $this->member['phone'] = $result['response']['response']['mobile'];
							// if ($result['response']['response']['birthyear'] && $result['response']['response']['birthday']) $this->member['birthday'] = $result['response']['response']['birthyear'].str_replace('-','',$result['response']['response']['birthday']);
							// $this->member['birthdayType'] = 'SOLAR';
							// $this->member['gender'] = $result['response']['response']['gender'];
							// if ($result['response']['response']['gender'] == 'M') {
							// 	$this->member['gender'] = '남';
							// } elseif ($result['response']['response']['gender'] == 'W') {
							// 	$this->member['gender'] = '여';
							// }
							$this->member['argeeService'] = 'Y';
							$this->member['argeePrivacy'] = 'Y';
							$this->member['argeeThird'] = '';
							$this->member['agreeLetter'] = '';
							$join = $this->joinProc();
							if ($join['result'] == 'success') {
								echo "<script>";
								echo "alert('".$join['msg']."');";
								echo "opener.location = '/';";
								echo "window.close();";
								echo "</script>";
							} else {
								echo "<script>";
								echo "alert('".$join['msg']."');";
								echo "window.close();";
								echo "</script>";
							}
							exit;
						} else {
							// 가입정보 세션에 저장
							$_SESSION['joinProvider'] = $provider;
							$_SESSION['joinSocialId'] = $result['response']['response']['id'];
							$_SESSION['joinId'] = $provider.'_'.$result['response']['response']['id'];
							$_SESSION['joinEmail'] = $result['response']['response']['email'];
							$_SESSION['joinName'] = $result['response']['response']['name'];
							$_SESSION['joinNickname'] = $result['response']['response']['nickname'];
							$_SESSION['joinProfileImage'] = $result['response']['response']['profile_image'];
							// $_SESSION['joinPhone'] = $result['response']['response']['mobile'];
							// if ($result['response']['response']['birthyear'] && $result['response']['response']['birthday']) $_SESSION['joinBirthday'] = $result['response']['response']['birthyear'].str_replace('-','',$result['response']['response']['birthday']);
							// $_SESSION['joinBirthdayType'] = 'SOLAR';
							// $_SESSION['joinGender'] = $result['response']['response']['gender'];
							// if ($result['response']['response']['gender'] == 'M') {
							// 	$_SESSION['joinGender'] = '남';
							// } elseif ($result['response']['response']['gender'] == 'W') {
							// 	$_SESSION['joinGender'] = '여';
							// }
							// 회원가입 입력 페이지로 이동
							echo "<script>";
							echo "opener.location = '/member/join2';";
							echo "window.close();";
							echo "</script>";
						}
					}
				} else {
					throw new \Exception($provider." 로그인 오류", 400);
				}
			} else {
				throw new \Exception($provider." 로그인 오류", 400);
			}
        } catch(\Exception $e) {
			echo "<script>";
			echo "alert('".$this->common->getExceptionMessage($e)."');";
			echo "window.close();";
			echo "</script>";
		}
    }

	// ---- 카카오 ------------------------------------------------------------------------------------------------------- //
    /**
	 * 카카오 api 정보
	 */
    protected function getKakaoApiInfo($action='login', $option='')
	{
        $apiInfo = [];
		$apiInfo['provider'] = 'KAKAO';
		$domain = $this->domain;
        // 운영
        $apiInfo['client_id'] = $this->company['kakao']['oauthClientId'];
        $apiInfo['client_secret'] = $this->company['kakao']['oauthClientSecret'];
        // 테스트
        // $apiInfo['client_id'] = "8c534d0364c5b058e28e886716e297a0";
        // $apiInfo['client_secret'] = "ZrjFCDy12ANwB9v9FilzSzfRXajpP1Dr";

		// 필수값 : client_id, client_secret
		if (empty($apiInfo['client_id']) || empty($apiInfo['client_secret'])) {
			return null;
		}

		// callback url
		if ($action == 'join') {
			if ($option == 'simpleJoin') {
				$apiInfo['redirect_uri'] = urlencode($domain."/member/kakaoJoin2");
			} else {
				$apiInfo['redirect_uri'] = urlencode($domain."/member/kakaoJoin");
			}
		} elseif ($action == 'login') {
			if ($option == 'withoutJoin') {
				$apiInfo['redirect_uri'] = urlencode($domain."/member/kakaoLogin2");
			} else {
				$apiInfo['redirect_uri'] = urlencode($domain."/member/kakaoLogin");
			}
		}

		// 로그인 API
		$apiInfo['oauth_authorize_url'] = "https://kauth.kakao.com/oauth/authorize?response_type=code&client_id=".$apiInfo['client_id']."&redirect_uri=".$apiInfo['redirect_uri']."&state=".$this->generateState();
		// 토큰 API url
		$apiInfo['oauth_token_url'] = "https://kauth.kakao.com/oauth/token?grant_type=authorization_code&client_id=".$apiInfo['client_id']."&client_secret=".$apiInfo['client_secret']."&redirect_uri=".$apiInfo['redirect_uri']."&code=".(empty($_GET["code"])?'':$_GET["code"])."&state=".(empty($_GET["state"])?'':$_GET["state"]);
		// 사용자 API url
		$apiInfo['user_api_url'] = "https://kapi.kakao.com/v2/user/me";
        return $apiInfo;
    }

	/**
	 * 카카오 api url 조회
	 */
	public function getKakaoApiURL($action='login', $option='')
	{
        $apiInfo = $this->getKakaoApiInfo($action, $option);
		$apiURL = $apiInfo['oauth_authorize_url'];
		return $apiURL;
	}

	/**
	 * 카카오 로그인 API : 로그인 Callback
	 * /member/kakaoLogin
	 */
	public function kakaoLogin($action='')
	{
		$this->kakaoOauth('login');
	}

	/**
	 * 카카오 로그인 API : 가입없이 로그인 허용 Callback
	 * 댓글쓰기에서 사용
	 * /member/kakaoLogin2
	 */
	public function kakaoLogin2()
	{
		$this->kakaoOauth('login', 'withoutJoin');
	}

    /**
     * 카카오 로그인 API : 가입 Callback
	 * /member/kakaoJoin
     */
	public function kakaoJoin()
	{
		$this->kakaoOauth('join');
	}

	/**
     * 카카오 로그인 API : 간편가입 Callback
	 * /member/kakaoJoin2
     */
	public function kakaoJoin2()
	{
		$this->kakaoOauth('join', 'simpleJoin');
	}

	/**
     * 카카오 로그인 API : 가입 Callback
	 * /member/kakaoJoin
     */
    protected function kakaoOauth($action='login', $option='')
	{
		$result = [];
        try {
			// 토큰 조회 url
			$apiInfo = $this->getKakaoApiInfo($action, $option);
			$provider = $apiInfo['provider'];
			$oauth_token_url = $apiInfo['oauth_token_url'];
			$user_api_url = $apiInfo['user_api_url'];

			// 콜백 처리
			$result = $this->oauthProc($oauth_token_url, $user_api_url, false, false);

			if ($result['result'] == 'success') {
				if ($result['response']['id']) {
					// 인증성공

					if ($action == 'login') {
						// 로그인
						$filter = [];
						$filter['coId'] = $this->coId;
						// $filter['status'] = 'normal';
						$filter['provider'] = $provider;
						$filter['socialId'] = trim($result['response']['id']);
						if ($this->loginProc($filter, $provider, $option)) {
							echo "<script>";
							echo "opener.afterLogin();";
							echo "window.close();";
							echo "</script>";
							exit;
						} else {
							if ($option == 'withoutJoin') {
								// 가입 없이 로그인
								$_SESSION['provider'] = $provider;
								$_SESSION['socialId'] = $result['response']['id'];
								$_SESSION['memberId'] = $provider.'_'.$result['response']['id'];
								$_SESSION['email'] = $result['response']['kakao_account']['email'];
								$_SESSION['memberName'] = $result['response']['properties']['nickname'];
								$_SESSION['nickname'] = $result['response']['properties']['nickname'];
								$_SESSION['profileImage'] = $result['response']['properties']['thumbnail_image'];
								echo "<script>";
								echo "opener.afterLogin();";
								echo "window.close();";
								echo "</script>";
								exit;
							} else {
								throw new \Exception("가입되지 않은 회원입니다.", 400);
							}
						}
					} elseif ($action == 'join') {
						// 가입
						if ($option == 'simpleJoin') {
							// 간편가입
							$this->member['provider'] = $provider;
							$this->member['socialId'] = trim($result['response']['id']);
							$this->member['id'] = $provider.'_'.trim($result['response']['id']);
							$this->member['email'] = $result['response']['kakao_account']['email'];
							$this->member['name'] = $result['response']['properties']['nickname'];
							$this->member['nickname'] = $result['response']['properties']['nickname'];
							$this->member['profileImage'] = $result['response']['properties']['thumbnail_image'];	// 110x110
							// $this->member['profileImage'] = $result['response']['properties']['profile_image'];	// 640x640
							// $this->member['phone'] = str_replace('+82 ','0',$result['response']['kakao_account']['phone_number']); //
							// if ($result['response']['kakao_account']['birthyear'] && $result['response']['kakao_account']['birthday']) $this->member['birthday'] = $result['response']['kakao_account']['birthyear'].$result['response']['kakao_account']['birthday'];
							// $this->member['birthdayType'] = $result['response']['kakao_account']['birthday_type'];
							// $this->member['gender'] = $result['response']['kakao_account']['gender'];
							// if ($result['response']['kakao_account']['gender'] == 'male') {
							// 	$this->member['gender'] = '남';
							// } elseif ($result['response']['kakao_account']['gender'] == 'female') {
							// 	$this->member['gender'] = '여';
							// }
							$this->member['argeeService'] = 'Y';
							$this->member['argeePrivacy'] = 'Y';
							$this->member['argeeThird'] = '';
							$this->member['agreeLetter'] = '';
							$join = $this->joinProc();
							if ($join['result'] == 'success') {
								echo "<script>";
								echo "alert('".$join['msg']."');";
								echo "opener.location = '/';";
								echo "window.close();";
								echo "</script>";	
							} else {
								echo "<script>";
								echo "alert('".$join['msg']."');";
								echo "window.close();";
								echo "</script>";
							}
						} else {
							// 가입정보 세션에 저장
							$_SESSION['joinProvider'] = $provider;
							$_SESSION['joinSocialId'] = trim($result['response']['id']);
							$_SESSION['joinId'] = $provider.'_'.trim($result['response']['id']);
							$_SESSION['joinEmail'] = $result['response']['kakao_account']['email'];
							$_SESSION['joinName'] = $result['response']['properties']['nickname'];
							$_SESSION['joinNickname'] = $result['response']['properties']['nickname'];
							$_SESSION['joinProfileImage'] = $result['response']['properties']['thumbnail_image'];	// 110x110
							// $_SESSION['joinProfileImage'] = $result['response']['properties']['profile_image'];	// 640x640
							// $_SESSION['joinPhone'] = str_replace('+82 ','0',$result['response']['kakao_account']['phone_number']); //
							// if ($result['response']['kakao_account']['birthyear'] && $result['response']['kakao_account']['birthday']) $_SESSION['joinBirthday'] = $result['response']['kakao_account']['birthyear'].$result['response']['kakao_account']['birthday'];
							// $_SESSION['joinBirthdayType'] = $result['response']['kakao_account']['birthday_type'];
							// $_SESSION['joinGender'] = $result['response']['kakao_account']['gender'];
							// if ($result['response']['kakao_account']['gender'] == 'male') {
							// 	$_SESSION['joinGender'] = '남';
							// } elseif ($result['response']['kakao_account']['gender'] == 'female') {
							// 	$_SESSION['joinGender'] = '여';
							// }
							// 회원가입 입력 페이지로 이동
							echo "<script>";
							echo "opener.location = '/member/join2';";
							echo "window.close();";
							echo "</script>";
						}
					}
				} else {
					throw new \Exception($provider." 로그인 오류", 400);
				}
			} else {
				throw new \Exception($provider." 로그인 오류", 400);
			}
        } catch(\Exception $e) {
			echo "<script>";
			echo "alert('".$this->common->getExceptionMessage($e)."');";
			echo "window.close();";
			echo "</script>";
		}
    }

	// ---- 구글 ------------------------------------------------------------------------------------------------------- //
	/**
	 * 구글 api 정보
	 * 
	 * OAuth 2.0을 사용하여 Google API에 액세스 : https://developers.google.com/identity/protocols/oauth2?hl=ko
	 * 웹 서버 애플리케이션에 OAuth 2.0 사용 : https://developers.google.com/identity/protocols/oauth2/web-server?hl=ko
	 * userinfo API : https://www.oauth.com/oauth2-servers/signing-in-with-google/verifying-the-user-info/
	 * people.get : https://developers.google.com/people/api/rest/v1/people/get
	 */
    protected function getGoogleApiInfo($action='login', $option='')
	{
        $apiInfo = [];
		$apiInfo['provider'] = 'GOOGLE';
		$domain = $this->domain;
		// 운영
        $apiInfo['client_id'] = $this->company['google']['oauthClientId'];
        $apiInfo['client_secret'] = $this->company['google']['oauthClientSecret'];
        // 테스트
        // $apiInfo['client_id'] = "168145098774-9hhbela1vuc23e5vt8b8l0klarq4h648.apps.googleusercontent.com";
        // $apiInfo['client_secret'] = "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";

		// 필수값 : client_id, client_secret
		if (empty($apiInfo['client_id']) || empty($apiInfo['client_secret'])) {
			return null;
		}

		// callback url
		if ($action == 'join') {
			if ($option == 'simpleJoin') {
				$apiInfo['redirect_uri'] = urlencode($domain."/member/googleJoin2");
			} else {
				$apiInfo['redirect_uri'] = urlencode($domain."/member/googleJoin");
			}
		} elseif ($action == 'login') {
			if ($option == 'withoutJoin') {
				$apiInfo['redirect_uri'] = urlencode($domain."/member/googleLogin2");
			} else {
				$apiInfo['redirect_uri'] = urlencode($domain."/member/googleLogin");
			}
		}

		// 로그인 API
		$apiInfo['oauth_authorize_url'] = "https://accounts.google.com/o/oauth2/v2/auth?client_id=".$apiInfo['client_id']."&redirect_uri=".$apiInfo['redirect_uri']."&response_type=code&scope=email%20profile%20openid&access_type=offline"."&state=".$this->generateState();
		// 토큰 API url
		$apiInfo['oauth_token_url'] = "https://oauth2.googleapis.com/token?grant_type=authorization_code&client_id=".$apiInfo['client_id']."&client_secret=".$apiInfo['client_secret']."&redirect_uri=".$apiInfo['redirect_uri']."&code=".(empty($_GET["code"])?'':$_GET["code"])."&state=".(empty($_GET["state"])?'':$_GET["state"]);
		// 로그인 정보 조회 : 
		// tokeninfo API url (id, email 만 조회 가능)
		// $apiInfo['user_api_url'] = "https://oauth2.googleapis.com/tokeninfo";
		// 로그인 정보 조회 : userinfo API
		$apiInfo['user_api_url'] = "https://www.googleapis.com/oauth2/v3/userinfo";
		// 로그인 정보 조회 : people API url (id, email 외 정보가 필요한 경우, API 사용 등록 필요)
		// $apiInfo['user_api_url'] = "https://people.googleapis.com/v1/people/me?personFields=names,emailAddresses,photos";

        return $apiInfo;
    }

	/**
	 * 구글 api url 조회
	 */
	public function getGoogleApiURL($action='login', $option='')
	{
        $apiInfo = $this->getGoogleApiInfo($action, $option);
		$client_id = $apiInfo['client_id'];
		$redirect_uri = $apiInfo['redirect_uri'];
		$apiURL = $apiInfo['oauth_authorize_url'];
		return $apiURL;
	}

	/**
	 * 구글 로그인 API : 로그인 Callback
	 * /member/googleLogin
	 */
	public function googleLogin()
	{
		$this->googleOauth('login');
	}

	/**
	 * 구글 로그인 API : 가입없이 로그인 허용 Callback
	 * 댓글쓰기에서 사용
	 * /member/googleLogin2
	 */
	public function googleLogin2()
	{
		$this->googleOauth('login', 'withoutJoin');
	}

    /**
     * 구글 로그인 API : 가입 Callback
	 * /member/googleJoin
     */
	public function googleJoin()
	{
		$this->googleOauth('join');
	}

	/**
     * 구글 로그인 API : 간편가입 Callback
	 * /member/googleJoin2
     */
	public function googleJoin2()
	{
		$this->googleOauth('join', 'simpleJoin');
	}

	/**
	 * 구글 로그인 API : Oauth 처리
     */
    protected function googleOauth($action='login', $option='')
	{
		$result = [];
        try {
			// 토큰 조회 url
			$apiInfo = $this->getGoogleApiInfo($action, $option);
			$provider = $apiInfo['provider'];
			$oauth_token_url = $apiInfo['oauth_token_url'];
			$user_api_url = $apiInfo['user_api_url'];

			// 콜백 처리
			$result = $this->oauthProc($oauth_token_url, $user_api_url, true, false);
			// print_r($result); exit;

			if ($result['result'] == 'success') {
				// tokeninfo API 또는 userinfo API 사용시
				if ($result['response']['sub']) {
					// 인증 성공
					$id = trim($result['response']['sub']);

					if ($action == 'login') {
						// 로그인
						$filter = [];
						$filter['coId'] = $this->coId;
						// $filter['status'] = 'normal';
						$filter['provider'] = $provider;
						$filter['socialId'] = trim($id);
						if ($this->loginProc($filter, $provider, $option)) {
							echo "<script>";
							echo "opener.afterLogin();";
							echo "window.close();";
							echo "</script>";
							exit;
						} else {
							if ($option == 'withoutJoin') {
								// 가입 없이 로그인
								$_SESSION['provider'] = $provider;
								$_SESSION['socialId'] = $id;
								$_SESSION['memberId'] = $provider.'_'.$id;
								$_SESSION['email'] = $result['response']['email'];
								$_SESSION['memberName'] = empty($result['response']['name'])?'':$result['response']['name'];	// userinfo API
								$_SESSION['profileImage'] = empty($result['response']['picture'])?'':$result['response']['picture'];	// userinfo API
								echo "<script>";
								echo "opener.afterLogin();";
								echo "window.close();";
								echo "</script>";
								exit;
							} else {
								throw new \Exception("가입되지 않은 회원입니다.", 400);
							}
						}
					} elseif ($action == 'join') {
						// 가입
						if ($option == 'simpleJoin') {
							// 간편가입
							$this->member['provider'] = $provider;
							$this->member['socialId'] = $id;
							$this->member['id'] = $provider.'_'.$id;
							$this->member['email'] = $result['response']['email'];
							$this->member['name'] = empty($result['response']['name'])?'':$result['response']['name'];	// userinfo API
							$this->member['nickname'] = empty($result['response']['name'])?'':$result['response']['name'];	// userinfo API
							$this->member['profileImage'] = empty($result['response']['picture'])?'':$result['response']['picture'];	// userinfo API
							$this->member['argeeService'] = 'Y';
							$this->member['argeePrivacy'] = 'Y';
							$this->member['argeeThird'] = '';
							$this->member['agreeLetter'] = '';
							$join = $this->joinProc();
							if ($join['result'] == 'success') {
								echo "<script>";
								echo "alert('".$join['msg']."');";
								echo "opener.location = '/';";
								echo "window.close();";
								echo "</script>";	
							} else {
								echo "<script>";
								echo "alert('".$join['msg']."');";
								echo "window.close();";
								echo "</script>";
							}
						} else {
							// 가입정보 세션에 저장
							$_SESSION['provider'] = $provider;
							$_SESSION['joinSocialId'] = $id;
							$_SESSION['joinId'] = $provider.'_'.$id;
							$_SESSION['joinEmail'] = $result['response']['email'];
							$_SESSION['joinName'] = empty($result['response']['name'])?'':$result['response']['name'];	// userinfo API
							$_SESSION['joinNickname'] = empty($result['response']['name'])?'':$result['response']['name'];	// userinfo API
							$_SESSION['joinProfileImage'] = empty($result['response']['picture'])?'':$result['response']['picture'];	// userinfo API
							// $_SESSION['joinPhone'] = '';
							// $_SESSION['joinBirthday'] = '';
							// $_SESSION['joinBirthdayType'] = '';
							// $_SESSION['joinGender'] = '';
							// 회원가입 입력 페이지로 이동
							echo "<script>";
							echo "opener.location = '/member/join2';";
							echo "window.close();";
							echo "</script>";
						}
					}

				// people API 사용시
				/*
				if ($result['response']['resourceName']) {
					// 인증 성공

					// 가입정보
					$id = '';
					$name = '';
					$email = '';
					$profileImage = '';
					foreach ($result['response']['names'] as $key => $value) {
						if ($value['metadata']['primary']) {
							$id = $value['metadata']['source']['id'];
							$name = $value['displayName'];
						}
					}
					foreach ($result['response']['emailAddresses'] as $key => $value) {
						if ($value['metadata']['primary']) {
							$email = $value['value'];
						}
					}
					foreach ($result['response']['photos'] as $key => $value) {
						if ($value['metadata']['primary']) {
							$profileImage = $value['url'];
						}
					}

					// 가입정보 세션에 저장
					$_SESSION['joinProvider'] = $provider;
					$_SESSION['joinSocialId'] = trim($id);
					$_SESSION['joinNickname'] = $name;
					$_SESSION['joinProfileImage'] = $profileImage;
					$_SESSION['joinId'] = $provider.'_'.trim($id);
					$_SESSION['joinName'] = $name;
					$_SESSION['joinEmail'] = $email;
					// $_SESSION['joinPhone'] = '';
					// $_SESSION['joinBirthday'] = '';
					// $_SESSION['joinBirthdayType'] = '';
					// $_SESSION['joinGender'] = '';

					echo "<script>";
					echo "opener.location = '/member/join2';";
					echo "window.close();";
					echo "</script>";
					*/

				} else {
					throw new \Exception($provider." 로그인 오류", 400);
				}
			} else {
				throw new \Exception($provider." 로그인 오류", 400);
			}
		} catch(\Exception $e) {
			echo "<script>";
			echo "alert('".$this->common->getExceptionMessage($e)."');";
			echo "window.close();";
			echo "</script>";
		}
    }
}