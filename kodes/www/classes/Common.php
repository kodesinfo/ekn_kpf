<?php
namespace Kodes\Www;

class Common
{
	/** @var Class */
	protected $json;
	protected $log;

	/** @var variable */
	public $coId;
    public $config;
    public $dataDir;
	public $device;
	protected $preview;

    public function __construct()
    {
		$this->json = new Json();
		$this->log = new Log();
		$this->coId = $GLOBALS['coId'];
		$this->config = $this->getConfigCommon();
		$this->dataDir = $this->config['path']['data'].'/'.$this->coId;
        $this->device = empty($GLOBALS['deviceType'])?'pc':$GLOBALS['deviceType'];
		$this->preview = isset($_GET['preview']) && $_GET['preview']=='Y'?'tmp_':'';
    }

    /**
     * 템플릿 class 객체 생성
     */
    public function setTemplate($template_dir=null, $compile_dir=null)
    {
        $tpl = new Template_();
		if (empty($template_dir)) {
			$tpl->template_dir = '_template';
		} else {
			$tpl->template_dir = $template_dir;
		}
		if (empty($compile_dir)) {
			$tpl->compile_dir = '_compile';
		} else {
			$tpl->compile_dir = $compile_dir;
		}
		if (!is_dir($tpl->compile_dir)) {
			mkdir($tpl->compile_dir, 0777, true);
		}
		return $tpl;
    }

	/**
	 * common
	 */
	public function getConfigCommon()
	{
		if (empty($GLOBALS['common'])) {
			$GLOBALS['common'] = $this->json->readJsonFile('/webSiteSource/kodes/www/config', 'common');
		}
		return $GLOBALS['common'];
	}

	/**
	 * 회사
	 */
	public function getCompany()
	{
		if (empty($GLOBALS['company'])) {
			$GLOBALS['company'] = $this->json->readJsonFile($this->dataDir.'/config', $this->coId."_company");
		}
		return $GLOBALS['company'];
	}

	/**
	 * 카테고리
	 */
	public function getCategory()
	{
		if (empty($GLOBALS['category'])) {
			$GLOBALS['category'] = $this->json->readJsonFile($this->dataDir, $this->coId."_category");
		}
		return $GLOBALS['category'];
	}

	/**
	 * 카테고리 Tree
	 */
	public function getCategoryTree()
	{
		if (empty($GLOBALS['categoryTree'])) {
			$GLOBALS['categoryTree'] = $this->json->readJsonFile($this->dataDir, $this->coId."_categoryTree");
		}
		return $GLOBALS['categoryTree'];
	}

	/**
	 * 게시판 정보 목록
	 */
	public function getBoardInfoList()
	{
		if (empty($GLOBALS['boardInfoList'])) {
			$GLOBALS['boardInfoList'] = $this->json->readJsonFile($this->dataDir, $this->coId."_board");
		}
		return $GLOBALS['boardInfoList'];
	}

	/**
	 * 팝업 조회
	 */
	public function getPopup()
	{
		$popup = [];
		$now = date('Y-m-d H:i:s');
		$temp =  $this->json->readJsonFile($this->dataDir.'/popup', 'popup');
		if(!empty($temp))
		{
			foreach ($temp as $key => $value) {
				if ($now < $value['startTime']) {
					continue;
				}
				if ($now > $value['endTime']) {
					continue;
				}
				$value['content'] = str_replace("'", "\'", $value['content']);
				$popup[] = $value;
			}
		}

		return $popup;
	}

	/**
	 * 데이터를 변환시킨다.
	 * 
	 * @param $val Array 변환시킬 데이터
	 * @param $flag 입력/수정/삭제
	 * @param $removeField 제거시킬 필드
     * @return Array 변환된 데이터
	 */
	public function covertDataField($val, $flag, $removeField=[])
	{
		if ($flag == "insert") {
			$val["insert"]["date"]			= date('Y-m-d H:i:s');
			if (!empty($_SESSION["memberId"])) {
				$val["insert"]["memberId"]		= $_SESSION["memberId"];
				$val["insert"]["memberName"]	= $_SESSION["memberName"];
			}
		} elseif ($flag == "update") {
			$val["update"]["date"]			= date('Y-m-d H:i:s');
			if (!empty($_SESSION["memberId"])) {
				$val["update"]["memberId"]		= $_SESSION["memberId"];
				$val["update"]["memberName"]	= $_SESSION["memberName"];
			}
		} elseif ($flag == "delete") {
			$val["delete"]["is"]			= true;
			$val["delete"]["date"]			= date('Y-m-d H:i:s');
			if (!empty($_SESSION["memberId"])) {
				$val["delete"]["memberId"]		= $_SESSION["memberId"];
				$val["delete"]["memberName"]	= $_SESSION["memberName"];
			}
		}

		// 필드 제거
		foreach ($removeField as $key => $value) {
			unset($val[$value]);
		}

		return $val;
	}

	/**
	 * requestMethod 체크
	 * 
	 * @param String http method
	 * @return void
	 */
	function checkRequestMethod($requestMethod)
	{
		if($requestMethod != $_SERVER['REQUEST_METHOD']) {
            throw new \Exception('허용되지 않는 요청입니다. (Method Not Allowed)', 405);
        }
	}

	/**
	 * 예외 메시지 처리 및 로그 출력
	 * 
	 * @param \Exception $e 예외 객체
	 * @return String $msg 사용자에게 전달 할 메시지
	 */
	function getExceptionMessage(\Exception $e)
	{
		http_response_code($e->getCode());
		$msg = '';
		if($e->getCode() < 500) {
			$msg = $e->getMessage();
		} else {
			$msg = "요청 처리에 실패하였습니다.";
			$this->log->writeLog($this->coId, $e->getCode().' : '.$e->getMessage(), "Exception");
		}
		return $msg;
	}

	/**
	 * ip 가져옴
	 */
	public function getRemoteAddr()
	{
		return empty($_SERVER['HTTP_X_FORWARDED_FOR'])?$_SERVER['REMOTE_ADDR']:$_SERVER['HTTP_X_FORWARDED_FOR'];
	}

	/**
	 * 2차원 배열 내 값을 key/value로 검색하여, 일치하는 첫번째 row를 반환
	 * 검색하려는 key가 row에 존재해야 함
	 * 
	 * @param Array $array 검색대상 배열
	 * @param String $key 검색할 필드명
	 * @param String $value 검색할 값
	 * @return Array 검색결과 row
	 */
	public function searchArray2D($array, $key, $value)
	{
		if (empty($array) || !is_array($array) || empty($key) || empty($value)) {
			return null;
		}
		$index = array_search($value, array_column($array, $key));
		if ($index !== false) {
			return $array[$index];
		} else {
			return null;
		}
	}

	/**
	 * strpos에서 needle을 배열로 사용한 함수
	 * 
	 * @param String $str 문자열
	 * @param Array $needleArray 배열
	 * @return Bool 결과 true/false
	 */
	public function strposArray($str, $needleArray)
	{
		if (empty($str)) {
			return true;
		}
		if (empty($needleArray) || !is_array($needleArray) || count($needleArray) == 0) {
			return false;
		}
		$return = false;
		foreach ($needleArray as $key => $value) {
			if (!empty($value) && strpos($str, $value) !== false) {
				$return = true;
			}
		}
		return $return;
	}

	/**
	 * 본문을 Text형태로 변환
	 */
	public function convertTextContent($content)
	{
		$content = preg_replace('/\[[^]]+]/i', '', $content);
		return trim(str_replace(['&nbsp;', '\r', '\n'], [' ', '', ' '], strip_tags(preg_replace('/<(figure).*?<\/\1>/s', '', $content))));
	}

	/**
	 * 파일 목록에서 썸네일(첫번째 이미지) 가져옴
	 */
	public function getThumbnail($files)
	{
		$thumbnail = '';
		foreach ($files as $key => $value) {
			if (!empty($value['type']) && $value['type'] == 'image'){
				$thumbnail = $value['path'];
				break;
			}
		}
		return $thumbnail;
	}

	/**
	 * 파일 목록에서 썸네일 캡션(첫번째 이미지) 가져옴
	 */
	public function getThumbnailCaption($files)
	{
		$caption = '';
		foreach ($files as $key => $value) {
			if (!empty($value['type']) && $value['type'] == 'image'){
				$caption = $value['caption'];
				break;
			}
		}
		return $caption;
	}

	/**
	 * id 타입 체크
	 */
	public function getContentType($id) {
		$contentType = match(true) {
			preg_match('/[0-9]{4}-[0-9]{2}-[0-9]{2}/', $id) => "daily",
			strpos($id, '!') === 0 => "tag",
			strpos($id, '@') === 0 => "reporterId",
			strpos($id, $this->coId.'_SC') === 0 => "serviceCategoryId",
			strpos($id, $this->coId.'_S') === 0 => "seriesId",
			strpos($id, $this->coId.'_P') === 0 => "programId",
			strlen(str_replace($this->coId, "", $id)) == 9 => "categoryId",
			strlen(str_replace($this->coId, "", $id)) == 12 => "article",
			strlen($id) == 9 => "relation",
			default => "relation"
		};

		return $contentType;
	}

	/**
    * 숫자로 되어 있는 값을 년, 년 분기, 년 월, 년 월 일 로 형식을 변경하여 리턴
	* input : 
	*		date : 날짜
    * @return Array  [ Change Date] 
    */
	function changeDateFormat($data){
		if(is_numeric(str_replace('-','',$data))){
			$num = strlen($data);

			switch($num){
				case 4: // 년도
					$data=$data."년";
					break;
				case 5: // 분기
					$data=substr($data,0,4)."년".substr($data,4,1)."분기";
					break;
				case 6: // 월
					$data=substr($data,0,4)."년".substr($data,4,2)."월";
					break;
				case 8: // 년월일
					$data=substr($data,0,4)."년".substr($data,4,2)."월".substr($data,6,2)."일";
					break;
				case 10: // 년월일
					$data=date("Y년 m월 d일",strtotime($data));
					break;
			}
		}
		return $data;
	}
}