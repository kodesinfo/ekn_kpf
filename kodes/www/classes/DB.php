<?php 
namespace Kodes\Www;

/**
 * DB 클래스
 * 
 * @author  Kodes <kodesinfo@gmail.com>
 * @version 1.0
 *
 * @license 해당 프로그램은 kodes에서 제작된 프로그램으로 저작원은 코드스(https://www.kode.co.kr)
 */
class DB
{
	/** @var DB Info */
    protected $dbInfo;

	/** @var DB Connection mongodb Connection */
    protected $_mongo;

    /** @var db mongodb write pipline*/
    protected $_writeConcen;
    
	/**
     * 생성자
     */
    public function __construct($dbName=null)
	{
		$configDB = json_decode(file_get_contents("/webSiteSource/kodes/www/config/db.json"), true);
        $this->dbInfo = !empty($configDB[$dbName])?$configDB[$dbName]:$configDB['wcmsDB'];
		$this->_mongo = new \MongoDB\Driver\Manager($this->dbInfo['server']);
		$this->_writeConcen = new \MongoDB\Driver\WriteConcern(\MongoDB\Driver\WriteConcern::MAJORITY, 1000);
	}

	/**
	* database change
	* @param string $databaseName 데이타 베이스 이름
	*/
	public function chageDatabase($databaseName)
	{
		$this->dbInfo['db'] = $databaseName;
	}

	/**
     * DB Collection의 검색조건에 맞는 Row의 갯수를 반환
	 * 
     * @param string $collection Mongodb collection 명
     * @param Array $filter Mongodb 검색조건
     * @return int Row의 갯수
     */
	public function count($collection, $filter)
	{
		try{
			$command = new \MongoDB\Driver\Command(["count" => $collection, "query" => (Object)$filter]);
			$result = $this->_mongo->executeCommand($this->dbInfo['db'], $command);
			$res = current($result->toArray());
			return intval($res->n);
		}catch(\Exception $e) {
			// echo 'Message: ' .$e->getMessage();
		}
	}

    /**
     * DB Collection 의 리스트를 다차원 배열로 반환
	 * 
     * @param string $collection Mongodb collection 명
     * @param Array $filter Mongodb 검색조건
     * @param Array $options Mongodb 검색 옵션 sort등
     * @return Array 검색 조건에 맞는 리스트 배열
     */
	public function list($collection, $filter, $options)
	{
		try{
			$return = [];
			$query = new \MongoDB\Driver\Query((Object)$filter, $options);
			$cursor = $this->_mongo->executeQuery($this->dbInfo['db'].'.'.$collection, $query);
			foreach ($cursor as $document) {
				$return[] = json_decode(json_encode($document), true);
			}
			return $return;
		}catch(\Exception $e) {
			// echo 'Message: ' .$e->getMessage();
		}
	}
	
    /**
     * DB Collection의 검색조건에 맞는 정보를 반환
	 * 
     * @param string $collection Mongodb collection 명
     * @param Array $filter Mongodb 검색조건
     * @return Array 검색 단일 문서
     */
	public function item($collection, $filter, $options)
	{
		try{
			$return = [];
			$options['limit'] = 1;
			$query = new \MongoDB\Driver\Query($filter, $options);
			$cursor = $this->_mongo->executeQuery($this->dbInfo['db'].'.'.$collection, $query);
			foreach ($cursor as $document) {
				$return = json_decode(json_encode($document), true);
			}
			return $return;
		}catch(\Exception $e) {
			// echo 'Message: ' .$e->getMessage();
		}
    }
	
	/**
     * DB Collection의 검색조건에 맞는 Row의 정보를 입력
	 * 
     * @param string $collection Mongodb collection 명
     * @param Array $filter Mongodb 검색조건
     * @return int Row의 갯수
     */
	public function insert($collection, $objectInfo)
	{
		try{
			$bulk = new \MongoDB\Driver\BulkWrite;
			$bulk->insert($objectInfo);
			$result = $this->_mongo->executeBulkWrite($this->dbInfo['db'].'.'.$collection, $bulk, $this->_writeConcen);
			return $result;
		}catch(\Exception $e) {
			// echo 'Message: ' .$e->getMessage();
		}
	}

	/**
     * DB Collection의 검색조건에 맞는 Row의 정보를 수정
	 * 
     * @param String $collection Mongodb collection 명
     * @param Array $filter Mongodb 검색조건
     * @param Array $object Mongodb 변경내용
	 * @param Bool $multi Mongodb 다중문서 수정 여부
	 * @param Bool $arrayFilters Mongodb 문서 내 배열 검색조건
     * @return int Row의 갯수
     */
	public function update($collection, $filter, $object, $multi=false, $arrayFilters=null)
	{
		try {
			$bulk = new \MongoDB\Driver\BulkWrite;
			$options = ['multi'=>$multi];
			if (!empty($arrayFilters)) {
				$options['arrayFilters'] = $arrayFilters;
			}
			$bulk->update($filter, $object, $options);
			$result = $this->_mongo->executeBulkWrite($this->dbInfo['db'].'.'.$collection, $bulk, $this->_writeConcen);
			return $result;
		}catch(\Exception $e) {
			// echo 'Message: ' .$e->getMessage();
		}
    }
    
    /**
     * DB Collection의 검색조건에 맞는 Row의 정보를 입력 또는 수정
	 * 
     * @param string $collection Mongodb collection 명
     * @param Array $filter Mongodb 검색조건
     * @param Array $options Mongodb 검색조건
     * @return int Row의 갯수
     */
	public function upsert($collection, $filter, $options)
	{
		try{
			$bulk = new \MongoDB\Driver\BulkWrite;
			$bulk->update($filter, $options, ['upsert'=>true, 'multi'=>false]);
			$result = $this->_mongo->executeBulkWrite($this->dbInfo['db'].'.'.$collection, $bulk, $this->_writeConcen);
			return $result;
		}catch(\Exception $e) {
			// echo 'Message: ' .$e->getMessage();
		}
	}

    /**
     * DB Collection의 검색조건에 맞는 Row의 정보를 삭제
	 * 
     * @param string $collection Mongodb collection 명
     * @param Array $filter Mongodb 검색조건
	 * @param Bool $limit true(매칭된 첫번째 문서 삭제), false(매칭된 모든 문서 삭제)
     * @return int Row의 갯수
     */
	public function delete($collection, $filter, $limit=true)
	{
		try{
			$bulk = new \MongoDB\Driver\BulkWrite;
			$bulk->delete($filter, ['limit' => $limit]);
			$result = $this->_mongo->executeBulkWrite($this->dbInfo['db'].'.'.$collection, $bulk, $this->_writeConcen);
			return $result;
		}catch(\Exception $e) {
			// echo 'Message: ' .$e->getMessage();
		}
    }

    /**
     * Mongo Command 함수 실행
     *
     * @param string $database database 이름
     * @param array $aggregate 검색 조건
     * @return array 검색 조건에 맞는 검색 결과
     */
	public function command($database, $aggregate)
	{
		try{
			$query = new \MongoDB\Driver\Command($aggregate);
			$cursor = $this->_mongo->executeCommand($database, $query);
			$result =  array();
			$i=0;
			foreach ($cursor as $document) {
				$result[$i++] = json_decode(json_encode($document),true);;
			}

			return $result;
		}catch(\Exception $e) {
			// echo 'Message: ' .$e->getMessage();
		}
	}
}
