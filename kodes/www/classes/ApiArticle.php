<?php
namespace Kodes\Www;

// ini_set('display_errors', 1);

/**
 * api class
 */
class ApiArticle
{
    /** const */
	const ARTICLE_COLLECTION = 'article';

	/** @var Class 공통 */
	protected $common;
	protected $db;
	protected $json;
    protected $tpl;

	/** @var variable */
	protected $coId;
    protected $request;
    protected $id;
    protected $cdnDomain = '';	// cdn 도메인

    public function __construct($cdnDomain='')
    {
        // class
        $this->common = new Common();
        $this->db = new DB();
        $this->json = new Json();

        $this->coId = $this->common->coId;
        $this->cdnDomain = $cdnDomain;
        // data 저장 경로
        $this->dataPath = $this->common->config['path']['data'].'/'.$this->coId;
        $this->category = $this->common->getCategory();
	}

    /**
     * article 목록 DB에서 조회
     * 
     * @param request[key]           [필수] 조회 id 구분자 : categoryId(default), seriesId, programId, tag, daily(yyyy-mm-dd), reporterId, analytics, search
     * @param request[id]            [필수] 조회 id : id(여러개 일 경우 구분자 ,), keyword(key='search' 일때)
     * @param request[page]          [선택] 페이지 번호
     * @param request[limit]         [선택] 조회 갯수
     * @param request[pageNavCount]  [선택] 페이지 nav 버튼 수
     * @param request[inImage]       [선택] 이미지 있음 : 1
     * @param request[inVideo]       [선택] 동영상 있음 : 1
     * @param request[type_isFlash]  [선택] 속보여부 : 1
     * @param request[iniAids]       [선택] 제외article ID : Array
     * @param request[useRelation]   [선택] 관련기사 사용여부 : 1
     * @param request[isHtml]        [선택] 본문html 여부 : 1
     * 
     * key='search' 일때 아래 param 사용 가능
     * @param request[categoryId]    [선택] 카테고리ID
     * @param request[seriesId]      [선택] 시리즈ID
     * @param request[programId]     [선택] 프로그램ID
     * @param request[tag]           [선택] 태그
     * @param request[reporterId]    [선택] 기자ID
     * @param request[daily]         [선택] 발행일
     */
    public function items($request, $totalCount=null)
    {
        // key
        if (empty($request['key'] || !in_array($request['key'],['categoryId','seriesId','programId','tag','reporterId','daily','analytics','search']))) {
            return false;
        }

        $result = [];

        if ($request['key'] == 'analytics') {
            // 인기기사
            $items = $this->getAnalytics($request);
            return $items;
        } else {
            // DB 조회
            $filter['coId'] = $this->coId;
            $filter['status'] = 'publish';

            // 카테고리 리스트
            if ($request['key'] == 'categoryId') {
                $categories = [];
                if (!is_array($request['id'])) $request['id'] = explode(',', trim($request['id'],','));
                foreach ($request['id'] as $key => $value) {
                    if (!empty($value)) {
                        $categories[$key] = new \MongoDB\BSON\Regex('^'.preg_replace('/([0]{3,3})+$/', '', $value));
                        // info 조회
                        $result['info'][$key] = $this->common->searchArray2D($this->category, 'id', $value);
                    }
                }
                $filter['category.id'] = ['$in'=>$categories];

                // nav : 카테고리 일때만 조회
                $result['categoryNav'] = array_reverse($this->getNavigationList($request['id'][0]));
            }

            // 시리즈 리스트
            if ($request['key'] == 'seriesId') {
                $series = [];
                if (!is_array($request['id'])) $request['id'] = explode(',', trim($request['id'],','));
                foreach ($request['id'] as $key => $value) {
                    $series[$key] = $value;
                    // info 파일 조회
                    $result['info'][$key] = $this->getSeries($value);
                }
                $filter['series.id'] = ['$in'=>$series];
            }

            // 프로그램 리스트
            if ($request['key'] == 'programId') {
                $program = [];
                if (!is_array($request['id'])) $request['id'] = explode(',', trim($request['id'],','));
                foreach ($request['id'] as $key => $value) {
                    $program[$key] = $value;
                    // info 파일 조회
                    $result['info'][$key] = $this->getProgram($value);
                }
                $filter['program.id'] = ['$in'=>$program];
            }

            // 태그별 리스트
            if ($request['key'] == 'tag') {
                $tags = [];
                if (!is_array($request['id'])) $request['id'] = explode(',', trim($request['id'],','));
                foreach ($request['id'] as $key => $value) {
                    $tags[$key] = $value;
                    // info 파일 조회
                    $result['info'][$key] = $this->getTag($value);
                }
                $filter['tags'] = ['$in'=>$tags];
            }

            // 작성자별 리스트
            if ($request['key'] == 'reporterId') {
                $reporter = [];
                if (!is_array($request['id'])) $request['id'] = explode(',', trim($request['id'],','));
                foreach ($request['id'] as $key => $value) {
                    $reporter[$key] = $value;
                    // info 파일 조회
                    $result['info'][$key] = $this->getReporter($value);
                }
                $filter['reporter.id'] = ['$in'=>$reporter];
            }

            // 일자별 리스트 (yyyy-mm-dd)
            if ($request['key'] == 'daily') {
                $pubDate = $request['id']; // real 
                $filter['firstPublishDate'] = new \MongoDB\BSON\Regex(substr($pubDate,0,10).' *');
            }

            // 검색어 설정
            if ($request['key'] == 'search') {
                $filter['$text'] = ['$search' => $request['id']];

                // categoryId
                if (!empty($request['categoryId'])) {
                    $categoryId = $request['categoryId'];
                    $categories = [];
                    if (!is_array($categoryId)) $categoryId = explode(',', trim($categoryId,','));
                    foreach ($categoryId as $key => $value) {
                        $categories[$key] = new \MongoDB\BSON\Regex('^'.preg_replace('/([0]{3,3})+$/', '', $value));
                    }
                    $filter['category.id'] = ['$in'=>$categories];
                }
                // seriesId
                if (!empty($request['seriesId'])) {
                    $seriesId = $request['seriesId'];
                    $series = [];
                    if (!is_array($seriesId)) $seriesId = explode(',', trim($seriesId,','));
                    foreach ($seriesId as $key => $value) {
                        $series[$key] = $value;
                    }
                    $filter['series.id'] = ['$in'=>$series];
                }
                // programId
                if (!empty($request['programId'])) {
                    $programId = $request['programId'];
                    $program = [];
                    if (!is_array($programId)) $programId = explode(',', trim($programId,','));
                    foreach ($programId as $key => $value) {
                        $program[$key] = $value;
                    }
                    $filter['program.id'] = ['$in'=>$program];
                }
                // tag
                if (!empty($request['tag'])) {
                    $tag = $request['tag'];
                    $tags = [];
                    if (!is_array($tag)) $tag = explode(',', trim($tag,','));
                    foreach ($tag as $key => $value) {
                        $tags[$key] = $value;
                    }
                    $filter['tags'] = ['$in'=>$tags];
                }
                // reporterId
                if (!empty($request['reporterId'])) {
                    $reporterId = $request['reporterId'];
                    $reporter = [];
                    if (!is_array($reporterId)) $reporterId = explode(',', trim($reporterId,','));
                    foreach ($reporterId as $key => $value) {
                        $reporter[$key] = $value;
                        // info 파일 조회
                        $result['info'][$key] = $this->getReporter($value);
                    }
                    $filter['reporter.id'] = ['$in'=>$reporter];
                }
                // daily : 일자별 리스트 (yyyy-mm-dd)
                if (!empty($request['daily'])) {
                    $daily = $request['daily'];
                    $filter['firstPublishDate'] = new \MongoDB\BSON\Regex(substr($daily,0,10).' *');
                }
            }

            // 이미지 있음
            if (isset($request['inImage']) && ($request['inImage'] == '1' || $request['inImage'] == 'Y')) {
                $filter['files.type'] = 'image';
            }

            //  동영상 있음
            if (isset($request['inVideo']) && ($request['inVideo'] == '1' || $request['inVideo'] == 'Y')) {
                $filter['inVideo'] = true;
            }

            //  속보여부
            if (isset($request['type_isFlash']) && ($request['type_isFlash'] == '1' || $request['type_isFlash'] == 'Y')) {
                $filter['type.isFlash'] = true;
            }

            // 제외 article
            if (!empty($request['iniAids'])) {
                if (!is_array($request['iniAids'])) $request['iniAids'] = explode(',', $request['iniAids']);
                $filter['aid'] = ['$nin'=>$request['iniAids']];
            }

            // 자동노출제외
            if (!empty($request['excludeAutoPlace']) && boolval($request['excludeAutoPlace'])) {
                $filter['excludeAutoPlace'] = ['$ne'=>true];
            }

            // count
            if ($totalCount === null) {
                $result['totalCount'] = $this->db->count(self::ARTICLE_COLLECTION, $filter);
            } else {
                $result['totalCount'] = $totalCount;
            }

            // page
            $page = empty($request['page'])?1:$request['page'];
            $limit = empty($request['limit'])?10:$request['limit'];
            $pageNavCount = empty($request['pageNavCount'])?10:$request['pageNavCount'];
            $classPage = new Page();
            $result['page'] = $classPage->page($limit, $pageNavCount, $result['totalCount'], $page);
            
            // 옵션
            $options = ['skip' => ($page - 1) * $limit, 'limit' => $limit, 'sort' => ['firstPublishDate' => -1], 'projection' => ['_id'=>0]];
            
            // 1 page는 1년 이내 기사만 조회(category, series, program, tag)
            // 필요 시 사용
            // if ($page > 1 && in_array($request['key'], ['category','series','program','tag'])) {
            //     $filter['firstPublishDate'] = ['$gt'=>date('Y-m-d H:i:s',strtotime('-1 year'))];
            // }

            // list 조회
            $items = $this->db->list(self::ARTICLE_COLLECTION, $filter, $options);
        }

        if (!empty($items) || is_array($items)) {
            foreach ($items as $key => $value) {
                if (!empty($value['aid'])) {
                    $items[$key]['latestPublishDate'] = $value['publish']['date'];
                    if(!empty($request['isHtml']) && $request['isHtml']=="1"){
                        $items[$key]['encodedContent'] = preg_replace('/\r\n|\r|\n/','',$value['content']);
                    }
                    $items[$key]['content'] = $this->common->convertTextContent($value['content']);
                    $items[$key]['thumbnail'] = $this->common->getThumbnail($value['files']);
                    $items[$key]['thumbnailCaption'] = $this->common->getThumbnailCaption($value['files']);

                    // cdn 적용 : thumbnail
                    if (!empty($this->cdnDomain) && !empty($items[$key]['thumbnail'])) {
                        $items[$key]['thumbnail'] = $this->cdnDomain.$items[$key]['thumbnail'];
                    }

                    // 관련기사
                    if (!empty($request['useRelation']) && $request['useRelation'] == '1') {
                        $maxCountRelation = 5; // 조회 할 관련기사 수
                        if (!empty($value['relationId'][0])) {
                            $items[$key]['relation'] = $this->getRelation($value['relationId'][0]);
                        }
                        // 자기 자신은 제거
                        if (!empty($items[$key]['relation']['items']) && count($items[$key]['relation']['items']) > 0) {
                            $index = array_search($items[$key]['aid'], array_column($items[$key]['relation']['items'], 'aid'));
                            if ($index !== false) {
                                array_splice($items[$key]['relation']['items'], $index, 1);
                            }
                            // maxCountRelation 갯수로 설정
                            $items[$key]['relation']['items'] = array_slice($items[$key]['relation']['items'], 0, $maxCountRelation);
                        }
                    }

                    // 기자
                    if (!empty($items[$key]['reporter'])) {
                        foreach ($items[$key]['reporter'] as $key2 => $value2) {
                            if (!empty($value2['id'])) {
                                $reporter = $this->getReporter($value2['id']);
                                if (!empty($reporter)) {
                                    $items[$key]['reporter'][$key2] = $reporter;
                                }
                            }
                        }
                    }
                }
            }
        }
        $result['items'] = $items;

        return $result;
    }

    /**
     * 인기기사(Analytics) 데이터 조회
     */
    protected function getAnalytics($request)
    {
        // 30일 이전 기사가 많이본 뉴스로 나오지않도록 시작id를 설정
        $startId = $this->coId.date("Ymd",strtotime("-30 days"))."0000";

        if(!empty($request['id']) && is_array($request['id'])){
            $request['id'] = $request['id'][0];
        }
        // json 조회
        if (!empty($request['id'])) {
            $result = $this->json->readJsonFile($this->dataPath.'/list/analytics', $request['id']);
            if(empty($result)) $result= [];
            if($request['gaCategory']!=''){
                $result['info'] = $this->common->searchArray2D($this->category, 'id', $request['gaCategory']);
            }
            unset($result['info']['insert'], $result['info']['update']);
        }
    
        if (!empty($result['items'])) {
            foreach ($result['items'] as $key => $value) {
                // startId 보다 id 값이 이전일 경우 배열에서 제거 
                if($startId > $value['aid']){
                    unset($result['items'][$key]);
                    continue;
                }

                if (!empty($value['aid']) ) {
                    $item = $this->getArticleJson($value['aid']);
                    if (!empty($item)) {
                        $item['latestPublishDate'] = $item['publish']['date'];
                        $item['content'] = $this->common->convertTextContent($item['content']);
                        if(strlen($item['content'])>500){
                            $item['content'] = mb_substr($item['content'], 0, 500);
                        }
                        $item['thumbnail'] = $this->common->getThumbnail($item['files']);
                        $item['thumbnailCaption'] = $this->common->getThumbnailCaption($item['files']);
                        unset($item['type'], $item['publishMedia'], $item['embargo']);
                        
                        $result['items'][$key] = $item;
                        $result['items'][$key]['pv'] = $value['pv'];
                    }
                }
            }

            $data['items'] = $result['items'];
            $data['info'][0] = $result['info'];
            return $data;
        }
        return '';
    }

    /**
     * article 조회
     */
    public function item($request)
    {
        $api = new Api();

        $result = [];

        // article 조회
        $result['article'] = $this->getArticleJson($request['id']);

        if(empty($result['article'])) {
            // 미발행 또는 삭제된 기사
            $result['article'] = [];
            $result['article']['status'] = 'delete';
            return $result;
        } elseif ($result['article']['status'] != 'publish') {
            // 발행상태가 아니면 삭제를 리턴
            unset($result['article']);
            $result['article']['status'] = 'delete';
            return $result;
        }
        
        // seo
        $result['seo']['title'] = htmlentities($result['article']['title']);
        $result['seo']['description'] = mb_strcut($this->common->convertTextContent($result['article']['content']), 0, 200);
        foreach ($result['article']['files'] as $key => $value) {
			if ($value['type'] == 'image') {
				$result['seo']['img']['path'] = $value['path'];
                $result['seo']['img']['width'] = $value['width'];
                $result['seo']['img']['height'] = $value['height'];
				break;
			}
		}

        // nav
        $result['categoryNav'] = array_reverse($this->getNavigationList($result['article']['category'][0]['id']));

        // 본문
        $result['article']['content'] = html_entity_decode($result['article']['content']);
        $result['article']['textContent'] = $this->common->convertTextContent($result['article']['content']);
        $result['article']['content'] = str_replace(["\r","\n"], ["","\n"], $result['article']['content']);
        $result['article']['content'] = str_replace("../", "/", $result['article']['content']);
        // 본문 replace : video poster
        $result['article']['content'] = str_replace('poster="upload/vodImg/', 'poster="/vodImg/', $result['article']['content']);

        // 댓글
        $result['comment'] = $this->getArticleCommentJson($request['id']);

        // 기자
        $reporter = [];
        foreach ($result['article']['reporter'] as $key => $value) {
            $temp = $this->getReporter($value['id']);
            if (!empty($temp)) {
                $reporter[] = $temp;
            }
        }
		if (empty($reporter[0]['id'])) {
			$result['article']['reporter'] = $reporter;
		}
		// 기자의 작성 기사
        $maxCountReporterArticle = 5; // 조회 할 기자의 작성기사 수
		foreach($result['article']['reporter'] as &$item) {
            if (!empty($item['id'])) {
                $temp = $api->data('getArticles', ['key'=>'reporterId', 'page'=>'1', 'limit'=>$maxCountReporterArticle+1, 'id'=>$item['id']]);
                $item['info'] = $temp['info'];
                $item['items'] = $temp['items'];
                if (!empty($item['items']) && count($item['items']) > 0) {
                    // 자기 자신은 제거
                    $index = array_search($result['article']['aid'], array_column($item['items'], 'aid'));
                    if ($index !== false) {
                        array_splice($item['items'], $index, 1);
                    }
                    // maxCountRelation 갯수로 설정
                    $item['items'] = array_slice($item['items'], 0, $maxCountReporterArticle);
                }
            }
		}
		unset($item);

        // -- 관련기사 처리 --------------------------------------------------------------------------
        $maxCountRelation = 10; // 조회 할 관련기사 수
        // 첫번째 관련기사
        if (!empty($result['article']['relationId'][0])) {
            $result['article']['relation'] = $this->getRelation($result['article']['relationId'][0]);
        }
        // 첫번째 시리즈
        if (!empty($result['article']['series'][0]['id']) && (empty($result['article']['relation']['items']) || count($result['article']['relation']['items']) <= 1)) {
            $result['article']['relation'] = $api->data('getArticles', ['key'=>'seriesId', 'page'=>1, 'limit'=>$maxCountRelation+1, 'id'=>$result['article']['series'][0]['id']]);
        }
        // 첫번째 프로그램
        if (!empty($result['article']['program'][0]['id']) && (empty($result['article']['relation']['items']) || count($result['article']['relation']['items']) <= 1)) {
            $result['article']['relation'] = $api->data('getArticles', ['key'=>'programId', 'page'=>1, 'limit'=>$maxCountRelation+1, 'id'=>$result['article']['program'][0]['id']]);
        }
        // 첫번째 카테고리
        if (!empty($result['article']['category'][0]['id']) && (empty($result['article']['relation']['items']) || count($result['article']['relation']['items']) <= 1)) {
            $result['article']['relation'] = $api->data('getArticles', ['key'=>'categoryId', 'page'=>1, 'limit'=>$maxCountRelation+1, 'id'=>$result['article']['category'][0]['id']]);
        }
        // 자기 자신은 제거
        if (!empty($result['article']['relation']['items']) && count($result['article']['relation']['items']) > 0) {
            $index = array_search($result['article']['aid'], array_column($result['article']['relation']['items'], 'aid'));
            if ($index !== false) {
                array_splice($result['article']['relation']['items'], $index, 1);
            }
            // maxCountRelation 갯수로 설정
            $result['article']['relation']['items'] = array_slice($result['article']['relation']['items'], 0, $maxCountRelation);
        }
		// -- //관련기사 처리 --------------------------------------------------------------------------

        return $result;
    }

    /**
     * 기사 정보 json 조회
     */
    public function getArticleJson($id)
    {
        preg_match('/^'.$this->coId.'([0-9]{4})([0-9]{2})([0-9]{2})/', $id, $matches);
        $dir = $this->dataPath.'/article/'.$matches[1].'/'.$matches[2].'/'.$matches[3];
        return $this->json->readJsonFile($dir, $id);
    }

    /**
     * 기사 정보 json 조회
     */
    protected function getArticleCommentJson($id)
    {
        preg_match('/^'.$this->coId.'([0-9]{4})([0-9]{2})([0-9]{2})/', $id, $matches);
        $dir = $this->dataPath.'/comment/article/'.$matches[1].'/'.$matches[2].'/'.$matches[3];
        return $this->json->readJsonFile($dir, $id.'_comment');
    }

    /**
     * 관련기사 조회
     */
    protected function getRelation($id)
    {
        $dir = $this->dataPath.'/list/relation';
        return $this->json->readJsonFile($dir, $id);
    }

    /**
     * nav 
     */
    public function getNavigationList($id, $rVal=[])
    {
        $id = trim($id);
		if (empty($id) || empty($this->category) || !is_array($this->category)) return [];
        
		$index = array_search($id, array_column($this->category, 'id'));
        if ($index !== false) {
            $rVal[] = [
                'id' => $this->category[$index]['id'],
                'name' => $this->category[$index]['name'],
                'link' => [
                    'pc' => $this->category[$index]['link']['pc'],
                    'mobile' => $this->category[$index]['link']['mobile'],
                ],
                'imagePath' => $this->serviceCategory[$index]['imagePath'],
            ];
            if (!empty($this->category[$index]['parentId']) && $this->category[$index]['parentId'] != "0") {
                $rVal = $this->getNavigationList($this->category[$index]['parentId'], $rVal);
            }
        }
		
		return $rVal;
	}

    /**
     * serviceCategory nav
     */
    public function getServiceNavigationList($id, $rVal=[])
    {
        $id = trim($id);
		if (empty($id) || empty($this->serviceCategory) || !is_array($this->serviceCategory)) return [];

        $index = false;
        if (empty($rVal) && $index === false) {
            // link와 REQUEST_URI가 같은 경우
            foreach ($this->serviceCategory as $key => $value) {
                if ((!empty($value['link']['pc']) && $value['link']['pc'] == $_SERVER['REQUEST_URI']) || (!empty($value['link']['mobile']) && $value['link']['mobile'] == $_SERVER['REQUEST_URI'])) {
                    $index = $key;
                    break;
                }
            }
        }
        if ($index === false) {
            // ID가 일치
		    $index = array_search($id, array_column($this->serviceCategory, 'id'));
        }
        if ($index !== false) {
            $rVal[] = [
                'id' => $this->serviceCategory[$index]['id'],
                'name' => $this->serviceCategory[$index]['name'],
                'link' => [
                    'pc' => $this->serviceCategory[$index]['link']['pc'],
                    'mobile' => $this->serviceCategory[$index]['link']['mobile'],
                ],
                'imagePath' => $this->serviceCategory[$index]['imagePath'],
            ];
            if (!empty($this->serviceCategory[$index]['parentId']) && $this->serviceCategory[$index]['parentId'] != "0") {
                $rVal = $this->getServiceNavigationList($this->serviceCategory[$index]['parentId'], $rVal);
            }
        }

		return $rVal;
	}

    /**
     * 시리즈 정보 조회
     */
    public function getSeries($id)
    {
        $info = null;
        if (!empty($id)) {
            // info 파일 조회
            $info = $this->json->readJsonFile($this->dataPath.'/list/series', $id.'_info');
            // 없으면 DB 조회 후 서비스 파일 생성
            if (empty($info)) {
                $info = $this->db->item('series', ['coId' => $this->coId, 'id' => $id], ['projection'=>['_id'=>0]]);
                if (!empty($info)) {
                    $this->json->makeJson($this->dataPath.'/list/series', $id.'_info', $info);
                }
            }
        }
        // 없으면 ID만 설정
        if (empty($info)) {
            $info = ['coId'=>$this->coId, 'id' => $id];
        }
        $info['contentType'] = 'series';
        return $info;
    }

    /**
     * 프로그램 정보 조회
     */
    public function getProgram($id)
    {
        $info = null;
        if (!empty($id)) {
            // info 파일 조회
            $info = $this->json->readJsonFile($this->dataPath.'/list/program', $id.'_info');
            // 없으면 DB 조회 후 서비스 파일 생성
            if (empty($info)) {
                $info = $this->db->item('program', ['coId' => $this->coId, 'id' => $id], ['projection'=>['_id'=>0]]);
                if (!empty($info)) {
                    $this->json->makeJson($this->dataPath.'/list/program', $id.'_info', $info);
                }
            }
        }
        // 없으면 ID만 설정
        if (empty($info)) {
            $info = ['coId'=>$this->coId, 'id' => $id];
        }
        $info['contentType'] = 'program';
        return $info;
    }

    /**
     * 태그 정보 조회
     */
    public function getTag($id)
    {
        $info = null;
        if (!empty($id)) {
            // info 파일 조회
            $name = str_replace(['\\','/',':','*','?','"','<','>','|'], ['-','-','-','-','-','-','-','-','-'], $id);
            $info = $this->json->readJsonFile($this->dataPath.'/list/tag', $name.'_info');
            // 없으면 DB 조회 후 서비스 파일 생성
            if (empty($info)) {
                $info = $this->db->item('tag', ['coId' => $this->coId, 'name' => $id], ['projection'=>['_id'=>0]]);
                if (!empty($info)) {
                    $this->json->makeJson($this->dataPath.'/list/tag', $name.'_info', $info);
                }
            }
        }
        // 없으면 ID만 설정
        if (empty($info)) {
            $info = ['coId'=>$this->coId, 'name'=>$id];
        }
        $info['contentType'] = 'tag';
        return $info;
    }

    /**
     * 컷 정보 조회
     */
    public function getCut($id)
    {
        $info = null;
        if (!empty($id)) {
            // info 파일 조회
            $name = str_replace(['\\','/',':','*','?','"','<','>','|'], ['-','-','-','-','-','-','-','-','-'], $id);
            $info = $this->json->readJsonFile($this->dataPath.'/list/cut', $name.'_info');
            // 없으면 DB 조회 후 서비스 파일 생성
            if (empty($info)) {
                $info = $this->db->item('cut', ['coId' => $this->coId, 'name' => $id], ['projection'=>['_id'=>0]]);
                if (!empty($info)) {
                    $this->json->makeJson($this->dataPath.'/list/cut', $name.'_info', $info);
                }
            }
        }
        // 없으면 ID만 설정
        if (empty($info)) {
            $info = ['coId'=>$this->coId, 'name'=>$id];
        }
        $info['contentType'] = 'cut';
        return $info;
    }

    /**
     * 기자 정보 조회
     */
    public function getReporter($id)
    {
        $info = null;
        if (!empty($id)) {
            // info 파일 조회
            $info = $this->json->readJsonFile($this->dataPath.'/reporter', $id);
            // 없으면 DB 조회 후 서비스 파일 생성
            if (empty($info)) {
                $info = $this->db->item('manager', ['id' => $id], ['projection'=>['_id'=>0, 'password'=>0, 'salt'=>0]]);
                if (!empty($info)) {
                    $this->json->makeJson($this->dataPath.'/reporter', $id, $info);
                }
            }
        }
        // 없으면 ID만 설정
        if (empty($info)) {
            $info = ['coId'=>$this->coId, 'id' => $id];
        }
        $info['contentType'] = 'reporter';
        return $info;
    }
}