<?php
namespace Kodes\Www;
/**
 *  함수 (서비스 API)
 *
 * @file
 * @author  Kodes <kodesinfo@gmail.com>
 * @version 1.0
 *
 * @section LICENSE
 * 해당 프로그램은 kodes에서 제작된 프로그램으로 저작원은 코드스
 * https://www.kodes.co.kr
 *
 * https://tcms.kode.co.kr/apiService/get?apiIdx=oil_nationwide_price&axisX=DATE&axisY=PRICE&startDate=2023-10-05&endDate=2023-10-05
 *
 */

class ApiService
{
    /** @var String Collection Name */
    const COLLECTION = "api";

    protected $db;
    protected $common;

    /**
     * Compnay 생성자 config, DB 셋팅
	 * 
     */
    public function __construct(){
        // class
        $this->db = new DB();
		$this->db->chageDatabase("api");

        $this->common = new Common();
    }

	/**
	 * /apiService/get?apiIdx=oil_nationwide_price&axisX=DATE&axisY=PRICE&startDate=2023-10-05&endDate=2023-10-05
	 */
	public function get()
	{	
		$result = 'test';
		try {
            $field = ['collection'=>$_GET["apiIdx"]];
            $option = [];
            $data = $this->db->list(self::COLLECTION, $field, $option);
            
			$collection = $data[0]['collection'];

			$field1 = $_GET['axisX'];
			$field2 = $_GET['axisY'];
			$field3 = $_GET['field3'];
			$condition = $_GET['condition'];

			$tags=$field1.", ".$field2;
			$_GET['startDate'] = ( $_GET['startDate'] == "날짜선택"?"":$_GET['startDate']);
			$_GET['endDate'] = ( $_GET['endDate'] == "최신자"?"":$_GET['endDate']);

			$filter = [];
			if( !empty($field1)){
				if( !empty($_GET['startDate']!="") && !empty($_GET['endDate']!=""))
				{
					$filter[$field1] = ['$gte'=>$_GET['startDate'],'$lte'=>$_GET['endDate']];
				}
				else if( empty($_GET['startDate']!="") && !empty($_GET['endDate']!=""))
				{
					$filter[$field1] = ['$lte'=>$_GET['endDate']];
				}
				else if( !empty($_GET['startDate']!="") && empty($_GET['endDate']!=""))
				{
					$filter[$field1] = ['$gte'=>$_GET['startDate']];
				}else{
					$filter[$field1] = ['$gte'=>$data[0]['lastItemDate'],'$lte'=>$data[0]['lastItemDate']];
				}
			}
			
			if( !empty($field3) && !empty($condition)){
				$filter[$field3] = ['$in'=>explode(',', $condition)];
			}
            $option = ['sort' => [ $field1 => 1]];
            $result = $this->db->list($collection, $filter, $option);
			
			foreach($result as $key => $val){
				$result[$key][$field1]=$this->common->changeDateFormat($val[$field1]);
			}
	
		}catch(\Exception $e) {
            $result['msg'] = $this->common->getExceptionMessage($e);
        }

		echo json_encode($result);
	}
}
?>