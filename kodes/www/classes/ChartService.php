<?php
namespace Kodes\Www;

/**
 *  함수 (서비스 API)
 *
 * @file
 * @author  Kodes <kodesinfo@gmail.com>
 * @version 1.0
 *
 * @section LICENSE
 * 해당 프로그램은 kodes에서 제작된 프로그램으로 저작원은 코드스
 * https://www.kodes.co.kr
 *
 * http://xxx.kode.co.kr//get?id=bokBenchRate&startDate=20210801&endDate=20210810&axisY=date&axisX=data
 *
 */

class ChartService
{
	/** @var Class */
	protected $db;
	protected $common;

    /*
     * 허용 도메인 확인
     */
    public function __construct()
	{
		$this->path ='/webData/'.$_GET['coId'].'/chart/'.$_GET['date'];
		
		if( !$this->permissionDomain() ){
			//echo "허용되지 않는 Domain의 호출 입니다.";
			//die();
		}
    }

    /**
    * 사용 가능한 도메인인지 확인
    * @return Array  [ api Field List] 
    */
	public function permissionDomain(){
		$rtrnData = false;
		//$data = 'tcms.kode.co.kr';
		$data = [];

		if( !empty($data) ){
			$data = explode("\n",$data);
			foreach($data as $val){
				$val = str_replace(['*','.'],['.*','[.]'],$val);
				if(preg_match('/'.$val.'/',$_SERVER['HTTP_REFERER'])){
					$rtrnData = true;
					break;
				}
			}
		}else{
			$rtrnData = true;
		}

		return $rtrnData;
	}

    /**
    * api 데이터 json 
    * @return Array  [ api Field List] 
    */
	public function get()
	{
		if( !empty($_GET['id'])){
			$data = $this->getJsonData();
			
			if( empty($data)){
				$data = $this->getDBData();
				$this->putJsonData($list);
			}
			
			//$data['axisY']=array_column($list,$_GET['axisY']);
			//$data['axisX']=array_column($list,$_GET['axisX']);
		}else{
			
		}

		echo json_encode($data);
	}

    /**
    * cache 파일을 읽어 api 데이터를 리턴
    * @return Array  [ api data ] 
    */
	public function getJsonData()
	{
		$file  = $this->path.'/'.$_GET['id'].'.json';
		$data = '';
		if(is_file($file)){
			$data = json_decode(file_get_contents($file),true);
		}

		return $data;
	}

	public function getDBData()
	{

	}

    /**
    * api 데이터를 cache 파일에 기록
    * @return json
    */
	public function putJsonData($data)
	{
		$file  = $this->path.'/'.$_GET['id'].'.json';
		//file_put_contents($file, $data);
	}
}
?>