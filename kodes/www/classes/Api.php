<?php
namespace Kodes\Www;

// ini_set('display_errors', 1);

/**
 * api class
 * 
 * api 조회가 느릴 경우 확인사항
 * 1. collection에 index가 존재하는가?
 * 2. cache가 정상적으로 생성되는가?
 * 
 * 작업 시 주의사항
 * 1. warning이 발생하지 않도록 해야 함 (너무 많은 log가 발생할 수 있음)
 */
class Api
{
    /** const */
    const TTL = 1;  // Time To Live : 파일 갱신 주기(분)
	const API_PUBLISH_COLLECTION = 'apiPublish';
    const USE_COUNT_API_PUBLISH = 0;  // API_PUBLISH TotalCount 사용 기준값(해당 수 이상 사용)

	/** @var Class 공통 */
	protected $common;
	protected $json;
    protected $tpl;
	protected $db;

	/** @var variable */
	protected $coId;
    protected $request;
    protected $id;
    protected $cacheName;
    protected $cdnDomain = '';	    // cdn 도메인

    /**
     * 생성자
     */
    public function __construct($id=null)
    {
        // class
        $this->common = new Common();
        $this->db = new DB();
        $this->json = new Json();

        $this->coId = $this->common->coId;

        $this->dataPath = $this->common->config['path']['data'].'/'.$this->coId;    // nas
        $this->localDataPath = '/localData';                                        // local
        // cache 저장 경로
        $this->publishPath = $this->dataPath.'/api/publish';        // publish
        $this->cachePath = $this->dataPath.'/api/cache';            // cache
        $this->pageCachePath = $this->dataPath.'/api/page';         // pageCache

        $this->company = $this->common->getCompany();
        $this->cdnDomain = !empty($this->company['domain']['cdn'])?$this->company['domain']['cdn']:'';

        $this->id = $id;
	}

    /**
     * request 설정
     * 
     * @param allowMethod 허용된 method (array)
     */
    protected function setRequest($allowMethod)
    {
        if (in_array($_SERVER['REQUEST_METHOD'], $allowMethod)) {
            $this->request = $GLOBALS['_'.$_SERVER['REQUEST_METHOD']];
            unset($this->request['_url']);
            if ($this->id) {
                $this->request['id'] = $this->id;
            }
        } else {
            throw new \Exception("허용되지 않은 요청입니다.(allow: ".implode(',',$allowMethod).") - request: ".$_SERVER['REQUEST_METHOD']." ".$_GET['_url'], 405);
        }
    }

    /**
     * 권한 체크
     * 
     * @param authKey api key (wcms 회사정보에서 생성)
     * @param authSecret api secret (wcms 회사정보에서 생성)
     * @todo key, secret을 header로 전달하도록 수정
     */
    protected function checkAuth()
    {
        // authKey, authSecret 검증
        if ($this->company['api']['key'] != $this->request['authKey'] || $this->company['api']['secret'] != $this->request['authSecret']) {
            throw new \Exception("권한이 없습니다.", 401);
        }
    }

    /**
     * ETag 생성
     * 
     * @return ETag
     */
    protected function getETag()
    {
        return md5(uniqid($this->coId.'ETag',true));
    }

    /**
     * http cache 체크
     * 
     * @param response
     */
    protected function checkHttpCache($response)
    {
        $ifModifiedSince = isset($_SERVER['HTTP_IF_MODIFIED_SINCE']) ? $_SERVER['HTTP_IF_MODIFIED_SINCE'] == $response['Last-Modified'] : false;
        $ifNoneMatch = isset($_SERVER['HTTP_IF_NONE_MATCH']) ? $_SERVER['HTTP_IF_NONE_MATCH'] == $response['ETag'] : false;
        header('Last-Modified: '.$response['Last-Modified']);
        header('ETag: '.$response['ETag']);
        if ($ifModifiedSince !== false || $ifNoneMatch !== false) {
            http_response_code(304);    // Not Modified
            exit;
        }
    }

    /**
     * cache 명 생성
     * 포맷 : md5($functionName.'_'.http_build_query($param))
     * 
     * @param functionName 함수명
     * @return cache cache file name
     */
    protected function generateCacheName($functionName)
    {
        $param = $this->request;
        unset($param['authKey'], $param['authSecret'], $param['noCache'], $param['dev']); //cacheName 제외
        ksort($param);
        $this->cacheName = md5($functionName.'_'.http_build_query($param));
    }

    /**
     * cache 시간과 TTL을 비교하여 조회
     * 
     * @param cacheDate 파일저장 일시와 비교할 일자를 지정할 경우
     * @return response
     */
    protected function getCacheTime($cacheDate=null)
    {
        $filePath = $this->cachePath.'/'.$this->cacheName.'.json';
        if (!empty($this->cacheName) && is_file($filePath)) {
            $fileDate = date('YmdHi', filemtime($filePath));
            if (empty($cacheDate)) {
                // 현재일시와 비교하는 경우
                $date = date('YmdHi');
                $date = $date - ($date % self::TTL);
                if ($fileDate < $date) {
                    return false;
                } else {
                    return $this->getCache();
                }
            } else {
                // cacheDate와 비교하는 경우
                $date = date('YmdHi', strtotime($cacheDate));
                if ($fileDate < $date) {
                    return false;
                } else {
                    return $this->getCache();
                }
            }
        } else {
            return false;
        }
    }

    /**
     * cache 조회
     */
    protected function getCache()
    {
        $filePath = $this->cachePath.'/'.$this->cacheName.'.json';
        if (!empty($this->cacheName) && is_file($filePath)) {
            header('CacheFile-Date: '.date('Y-m-d H:i:s', filemtime($filePath)));    // cache file 사용
            $response = $this->json->readJsonFile($this->cachePath, $this->cacheName);
            $response['Last-Modified'] = gmdate('D, d M Y H:i:s', filemtime($filePath)).' GMT';
            return $response;
        } else {
            return false;
        }
    }

    /**
     * cache 저장
     * 
     * @param response
     * @return Last-Modified cache 저장시각
     */
    protected function setCache($response)
    {
        if (empty($this->cacheName)) {
            return null;
        }
        $this->json->makeJson($this->cachePath, $this->cacheName, $response);
        return gmdate('D, d M Y H:i:s', filemtime($this->cachePath.'/'.$this->cacheName.'.json')).' GMT';
    }

    /**
     * API PUBLISH 조회
     */
    protected function getApiPublish($type)
    {
        $filter = [
			'coId' => $this->coId,
            'type' => $type,
			'key' => $this->request['key'],
            'value' => is_array($this->request['id'])?$this->request['id']:explode(',', $this->request['id']),
            'inImage' => $this->request['inImage'],
            'inVideo' => $this->request['inVideo'],
            'type_isFlash' => $this->request['type_isFlash'],
		];

        // File 조회
        ksort($filter);
        $publishFileName = md5(http_build_query($filter));
        $apiPublish = $this->json->readJsonFile($this->publishPath, $publishFileName);

        // File 없으면 DB 조회
        if (empty($apiPublish)) {
            $options['projection'] = ['_id'=>0];
            $apiPublish = $this->db->item(self::API_PUBLISH_COLLECTION, $filter, $options);
        }

        return $apiPublish;
    }

    /**
     * API PUBLISH 저장
     */
    protected function setApiPublish($type, $apiPublish, $response)
    {
        if (empty($apiPublish['key'])) {
            $apiPublish = [
                'coId' => $this->coId,
                'type' => $type,
                'key' => $this->request['key'],
                'value' => (is_array($this->request['id'])?$this->request['id']:explode(',', $this->request['id'])),
                'inImage' => $this->request['inImage'],
                'inVideo' => $this->request['inVideo'],
                'type_isFlash' => $this->request['type_isFlash'],
                'makeDate' => date("Y-m-d H:i:s"),
            ];
        }
        $filter = [
			'coId' => $apiPublish['coId'],
            'type' => $apiPublish['type'],
			'key' => $apiPublish['key'],
            'value' => $apiPublish['value'],
            'inImage' => $apiPublish['inImage'],
            'inVideo' => $apiPublish['inVideo'],
            'type_isFlash' => $apiPublish['type_isFlash'],
		];
		$apiPublish['cacheDate'] = date("Y-m-d H:i:s");        // cache 생성일시

        // totalCount 있는 경우만 cache 저장
        if (isset($response['totalCount'])) {
            // DB 저장
            $apiPublish['totalCount'] = $response['totalCount'];  // totalCount
            $data = ['$set'=>$apiPublish];
            $result = $this->db->upsert(self::API_PUBLISH_COLLECTION, $filter, $data);

            // File 저장
            ksort($filter);
            $publishFileName = md5(http_build_query($filter));
            $this->json->makeJson($this->publishPath, $publishFileName, $apiPublish);
        }
    }

    /**
     * http 호출 처리
     * 
     * @param getFunctionName 조회 시 사용될 함수 명, 해당 함수를 생성하여 구현해야 함
     * @param allowMethod 허용된 method
     * @return json 출력
     */
    protected function httpProcess($functionName, $allowMethod=['GET'])
    {
        try {
            if (empty($functionName)) {
                return false;
            }

            header('Cache-Control: protected');

            // Content-Type 체크 (필요 시)
            // if (!in_array('application/json',explode(';',$_SERVER['CONTENT_TYPE']))) {
            //     http_response_code(400);    // Bad Request
            //     exit;
            // }

            // request 설정
            $this->setRequest($allowMethod);

            // 권한 체크
            $this->checkAuth();

            if ('GET' == $_SERVER['REQUEST_METHOD']) {
                // GET
                $response = $this->data($functionName);

                if ($response) {
                    if (empty($this->request['noCache']) || $this->request['noCache'] != '1') {
                        // http cache 체크 (네트워크 사용량을 줄이기 위함)
                        // $this->checkHttpCache($response);
                    }

                    // 성공
                    http_response_code(200);
                    header('Content-Type: application/json; charset=UTF-8');
                    echo json_encode($response);

                } elseif ($response === false) {
                    throw new \Exception("잘못된 요청입니다.", 400);
                    // http_response_code(400);    // Bad Request
                } else {
                    http_response_code(204);    // No Content: 요청 정상 처리하였지만, 돌려줄 리소스 없음.
                }
            } elseif ('POST' == $_SERVER['REQUEST_METHOD']) {
                // POST
            } elseif ('PUT' == $_SERVER['REQUEST_METHOD']) {
                // PUT
            } elseif ('DELETE' == $_SERVER['REQUEST_METHOD']) {
                // DELETE
            }
        } catch(\Exception $e) {
			$return['msg'] = $this->common->getExceptionMessage($e);
		}
        exit;
    }

    /**
     * data 조회
     * 내부에서 api를 이용하는 경우 사용
     * cache data를 조회 후 $response가 없으면 $functionName 실행
     * 
     * @param functionName      함수명
     * @param request           요청 변수
     * @param request[noCache]  캐시파일 사용 안함 : 1
     * @return response         
     */
    public function data($functionName, $request=null)
    {
        // TTL을 체크하여 cache 조회를 사용할 함수명
        $getCacheFunctions = ['getBoards'];
        // cache를 저장할 함수명
        $setCacheFunctions = ['getArticles','getBoards'];

        // 러닝타임 체크 시작
        $startTime = microtime(true);

        $response = [];

        if ($request) {
            $this->request = $request;
        }
        if (empty($this->request['id'])) {
            return false;
        }

        // GET noCache 적용
        if (!empty($_GET['noCache'])) {
            $this->request['noCache'] = $_GET['noCache'];
        }

        // cache 이름 생성
        $this->generateCacheName($functionName);

        // getCacheTime을 사용하는 function
        if (in_array($functionName, $getCacheFunctions)) {
            // cache 조회
            if (empty($this->request['noCache']) || $this->request['noCache'] != '1') {
                $response = $this->getCacheTime();
                if ($response) {
                    $response['Data-Source'] = 'Cache';
                    return $response;
                }
            }
        }

        // data 조회
        if (method_exists($this, $functionName)) {
            $response = $this->$functionName();
        } else {
            return false;
        }

        // ETag 생성
        $response['ETag'] = $this->getETag();

        // setCache 사용하는 function
        if (in_array($functionName, $setCacheFunctions)) {
            // cache 저장
            $response['Last-Modified'] = $this->setCache($response);
        }
        // if (empty($this->request['noCache']) || $this->request['noCache'] != '1') {
        //     $response['Last-Modified'] = $this->setCache($response);
        // } else {
        //     $response['Last-Modified'] = gmdate('D, d M Y H:i:s').' GMT';
        // }

        // 러닝타임 체크 종료
        $response['Running-Time'] = microtime(true) - $startTime;

        return $response;
    }

    /**********************************************************************************
     * page cache
     */

    /**
     * page cache를 조회
     */
    public function getPageCache($pageName, $allowMethod=['GET'])
    {
        // request 설정
        $this->setRequest($allowMethod);

        // cache 이름 생성
        $this->generateCacheName($pageName);

        // cache 조회
        if (empty($this->request['noCache']) || $this->request['noCache'] != '1') {
            return $this->getPageCacheTime(null, $this->pageCachePath);
        }
    }

    /**
     * page cache를 저장
     */
    public function setPageCache($response)
    {
        if (empty($this->cacheName) || empty($response)) {
            return null;
        }
        if (!is_dir($this->pageCachePath)) {
            mkdir($this->pageCachePath, 0777, true);
        }
        file_put_contents($this->pageCachePath.'/'.$this->cacheName, $response);
    }

    /**
     * page cache 시간과 TTL을 비교하여 조회
     * 
     * @param cacheDate 파일저장 일시와 비교할 일자를 지정할 경우
     * @param cachePath 파일저장 경로를 지정할 경우
     * @return response 페이지 내용
     */
    protected function getPageCacheTime()
    {
        $filePath = $this->pageCachePath.'/'.$this->cacheName;
        if (!empty($this->cacheName) && is_file($filePath)) {
            $fileDate = date('YmdHi', filemtime($filePath));
            // 현재일시와 비교 경우
            $date = date('YmdHi');
            $date = $date - ($date % self::TTL);
            if ($fileDate < $date) {
                return false;
            } else {
                // cache 조회
                header('PageCacheFile-Date: '.date('Y-m-d H:i:s', filemtime($filePath)));    // cache file 사용
                return file_get_contents($filePath);
            }
        } else {
            return false;
        }
    }

    /**********************************************************************************
     * api 데이터 조회
     */

    /**
     * article 목록 DB에서 조회
     * api publish collection 과 연동됨
     * 
     * @param request[key]           [필수] 조회 id 구분자 : categoryId(default), seriesId, programId, tag, daily(yyyy-mm-dd), reporterId, inVideo, analytics, search
     * @param request[id]            [필수] 조회 id : id(여러개 일 경우 구분자 ,), keyword(key='search' 일때)
     * @param request[page]          [선택] 페이지 번호
     * @param request[limit]         [선택] 조회 갯수
     * @param request[pageNavCount]  [선택] 페이지 nav 버튼 수
     * @param request[inImage]       [선택] 이미지 있음 : 1
     * @param request[inVideo]       [선택] 동영상 있음 : 1
     * @param request[type_isFlash]  [선택] 속보여부 : 1
     * @param request[iniAids]       [선택] 제외article ID : Array
     * @param request[useRelation]   [선택] 관련기사 사용여부 : 1
     * 
     * 검색조건(key='search' 일때)
     * 검색은 발행정보 cache는 사용하지 않으며, api cache만 사용함
     * @param request[categoryId]    [선택] 카테고리ID
     * @param request[seriesId]      [선택] 시리즈ID
     * @param request[programId]     [선택] 프로그램ID
     * @param request[tag]           [선택] 태그
     * @param request[reporterId]    [선택] 기자ID
     * @param request[daily]         [선택] 발행일
     */
    protected function getArticles()
    {
        if (empty($this->request['id']) || empty($this->request['key'])) {
            return false;
        }

        $response = null;

        // api 발행정보
        $apiPublish = null;
        // 체크 : cache를 사용하지 않는 key
        if (!in_array($this->request['key'],['analytics','search'])) {
            // bool
            $this->request['inImage'] = empty($this->request['inImage']) || $this->request['inImage'] == 'N'?false:true;
            $this->request['inVideo'] = empty($this->request['inVideo']) || $this->request['inVideo'] == 'N'?false:true;
            $this->request['type_isFlash'] = empty($this->request['type_isFlash']) || $this->request['type_isFlash'] == 'N'?false:true;

            // apiPublish 조회
            $apiPublish = $this->getApiPublish('article');
            if (!empty($apiPublish['key'])) {
                if (!empty($apiPublish['cacheDate']) && $apiPublish['makeDate'] < $apiPublish['cacheDate']) {
                    // cache 조회
                    if (empty($this->request['noCache']) || $this->request['noCache'] != '1') {
                        $response = $this->getCacheTime($apiPublish['cacheDate']);
                    } else {
                        $response = null;
                    }
                } else {
                    $response = null;
                }
                $response['Data-Source'] = 'Cache';
            }
        }

        // DB조회 (cache 없음)
        // if (empty($response['items'])) {
        if (empty($response['info']) && empty($response['items'])) {
            // count를 DB에서 조회하는 조건
            if (empty($apiPublish['makeDate']) && empty($apiPublish['totalCount'])) {
                // apiPublish가 없으면
                $apiPublish['totalCount'] = null;
            } elseif (self::USE_COUNT_API_PUBLISH != 0 && self::USE_COUNT_API_PUBLISH > $apiPublish['totalCount']) {
                // totalCount가 USE_COUNT_API_PUBLISH 미만
                $apiPublish['totalCount'] = null;
            }
            $apiArticle = new ApiArticle($this->cdnDomain);
            $response = $apiArticle->items($this->request, $apiPublish['totalCount']);
            if (empty($response)) {
                return false;
            }

            if ($this->request['key'] != 'search') {
                $this->setApiPublish('article', $apiPublish, $response);
            }

            if (!in_array($this->request['key'],['analytics'])) {
                $response['Data-Source'] = 'DB';
            } else {
                $response['Data-Source'] = 'File';
            }
        }

        return $response;
    }

    /**
     * article 조회
     * 
     *  @param id [필수]기사id
     */
    protected function getArticle()
    {
        if (empty($this->request['id'])) {
            return false;
        }

        // 조회
        $apiArticle = new ApiArticle();
        $response = $apiArticle->item($this->request);

        $response['Data-Source'] = 'File';

        return $response;
    }

    /**
     * 면편집 layout 정보 조회
     * 
     * @param request[id]           [필수] 페이지ID
     * @param request[dataType]     [필수] 데이터 타입 : json, html
     * @param request[contentType]  [필수] 컨텐츠 타입 : layout, box
     * @param request[deviceType]   [필수] 디바이스 타입 : pc(default), mobile
     * @param request[boxId]        [선택] 영역ID, contentType=box 인 경우 필수 : 구분자(,)
     * @param request[prefixLayout] [선택] layout 미리보기 prefix
     * @param request[prefixBox]    [선택] box 미리보기 prefix
     * @param request[useRalation]  [선택] 관련기사 사용여부 : 1
     * @param request[noAd]         [선택] 배너없음 : 1
     * @param request[listIdType]   [선택] (기사리스트 전용) 리스트 타입 : 리스트ID의 종류 (categoryId, seriseId, programId, reporterId, tag)
     * @param request[listId]       [선택] (기사리스트 전용) 리스트 ID
     * @param request[page]      	[선택] (기사리스트 전용) page 페이지 번호
     * @param request[limit]      	[선택] (기사리스트 전용) 리스트 조회 수
     * @param request[inImage]      [선택] (기사리스트 전용) 이미지 있음 : 1
     * @param request[inVideo]      [선택] (기사리스트 전용) 동영상 있음 : 1
     * @param request[aid]          [선택] (기사뷰 전용) 기사ID
     */
    protected function getLayout()
    {
        if (empty($this->request['id'])) {
            return false;
        }

        // 조회
        $request = $this->request;
        if (!empty($_GET['noAd'])) $request['noAd'] = $_GET['noAd'];	// 광고 없음
        $apiLayout = new ApiLayout($request['deviceType'], $this->cdnDomain);
        $response = $apiLayout->getLayout($request);

        $response['Data-Source'] = 'File';

        return $response;
    }

    /**
     * 게시글 목록 조회
     * 
     * @param request[key]          [필수] id 구분자 : id
     * @param request[id]           [필수] id
     * @param request[searchText]   [선택] 검색어
     * @param request[page]         [선택] 페이지 번호
     * @param request[limit]        [선택] 조회 갯수
     * @param request[pageNavCount] [선택] 페이지 nav 버튼 수
     */
    protected function getBoards()
    {
        if (empty($this->request['id'])) {
            return false;
        }

        // 조회
        $apiBoard = new ApiBoard($this->cdnDomain);
        // 게시글 목록
        $response = $apiBoard->items($this->request);

        $response['Data-Source'] = 'DB';

        return $response;
    }

    /**
     * 게시글 조회
     * 
     * @param request[key]          [필수] id 구분자 : id
     * @param request[id]           [필수] 게시글id
     * @param request[no]           [필수] 게시글no
     */
    protected function getBoard()
    {
        if (empty($this->request['id'])) {
            return false;
        }

        // 조회
        $apiBoard = new ApiBoard($this->cdnDomain);
        // 게시글 목록
        $response = $apiBoard->item($this->request);

        $response['Data-Source'] = 'DB';

        return $response;
    }

    /**********************************************************************************
     * http url 호출
     */

    /**
     * newslist api
     * 
     * @url : /api/articles/{id}?key=categoryId&authKey={authKey}&authSecret={authSecret}
     * @ex /api/articles/kbc001000000,kbc002000000?key=categoryId&authKey=kbc&authSecret=DhsONHXMfg9VXEpPqfTT7cfLXOQivsyR8PHwqUNVnIBz5uPrxbiVWa9vsjrGc9kzE2faTFHeTbJMQnxX5rA

     * @method GET
     */
    public function articles()
    {
        $this->httpProcess('getArticles', ['GET']);
    }

    /**
     * news api
     * 
     * @url : /api/article/{aid}?authKey={authKey}&authSecret={authSecret}
     * @ex /api/article/kod202205090001?authKey=kbc&authSecret=DhsONHXMfg9VXEpPqfTT7cfLXOQivsyR8PHwqUNVnIBz5uPrxbiVWa9vsjrGc9kzE2faTFHeTbJMQnxX5rA
     * 
     * @method : GET
     */
    public function article()
    {
        $this->httpProcess('getArticle', ['GET']);
    }

    /**
     * 면편집 layout api
     * 
     * @url : /api/layout/{layoutId}?contentType={contentType}&deviceType={deviceType}&dataType={dataType}&authKey={authKey}&authSecret={authSecret}
     * @ex : /api/layout/main?contentType=layout&deviceType=pc&dataType=html&authKey=kbc&authSecret=DhsONHXMfg9VXEpPqfTT7cfLXOQivsyR8PHwqUNVnIBz5uPrxbiVWa9vsjrGc9kzE2faTFHeTbJMQnxX5rA
     * @ex : /api/layout/main?contentType=layout&deviceType=pc&dataType=json&authKey=kbc&authSecret=DhsONHXMfg9VXEpPqfTT7cfLXOQivsyR8PHwqUNVnIBz5uPrxbiVWa9vsjrGc9kzE2faTFHeTbJMQnxX5rA
     * 
     * @method : GET
     */
    public function layout()
    {
        $this->httpProcess('getLayout', ['GET']);
    }

    /**
     * 게시판 리스트 api
     * 
     * @url : /api/boards/{id}?key=id&authKey={authKey}&authSecret={authSecret}
     * @ex /api/boards/kbc_P000088_note?key=id&authKey=kbc&authSecret=DhsONHXMfg9VXEpPqfTT7cfLXOQivsyR8PHwqUNVnIBz5uPrxbiVWa9vsjrGc9kzE2faTFHeTbJMQnxX5rA
     *
     * @method GET
     */
    public function boards()
    {
        $this->httpProcess('getBoards', ['GET']);
    }

    /**
     * 게시판 뷰 api
     * 
     * @url : /api/board/{id}?key=id&no={no}&authKey={authKey}&authSecret={authSecret}
     * @ex /api/board/kbc_P000088_note?key=id&no=1&authKey=kbc&authSecret=DhsONHXMfg9VXEpPqfTT7cfLXOQivsyR8PHwqUNVnIBz5uPrxbiVWa9vsjrGc9kzE2faTFHeTbJMQnxX5rA
     * 
     * @method : GET
     */
    public function board()
    {
        $this->httpProcess('getBoard', ['GET']);
    }
}