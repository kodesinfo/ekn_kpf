<?php
namespace Kodes\Www;

class Main
{
    public $common;
    protected $api;
    
    public function __construct()
    {
        $this->common = new Common();
        $this->api = new Api();
    }
    
    public function main()
    {
        $request = [];
        $request['id'] = 'main';
        $request['dataType'] = 'html';
        $request['contentType'] = 'layout';
        // $request['noCache'] = '1';
        $request['deviceType'] = $this->common->device;
		$return['main'] = $this->api->data('getLayout', $request);

        // side banner
        $request['id'] = 'sideBanner';
        $request['dataType'] = 'html';
        $request['contentType'] = 'box';
        $return['sideBanner'] = $this->api->data('getLayout', $request);

        // popup
        $return['popup'] = $this->common->getPopup();

		return $return;
    }
}