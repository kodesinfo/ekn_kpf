<?php
namespace Kodes\Www;

class Layout
{
    /** @var Class */
    protected $common;
    protected $api;

    /** @var variable */
    protected $id;

    /**
     * 생성자
     */
    public function __construct($id=null)
    {
        $this->common = new Common();
        $this->api = new Api();

        $this->id = $id;
    }

    /**
     * Layout View
     */
    public function view()
    {
        $return = [];

        // layout 조회
        $request = [];

        // 미리보기
        if (!empty($_GET['preview'])) {
            $preview = $_GET['preview']=="Y"?"tmp_":"";
            if (!empty($preview)) {
                $request['prefixBox'] ="tmp_";
            }
        }
        // history
        if (!empty($_GET['history'])) {
            $history = $_GET['history']?$_GET['history']:"";
            if (!empty($history)) {
                if ($history=="layout") {
                    $request['prefixLayout'] ="history_";
                } else {
                    $request['prefixBox'] ="history_";
                }
            }
        }

        $request['contentType'] = 'layout';
        $request['id'] = $this->id;
        $request['dataType'] = 'html';
        $request['deviceType'] = $this->common->device;
        // 기사리스트 전용
        if (!empty($_GET['id'])) {
            $request['listId'] = $_GET['id'];
            $request['listIdType'] = 'categoryId';
            $request['page'] = $_GET['page'];
            $request['limit'] = 20;
        }
        // 기사뷰 전용
        if (!empty($_GET['aid'])) {
            $request['aid'] = $_GET['aid'];
        }
        $return['main'] = $this->api->data('getLayout', $request);

		return $return;
    }

     /**
     * Layout photoView
     */
    public function photoView()
    {
        $return = [];

        // layout 조회
        $request = [];

        // 미리보기
        if (!empty($_GET['preview'])) {
            $preview = $_GET['preview']=="Y"?"tmp_":"";
            if (!empty($preview)) {
                $request['prefixBox'] ="tmp_";
            }
        }
        // history
        if (!empty($_GET['history'])) {
            $history = $_GET['history']?$_GET['history']:"";
            if (!empty($history)) {
                if ($history=="layout") {
                    $request['prefixLayout'] ="history_";
                } else {
                    $request['prefixBox'] ="history_";
                }
            }
        }

        $request['contentType'] = 'layout';
        $request['id'] = $this->id;
        $request['dataType'] = 'html';
        $request['deviceType'] = $this->common->device;
        // 기사리스트 전용
        if (!empty($_GET['id'])) {
            $request['listId'] = $_GET['id'];
            $request['listIdType'] = 'categoryId';
            $request['page'] = $_GET['page'];
            $request['limit'] = 20;
        }
        // 기사뷰 전용
        if (!empty($_GET['aid'])) {
            $request['aid'] = $_GET['aid'];
        }
        $return['main'] = $this->api->data('getLayout', $request);

		return $return;
    }
}