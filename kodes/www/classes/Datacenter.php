<?php
namespace Kodes\Www;

class Datacenter
{
    public $common;
    protected $api;
    
	private $pageLimit = 15;
	private $pageNavCnt = 10;

    public function __construct($id=null)
    {
        $this->common = new Common();
        $this->api = new Api();

		$this->id = $id;
    }
    
    public function datacenter()
    {
        $request = [];
        $request['id'] = 'datacenter';
        $request['dataType'] = 'html';
        $request['contentType'] = 'layout';
        // $request['noCache'] = '1';
        $request['deviceType'] = $this->common->device;
		$return['datacenter'] = $this->api->data('getLayout', $request);

        //print_r($return);

		return $return;
    }
    
    public function list()
    {
        $return['id'] = $this->id;

        $jsonFile = '/webData/ekn/list/category/'.$this->id.".json";
        $jsonData = "";
        if(is_file($jsonFile)){
            $str = file_get_contents($jsonFile);
		    $jsonData = json_decode($str,true);
        }

        $return['maxNum'] = $jsonData['count'];

        $list = $jsonData['list'];
        $filePage = intval(100/$this->pageLimit);

        // 기본 페이지와 최대 페이지 계산
        $maxPage=ceil($return['maxNum']/$this->pageLimit);
        $page = 1;
        if($_GET['page']==""?1:$_GET['page'] < $maxPage) {
            $page = $_GET['page']==""?1:$_GET['page'];
        }else{
            $page = $maxPage;
        }

        // 실제 가져와야 할 갯수 계산
        $startNum = ($page * $pageLimit) - $pageLimit;

        // 네비게이션 시작과 끝을 계산
        $startPage = (floor(($page-1) / $this->pageNavCnt) * $this->$pageNavCnt) + 1;
        $endPage = $startPage + $this->pageNavCnt - 1;
        if ($endPage > $maxPage) $endPage = $maxPage;

        // 페이징 계산
        for ($i=$startPage; $i<=$endPage; $i++) {
            $return['pageNavibar'][] = [
                'page' => $i,
                'class' => ($i == $page)? 'current' : '',
            ];
        }

        // 이전/다음 페이지 계산
        $prevPage = $startPage - 1;
        if($prevPage > 1){
            $return['prevPage'] = $prevPage;
        }else{
            $return['prevPage'] = '';
        }

        $nextPage = $endPage + 1;
        if($nextPage < $maxPage){
            $return['nextPage'] = $nextPage;
        }else{
            $return['nextPage'] = '';
        }

        // 시작/끝 페이지
        $return['firstPage'] = $maxPage?"1":'';
        $return['lastPage'] = $maxPage;

        $return['list'] = $jsonData['list'];

        return $return;
    }
}