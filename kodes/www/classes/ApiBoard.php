<?php
namespace Kodes\Www;

// ini_set('display_errors', 1);

/**
 * ApiBoard class
 */
class ApiBoard
{
    /** const */
	const BOARD_COLLECTION = 'board';
	const BOARD_INFO_COLLECTION = 'boardInfo';

	/** @var Class 공통 */
	protected $common;
	protected $db;
	protected $json;
    protected $tpl;

	/** @var variable */
	protected $coId;
    protected $request;
    protected $id;
    protected $cdnDomain = '';	// cdn 도메인

    public function __construct($cdnDomain='')
    {
        // class
        $this->common = new Common();
        $this->db = new DB();
        $this->json = new Json();

        $this->coId = $this->common->coId;
        $this->cdnDomain = $cdnDomain;
        // data 저장 경로
        $this->dataPath = $this->common->config['path']['data'].'/'.$this->coId;
	}

    /**
     * 게시글 목록 조회
     * 
     * @param request[id]            [필수] 게시판 ID
     * @param request[searchText]    [필수] 검색어
     * @param request[page]          [옵션] 페이지 번호
     * @param request[limit]         [옵션] 조회 갯수
     * @param request[pageNavCount]  [옵션] 페이지 nav 버튼 수
     */
    public function items($request, $totalCount=null)
    {
        // key
        if (empty($request['key'] || !in_array($request['key'], ['id','search','broadcastType','programBoardType']))) {
            return false;
        }

        $result = [];
        
        $filter['coId'] = $this->coId;
        $filter["status"] = 'publish';

        // key == id
        if ($request['key'] == 'id') {
            $ids = [];
            if (!is_array($request['id'])) $request['id'] = explode(',', trim($request['id'],','));
            $filter['id'] = ['$in'=>$request['id']];
            foreach ($request['id'] as $key => $value) {
                // 게시판 정보 조회
                $result['info'][] = $this->getBoardInfo($value);
            }
        } else {
            $filter[$request['key']] = $request['id'];
        }

        // 방송 타입
        if (!empty($request['broadcastType'])) {
            $filter['broadcastType'] = $request['broadcastType'];
        }

        // 프로그램 게시판 타입
        if (!empty($request['programBoardType'])) {
            $filter['programBoardType'] = $request['programBoardType'];
        }

        // 입력일 설정
        if (!empty($request['insert.date'])) {
            $filter['insert.date'] = $request['insert.date'];
        }
        
        // 검색어 설정
        if (!empty($request['searchText'])) {
            $filter['$text'] = ['$search' => $request['searchText']];
        }

        // sort
        $sort = ['isNotice'=>-1, 'pno'=>-1, 'depth'=>1, 'no'=>1];
        if (!empty($request['sort']) && !empty($request['order'])) {
            $sort = [$request['sort'] => intval($request['order'])];
        }

        // count
        if (empty($totalCount)) {
            $result['totalCount'] = $this->db->count(self::BOARD_COLLECTION, $filter);
        } else {
            $result['totalCount'] = $totalCount;
        }

        // page
        $page = empty($request['page'])?1:$request['page'];
        $limit = empty($request['limit'])?10:$request['limit'];
        $pageNavCount = empty($request['pageNavCount'])?10:$request['pageNavCount'];
        $classPage = new Page();
		$result['page'] = $classPage->page($limit, $pageNavCount, $result['totalCount'], $page);
        
        // 옵션
        $options = ['skip' => ($page - 1) * $limit, 'limit' => $limit, 'sort' => $sort, 'projection' => ['_id'=>0]];

        // list 조회
		$items = $this->db->list(self::BOARD_COLLECTION, $filter, $options);

        foreach ($items as $key => $value) {
            // index 계산
            $items[$key]['index'] = ($result['totalCount'] - (($page-1) * $limit)) - $key;
            $items[$key]['content'] = $this->common->convertTextContent($value['content']);

            $items[$key]['thumbnail'] = $this->common->getThumbnail($value['files']);
        }
        
        $result['items'] = $items;

        return $result;
    }

    /**
     * 게시글 조회
     * 
     * @param request[key]          [필수] id 구분자 : id
     * @param request[id]           [필수] 게시글id
     * @param request[no]           [필수] 게시글no
     */
    public function item($request)
    {
        $result = [];

        if (empty($request['id'])) {
            return $result;
        }

        // 게시판 정보 조회
        $result['info'][] = $this->getBoardInfo($request['id']);

        if (empty($request['no'])) {
            return $result;
        }

        $no = intval($request['no']);

        // list 조회
        $filter = [];
        $filter['coId'] = $this->coId;
        $filter['status'] = 'publish';
        if (empty($request['key']) || $request['key'] == 'id') {
            $filter['id'] = $request['id'];
            $filter['no'] = $no;
        }
        $options['projection'] = ['_id'=>0];
		$result['item'] = $this->db->item(self::BOARD_COLLECTION, $filter, $options);

        // 이전글
        $filter['no'] = ['$gt'=>$no];
        $options['sort'] = ['isNotice'=>1, 'pno'=>1, 'depth'=>-1,'no'=>-1];
        $result['prevItem'] = $this->db->item(self::BOARD_COLLECTION, $filter, $options);
        // 다음글
        $filter['no'] = ['$lt'=>$no];
        $options['sort'] = ['isNotice'=>-1, 'pno'=>-1, 'depth'=>1, 'no'=>1];
        $result['nextItem'] = $this->db->item(self::BOARD_COLLECTION, $filter, $options);

        // 본문
        // $result['article']['content'] = html_entity_decode($result['article']['content']);
        // $result['article']['textContent'] = $this->common->convertTextContent($result['article']['content']);
        // $result['article']['content'] = str_replace(["\r","\n"], ["","\n"], $result['article']['content']);
        // $result['article']['content'] = str_replace("../", "/", $result['article']['content']);

        return $result;
    }

    /**
     * 게시판 정보 조회
     */
    public function getBoardInfo($id)
    {
        // info 조회
        $item = $this->json->readJsonFile($this->dataPath.'/list/board', $id.'_info');

        if (empty($item)) {
            $filter = [];
            $filter['coId'] = $this->coId;
            $filter["isUse"] = true;
            $filter['id'] = $id;
            $options = [];
            $options['projection'] = ['_id'=>0];
            $item = $this->db->item(self::BOARD_INFO_COLLECTION, $filter, $options);
        }

        if (!empty($item['content'])) {
            $item['content'] = str_replace('../','/',$item['content']);
        }

        return $item;
    }
}