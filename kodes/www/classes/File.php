<?php 
namespace Kodes\Www;

//ini_set('display_errors', 1);
//ini_set('display_startup_errors', 1);
//error_reporting(E_ALL & ~E_DEPRECATED & ~E_NOTICE);
/**
 * 업로드 처리 설정
 * 
 * 업로드 파일크기 관련하여 php.ini 에서 아래 값을 확인할 것
    upload_max_filesize = 100M
    post_max_size = 110M
    max_execution_time = 600
    max_input_time = 600
 */
ini_set('memory_limit','2048M');

/**
 * 파일 클래스
 * 
 * @file
 * @author  Kodes <kodesinfo@gmail.com>
 * @version 1.0
 *
 * @license 해당 프로그램은 kodes에서 제작된 프로그램으로 저작원은 코드스(https://www.kode.co.kr)
 * https://www.kodes.co.kr
 */
class File
{
	const DS = DIRECTORY_SEPARATOR;
	const MAX_WIDTH = 1920;	// 이미지 가로 사이즈 제한
    /** @var Class DB Class */
    protected $db;
    /** @var Class Common Class */
    protected $common;
	protected $log;
    /** @var String Collection Name */
	protected $collection = "file";
    /** @var String media ID */
    protected $coId;

    public function __construct()
	{
        $this->db = new DB();
        $this->common = new Common();
		$this->log = new Log();

		$this->coId = $this->common->coId;

		$this->imageExt = ['jpg','jpeg','png','gif','bmp'];
		$this->allowUploadExt = ['jpg','jpeg','png','gif','bmp','zip','doc','docx','xls','xlsx','ppt','pptx','hwp','psd','pdf'];
    }

	/**
	 * 이미지 파일을 지정 경로로 업로드, DB저장 안함
	 */
	public function save()
	{
		try {
			$ds = self::DS;
			$global = $_POST['global'];
			// 상위경로 접근 방지를 위해 '.' 제거
			$path = (empty($_POST['path'])?'':$ds.str_replace('.','',$_POST['path']));
			if($global!='1') {
				$path = $ds.$this->coId.$ds.'upload'.$ds.'save'.$path;
			}
			if (!empty($path)) {
				$storeFolder = '/webData'.$path;
				$targetFolder = '/data'.$path;
				$targetFileName = '';
	
				if (!empty($_FILES)) {
					// File Upload
					$tempFile = $_FILES['file']['tmp_name'];
					if(!is_dir($storeFolder)){mkdir($storeFolder,0755,true);}
	
					$ext = substr(strrchr($_FILES['file']['name'],"."),1);
					$ext = strtolower($ext);
					$fileId = str_replace('.','',microtime(true));
					$targetFileName = $this->coId.$fileId;	// 해킹 방지를 위해 확장자 제거
					// $targetFileName = $this->coId.$fileId.'.'.$ext;
					$storeFilePath = $storeFolder.$ds.$targetFileName;
					$targetFilePath = $targetFolder.$ds.$targetFileName;

					// 업로드 허용 확장자 체크
					if (!in_array($ext, $this->allowUploadExt)) {
						throw new \Exception("업로드 허용되지 않은 파일입니다.", 400);
					}

					move_uploaded_file($tempFile,$storeFilePath);
					$image_info = getimagesize($storeFilePath);
	
					// 리사이징
					if (in_array($ext, ['jpg','jpeg','png','bmp'])) {
						$resizeInfo = $this->resizeMaxImage($storeFilePath, $storeFilePath, self::MAX_WIDTH);
						if($resizeInfo){
							// $this->fileInfo["size"] = $resizeInfo["size"];
							$image_info[0] = $resizeInfo["width"];
							$image_info[1] = $resizeInfo["height"];
						}
					}
	
					$data = [
						'path'=>$targetFilePath,
						'name'=>$targetFileName,
						'orgName'=>$_FILES['file']['name'],
						'width'=>$image_info[0],
						'height'=>$image_info[1],
						'size'=>$_FILES['file']['size'],
						'ext'=>$ext,
						'mimeType'=>$image_info['mime'],
						'type'=>in_array($ext, $this->imageExt)?"image":"doc",
					];
				}
			}
        } catch(\Exception $e) {
            $data['msg'] = $this->common->getExceptionMessage($e);
        }

		die(json_encode(['result'=>$data]));
	}

	/**
	 * 파일을 지정 경로로 업로드, DB저장 안함
	 */
	public function attach()
	{
		try {
			$ds = self::DS;
			// 상위경로 접근 방지를 위해 '.' 제거
			$path = (empty($_POST['path'])?'':$ds.str_replace('.','',$_POST['path']));
			if (!empty($path)) {

				$storeFolder = '/webData/'.$this->coId.$path;
				$targetFolder = '/data/'.$this->coId.$path;
				$targetFileName = '';

				if (!empty($_FILES)) {
					// File Upload
					$tempFile = $_FILES['file']['tmp_name'];
					if(!is_dir($storeFolder)){mkdir($storeFolder,0755,true);}

					// 원본파일명으로 업로드 허용하지 않음
					unset($_POST['useOrgName']);
					
					if (empty($_POST['useOrgName'])) {
						$ext = substr(strrchr($_FILES['file']['name'],"."),1);
						$ext = strtolower($ext);
						$fileId = str_replace('.','',microtime(true));
						$targetFileName = $this->coId.$fileId;	// 해킹 방지를 위해 확장자 제거
						// $targetFileName = $this->coId.$fileId.'.'.$ext;
						$storeFilePath = $storeFolder.$ds.$targetFileName;
						$targetFilePath = $targetFolder.$ds.$targetFileName;
					} else {
						$targetFileName = $_FILES['file']['name'];
						$storeFilePath = $storeFolder.$ds.$targetFileName;
						$targetFilePath = $targetFolder.$ds.$targetFileName;
					}

					// 업로드 허용 확장자 체크
					if (!in_array($ext, $this->allowUploadExt)) {
						throw new \Exception("업로드 허용되지 않은 파일입니다.", 400);
					}

					move_uploaded_file($tempFile,$storeFilePath);

					$data = [
						'path'=>$storeFilePath,
						'name'=>$targetFileName,
						'orgName'=>$_FILES['file']['name'],
						'ext'=>$ext,
						'type'=>in_array($ext, $this->imageExt)?"image":"doc",
					];
				}
			}
        } catch(\Exception $e) {
            $data['msg'] = $this->common->getExceptionMessage($e);
        }

		die(json_encode(['result'=>$data]));
	}

	/**
     * 이미지 제한 크기로 리사이즈
     * 이미지 비율은 유지
     * 
     * @param sourceFileStore 원본 파일 (절대경로 권장)
     * @param targetFileStore 생성 파일 (절대경로 권장)
     * @param maxImageLength 이미지 길이 제한
     */
    protected function resizeMaxImage($sourceFileStore, $targetFileStore, $maxImageLength=1920)
	{
        list($width, $height) = getimagesize($sourceFileStore);

        // 이미지의 긴 방향이 길이 제한보다 큰지 판단
        if($width >= $height && $width > $maxImageLength){
            $resizeWidth = $maxImageLength;
        }else if($width < $height && $height > $maxImageLength){
            $resizeHeight = $maxImageLength;
        }

        if($resizeWidth || $resizeHeight){
            // 사이즈 계산
            $src_x = 0;
            $src_y = 0;
            if($resizeWidth && $resizeHeight){
                $wRate = $resizeWidth / $width;
                $hRate = $resizeHeight / $height;
                // 이미지 비율 유지
                if($wRate > $hRate){
                    $rate =  $wRate;
                    $src_y = ( ( $height * $rate ) - $resizeHeight ) / 2;
                }else{
                    $rate =  $hRate;
                    $src_x = ( ( $width * $rate ) - $resizeWidth ) / 2;
                }
            }else if($resizeWidth){
                $rate = $resizeWidth / $width;
                $resizeHeight = $height * $rate;
            }else if($resizeHeight){
                $rate = $resizeHeight / $height;
                $resizeWidth = $width * $rate;
            }

            // 이미지 리사이즈
            $this->resizeImage($sourceFileStore, $targetFileStore, $src_x, $src_y, ($width*$rate), ($height*$rate), $width, $height);

            // 리사이즈 이후 값 설정
            $fileInfo["size"] = filesize($targetFileStore);
            $fileInfo["width"] = $resizeWidth;
            $fileInfo["height"] = $resizeHeight;
            return $fileInfo;
        }
        return false;
    }

    /**
     * 이미지 리사이즈
     */
    protected function resizeImage($file, $newfile, $src_x, $src_y, $dst_w, $dst_h, $width='', $height='')
	{
        if(empty($width) || empty($height)){
            list($width, $height) = getimagesize($file);
        }
		$dst_image = imagecreatetruecolor($dst_w, $dst_h);
        $imageType = exif_imagetype($file);
        switch ($imageType){
            case 1:
                $src_image = imagecreatefromgif($file);
                break;
            case 2:
                $src_image = imagecreatefromjpeg($file);
                break;
            case 3:
				@imagealphablending($dst_image, false);
                @imagesavealpha($dst_image, true);
                $src_image = imagecreatefrompng($file);
                break;
        }
        imagecopyresampled($dst_image, $src_image, 0, 0, $src_x, $src_y, $dst_w, $dst_h, $width, $height);
        switch ($imageType){
            case 1:
                imagegif($dst_image, $newfile);
                break;
            case 2:
                imagejpeg($dst_image, $newfile, 100);   // quality 설정
                break;
            case 3:
                imagepng($dst_image, $newfile);
                break;
        }
        imagedestroy($src_image);
        imagedestroy($dst_image);
    }

	/**
	 * 파일 다운로드
	 * 디운로드 url : /file/download?path=[파일경로]&orgName=[원본파일명]
	 * 
	 * @param path 파일경로
	 * @param orgName 원본파일명
	 * 
	 */
	public function download()
	{
		$path = trim($_GET['path'], '.');
		$orgName = $_GET['orgName'];

		// path가 없으면 실행하지 않음
		if (empty($path)) exit;
		// orgName이 없으면 path의 파일명으로 다운로드
		if (empty($orgName)) {
			$orgName = basename($path);
		}
		$file_name = str_replace("/data","/webData",$path);

		// make sure it's a file before doing anything!
		if(!is_file($file_name)) exit;

		// required for IE
		if(ini_get('zlib.output_compression')) 
			ini_set('zlib.output_compression', 'Off');	

		$mime = mime_content_type('$file_name');

		header("Pragma: public");
		header("Expires: 0");
		header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
		header('Last-Modified: '.gmdate ('D, d M Y H:i:s', filemtime($file_name)).' GMT');
		header("Content-Type: application/octet-stream");
		header("Content-Disposition: attachment; filename=".$orgName);
		header("Content-Transfer-Encoding: binary");
		header("Content-Length: ".filesize($file_name));

		ob_clean();
		flush();
		readfile($file_name);
		exit;
	}

	/**
	 * 매체별 파일을 일기위한 함수
	 */
	public function printFile(){
		// 상위 폴더를 담색하려고 하면 오류 처리
		if(preg_match("/[.]{2,}[\/]/i",$_GET['fileName'])){
			$this->putHeader('404');
		}

		$path = "/webData/".$this->coId."/".$_GET['fileName'];
		if(!is_file($path)){
			$path = "/webSiteSource/www/web/".$_GET['fileName'];
		}
		if(!is_file($path)){
			$this->putHeader('404');
		}
		readfile($path);
	}

	/**
	 * 404 header 처리
	 */
	public function putHeader($errorCode){
		$httpCode =['403'=>'Forbidden','404'=>'Not Found','500'=>'Internal Server Error'];
		header('HTTP/1.0 '.$errorCode.' '.$httpCode['$errorCode']);
		die();
	}
}