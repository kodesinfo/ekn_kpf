<?php
namespace Kodes\Www;

// ini_set('display_errors', 1);

/**
 * Login 클래스
 * 
 * @author  Kodes <kodesinfo@gmail.com>
 * @version 1.0
 *
 * @license 해당 프로그램은 kodes에서 제작된 프로그램으로 저작원은 코드스(https://www.kode.co.kr)
 */
class Login
{
    /** @var Class Common Class */
	protected $common;
	protected $json;
	protected $member;

    /**
	 * 생성자
	 */
    public function __construct()
	{
		// class
		$this->common = new Common();
		$this->json = new Json();
		$this->member = new Member();

		// variable
        $this->coId = $this->common->coId;
    }

    /**
     * 로그인 화면
     */
    public function login()
    {
        $result = [];
		try {
			if ($_SESSION['memberId']) {
				header("Location: /");
			}
            // 소셜로그인 api url (가입 후 로그인 가능)
            // $result['naverApiURL'] = $this->member->getNaverApiURL('login');
            // $result['kakaoApiURL'] = $this->member->getKakaoApiURL('login');
            // $result['googleApiURL'] = $this->member->getGoogleApiURL('login');
            // 소셜로그인 api url (가입 없이 로그인 가능)
            $result['naverApiURL'] = $this->member->getNaverApiURL('login', 'withoutJoin');
            $result['kakaoApiURL'] = $this->member->getKakaoApiURL('login', 'withoutJoin');
            $result['googleApiURL'] = $this->member->getGoogleApiURL('login', 'withoutJoin');
        } catch(\Exception $e) {
            $result['msg'] = $this->common->getExceptionMessage($e);
        }
        // print_r($result);
        return $result;
    }
}