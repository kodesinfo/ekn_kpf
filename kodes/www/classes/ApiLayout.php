<?php
namespace Kodes\Www;

// ini_set('display_errors', 1);

/**
 * ApiLayout 클래스
 * api에서 레이아웃을 조회하는 데 사용
 */
class ApiLayout
{
	/** @var Class 공통 */
	protected $common;
	protected $json;
    protected $tpl;

	/** @var variable */
	public $coId;
	protected $deviceType;
	protected $mAids = [];	// 수동편집 aid
	protected $cdnDomain = '';	// cdn 도메인
	protected $prefixLayout;
	protected $prefixBox;
	protected $articleView = null;	// 기사뷰

	/**
     * Json 생성자
     */
    public function __construct($deviceType='pc', $cdnDomain='')
    {
		// class
        $this->common = new Common();
        $this->json = new Json();

		$this->coId = $this->common->coId;
		$this->deviceType = empty($deviceType)?'pc':strtolower($deviceType);
		$this->cdnDomain = $cdnDomain;

		$this->dataPath = $this->common->config['path']['data'].'/'.$this->coId;
		$this->company = $this->common->getCompany();
		$this->category = $this->common->getCategory();
		$this->categoryTree = $this->common->getCategoryTree();
		$this->boardInfoList = $this->common->getBoardInfoList();
	}

	/**
	 * 면편집 데이터 조회
	 * 
	 * @param request[id]           [필수] 페이지ID
     * @param request[dataType]     [필수] 데이터 타입 : json, html
     * @param request[contentType]  [필수] 컨텐츠 타입 : layout, box
     * @param request[deviceType]   [필수] 디바이스 타입 : pc(default), mobile
     * @param request[boxId]        [선택] 영역ID, contentType=box 인 경우 필수 : 구분자(,)
     * @param request[prefixLayout] [선택] layout 미리보기 prefix
     * @param request[prefixBox]    [선택] box 미리보기 prefix
     * @param request[useRalation]  [선택] 관련기사 사용여부 : 1
	 * @param request[noAd]         [선택] 배너없음 : 1
     * @param request[listIdType]   [선택] (기사리스트 전용) 리스트 타입 : 리스트ID의 종류 (categoryId, seriseId, programId, reporterId, tag)
     * @param request[listId]       [선택] (기사리스트 전용) 리스트 ID
	 * @param request[page]      	[선택] (기사리스트 전용) page 페이지 번호
	 * @param request[limit]      	[선택] (기사리스트 전용) 리스트 조회 수
     * @param request[aid]          [선택] (기사뷰 전용) 기사ID
	 */
	public function getLayout($request)
	{
			// 미리보기
		$this->prefixLayout = empty($request['prefixLayout'])?'':$request['prefixLayout'];
		$this->prefixBox = empty($request['prefixBox'])?'':$request['prefixBox'];

		// box용 템플릿 class
		$template_dir = 'data/'.$this->coId.'/template';
		$compile_dir = '_compile/'.$this->coId.'/layout';
		$tpl = $this->common->setTemplate($template_dir, $compile_dir);

		$response = [];

		$id = $request['id'];
        $request['dataType'] = empty($request['dataType'])?'html':$request['dataType'];

        $layoutData = $this->makeLayoutData($this->prefixLayout.$id, '', $request);

        if ($request['dataType'] == 'html' && $request['contentType'] == 'layout') {
            // html, layout
			$template = $id.'.html';
            if (is_file('./'.$tpl->template_dir.'/'.$template)) {
				$tpl->define('box', $template);
				$tpl->assign($layoutData);
				$response['html'] = $tpl->fetch('box');
            }

        } else {
			// html, box 이거나 json 인 경우
            $response['items'] = [];
			if (!empty($request['boxId'])) {
				$boxId = explode(',',$request['boxId']);
			}
            foreach ($layoutData as $key => $value) {
				if (empty($value)) {
					continue;
				}
                if (empty($request['boxId']) || in_array($key, $boxId)) {
                    $response['items'][$key]['boxId'] = $key;
                    if ($request['dataType'] == 'html') {
						$response['items'][$key]['html'] = $value;
                    } else {
						$response['items'][$key]['objType'] = $value['objType'];
                        if (!empty($value['listType'])) $response['items'][$key]['listType'] = $value['listType'];
						$response['items'][$key]['title'] = $value['title'];
                        $response['items'][$key]['moreUrl'] = empty($value['moreurl'])?(empty($value['moreUrl'])?'':$value['moreUrl']):$value['moreurl'];
                        $response['items'][$key]['items'] = $value['items'];
                    }
                }
            }
        }
		return $response;
	}

	/**
	 * 레이아웃 데이터 생성
	 */
	protected function makeLayoutData($pageId, $currentCategoryId="", $request)
	{
		// 초기화
		$list = [];
		$this->articleView = null;
		
		$layoutInfo = $this->getLayoutInfo($pageId);

		if (empty($layoutInfo) || !is_array($layoutInfo)) {
			return $list;
		}

		foreach ($layoutInfo as $item) {

			if ($item['objType'] == 'din') {
				continue;
			}

			// showpc, showmobile 기존 데이터 처리
			if (empty($item['showPC'])) {
				if (!empty($item['showPc'])) {
					$item['showPC'] = $item['showPc'];
				} elseif (!empty($item['showpc'])) {
					$item['showPC'] = $item['showpc'];
				}
			}
			if (empty($item['showMobile']) && !empty($item['showmobile'])) {
				$item['showMobile'] = $item['showmobile'];
			}

			// pc노출, mobile노출 처리
			if (!empty($item['showPC']) || !empty($item['showMobile'])) {
				if (($this->deviceType=='pc'?$item['showPC']:$item['showMobile']) == 'N') {
					$list[$item['objId']] = '';
					continue;
				}
			}

			$box = '';
			if ($item['objType'] == "article") {
				// 기사
				$box = $this->makeArticleBox($item, $currentCategoryId, $request['dataType'], 'M');
			} elseif ($item['objType'] == "banner") {
				// 배너
				if (!empty($request['noAd'])) continue; // 배너 없음
				$box = $this->makeBannerBox($item, $request['dataType']);
			} elseif ($item['objType'] == "html") {
				// html
				if (!empty($request['noAd'])) continue; // 배너 없음
				$box = $this->makeHtmlBox($item, $request['dataType']);
			} elseif ($item['objType'] == "articleList") {
				if (!empty($request['listId'])) {
					// 기사리스트
					$box = $this->makeArticleList($item, $request);
				}
			} elseif ($item['objType'] == "articleView") {
				if (!empty($request['aid'])) {
					// 기사뷰
					$box = $this->makeArticleView($item, $request);
				}
			} elseif ($item['objType'] == "company") {
				// 회사정보
				$box = $this->makeCompany($item, $request['dataType']);
			} elseif ($item['objType'] == "board") {
				// 게시판
				$box = $this->makeBoardBox($item, $request['dataType']);
			}
			if (!empty($box)) {
				$list[$item['objId']] = $box;
			}
		}

		// 수동편집기사 제거를 위해 자동기사편집영역은 마지막에 처리
		foreach ($layoutInfo as $item) {

			if ($item['objType'] == 'din') {
				continue;
			}
			if ($item['objType'] != "article" || $item['listType'] != 'A') {
				continue;
			}

			// showpc, showmobile 기존 데이터 처리
			if (empty($item['showPC'])) {
				if (!empty($item['showPc'])) {
					$item['showPC'] = $item['showPc'];
				} elseif (!empty($item['showpc'])) {
					$item['showPC'] = $item['showpc'];
				}
			}
			if (empty($item['showMobile']) && !empty($item['showmobile'])) {
				$item['showMobile'] = $item['showmobile'];
			}

			// pc노출, mobile노출 처리
			if (!empty($item['showPC']) || !empty($item['showMobile'])) {
				if (($this->deviceType=='pc'?$item['showPC']:$item['showMobile']) == 'N') {
					$list[$item['objId']] = '';
					continue;
				}
			}

			$box = "";
			if ($item['objType'] == "article") {
				$box = $this->makeArticleBox($item, $currentCategoryId, $request['dataType'], 'A');
			}
			if (!empty($box)) {
				$list[$item['objId']] = $box;
			}
		}

		return $list;
	}

	/**
	 * 면편집 정보 조회
	 */
	protected function getLayoutInfo($pKind)
	{
		return $this->json->readJsonFile($this->dataPath."/layout", $pKind."Info");
	}

	/**********************************************************************************
     * Box
     */

	/**
	 * article Box 생성
	 */
	protected function makeArticleBox($data, $currentCategoryId="", $dataType="html", $listType='A')
	{
		// 스킨이 있을때만 처리
		$skin = empty($data['pcSkin'])?null:$data['pcSkin'];
		if ($this->deviceType == 'mobile') $skin = empty($data['mobileSkin'])?null:$data['mobileSkin'];
		if (!empty($skin)) {
			$box = $data;

			// pc/mobile 설정
			$box['skin'] = $skin;
			$box['showArticleNum'] = $this->deviceType=='pc'?$box['showArticleNumPC']:$box['showArticleNumMobile'];
			$box['contentCutNum'] = $this->deviceType=='pc'?$box['contentCutNumPC']:$box['contentCutNumMobile'];
			$box['showThumb'] = $this->deviceType=='pc'?$box['showThumbPC']:$box['showThumbMobile'];
			$box['thumbWidth'] = $this->deviceType=='pc'?$box['thumbWidthPC']:$box['thumbWidthMobile'];
			$box['thumbHeight'] = $this->deviceType=='pc'?$box['thumbHeightPC']:$box['thumbHeightMobile'];

			// 기본값
			// $box['startNum'] = 0;	// 필요시 추가
			$box['showArticleNum'] = empty($box['showArticleNum'])?0:$box['showArticleNum'];
			$box['contentCutNum'] = empty($box['contentCutNum'])?150:$box['contentCutNum'];
			$box['removeHeadline'] = empty($box['removeHeadline'])?'Y':$box['removeHeadline'];
			$box['onlyImage'] = empty($box['onlyImage'])?'N':$box['onlyImage'];
			$box['onlyVideo'] = empty($box['onlyVideo'])?'N':$box['onlyVideo'];
			$box['type_isFlash'] = empty($box['type_isFlash'])?'N':$box['type_isFlash'];
			$box['articleMerge'] = empty($box['articleMerge'])?'':$box['articleMerge'];
			$box['removeSquareBrackets'] = empty($box['removeSquareBrackets'])?'N':$box['removeSquareBrackets'];
			$box['conCapDel'] = empty($box['conCapDel'])?'Y':$box['conCapDel'];	// 본문 자를 때 캡션 제거 여부
			$box['showThumb'] = empty($box['showThumb'])?'Y':$box['showThumb'];
			$box['thumbWidth'] = empty($box['thumbWidth'])?'':$box['thumbWidth'];
			$box['thumbHeight'] = empty($box['thumbHeight'])?'':$box['thumbHeight'];
			$box['useRelation'] = (!empty($box['useRelation']) && $box['useRelation']=='Y')?'1':'';
			$box['currentCategoryId'] = $currentCategoryId;
			// @todo 사용여부 확인 후 삭제
			// $box['day'] = empty($box['day'])?'':$box['day'];
			// $box['popularTermSection'] = empty($box['popularTermSection'])?'':$box['popularTermSection'];
			// $box['currentCategoryId'] = empty($box['currentCategoryId'])?'':$box['currentCategoryId'];

			if ($listType == 'M' && $box['listType'] == $listType) {
				// 수동편집
				$readId = $this->prefixBox.$box['objId'];
				$temp = $this->manualArticleList($readId, $box, []);
				$data['items'] = empty($temp['items'])?[]:$temp['items'];
				$data["moreUrl"] = $this->getMoreUrl($data);

				if (!empty($data['items']) && is_array($data['items']) && count($data['items']) > 0) {
					$this->mAids = array_merge($this->mAids, array_column($data['items'], 'aid'));	// 수동기사 aid 수집
				}

                if ($dataType == 'json') {
                    return $data;
                }

				return $this->boxSetting($box['skin'], $data);

			} elseif ($listType == 'A' && $box['listType'] == $listType) {
				$temp = [];
				// 자동
				if ($box['autoGroupType']=="S") {
					// 시리즈
					$temp = $this->getArticles('seriesId', $box['series'], $box, $this->mAids);
					$data['items'] = empty($temp['items'])?[]:$temp['items'];
					$data['info'] = empty($temp['info'])?[]:$temp['info'];

				} elseif ($data['autoGroupType']=="P") {
					// 프로그램
					$temp = $this->getArticles('programId', $box['program'], $box, $this->mAids);
					$data['items'] = empty($temp['items'])?[]:$temp['items'];
					$data['info'] = empty($temp['info'])?[]:$temp['info'];
				} elseif ($data['autoGroupType']=="T") {
					// 태그
					$temp = $this->getArticles('tag', $box['tags'], $box, $this->mAids);
					$data['items'] = empty($temp['items'])?[]:$temp['items'];
					$data['info'] = empty($temp['info'])?[]:$temp['info'];
				} elseif ($data['autoGroupType']=="C") {
					// 카테고리
					$temp = $this->getArticles('categoryId', $box['category'], $box, $this->mAids);
					$data['items'] = empty($temp['items'])?[]:$temp['items'];
					$data['info'] = empty($temp['info'])?[]:$temp['info'];
				} elseif ($data['autoGroupType']=="A") {
					// 인기기사(analytics)
					$temp = $this->getArticles('analytics', $box['analytics'], $box, $this->mAids);
					$data['items'] = empty($temp['items'])?[]:$temp['items'];
				}elseif ($data['autoGroupType']=="Y") {
					// 유튜브 리스트
					$temp = $this->getArticles('youtube', [], $boxs);
					$data['items'] = empty($temp['items'])?[]:$temp['items'];
				}

				if (!empty($temp['Data-Source'])) $data['Data-Source'] = $temp['Data-Source'];
				if (!empty($temp['Last-Modified'])) $data['Last-Modified'] = $temp['Last-Modified'];
				if (!empty($temp['Running-Time'])) $data['Running-Time'] = $temp['Running-Time'];

				$data["moreUrl"] = $this->getMoreUrl($data);
				$data["showArticleNum"] = $box["showArticleNum"];

                if ($dataType == 'json') {
                    return $data;
                }

				return $this->boxSetting($box['skin'], $data);
			}
		}
	}

	/**
	 * banner Box 생성
	 */
	protected function makeBannerBox($data, $dataType="html")
	{
		if (empty($skin)) $skin = 'banner';	// 기본스킨 적용

		if (!empty($skin)) {
			$readId = $this->prefixBox.$data['objId'];
			$data['items'] = $this->readManualFile($readId);

			// 노출조건 확인하여 제외
			if (!empty($data['items'])) {
				$now = date("Y-m-d H:i:s");
				$list = [];
				foreach ($data['items'] as $key => $item) {
					if (!empty($item)) {
						// 배너 조회
						if (!empty($item['bnid'])) {
							$item['info'] = $this->getBannerJson($item['bnid']);
						}

						$isView = true;
						if (!empty($item['info']['startTime']) && strtotime($item['info']['startTime']) > strtotime($now)) {
							$isView = false;
						}
						if (!empty($item['info']['endTime']) && strtotime($item['info']['endTime']) < strtotime($now)) {
							$isView = false;
						}
						if (empty($item['info']['deviceType']) || !is_array($item['info']['deviceType']) || !in_array($this->deviceType, $item['info']['deviceType'])) {
							$isView = false;
						}

						if ($isView) {
							// cdn 적용 : files.path
							if (!empty($this->cdnDomain)) {
								foreach ($item['info']['files'] as $key => $value) {
									if (!empty($value['path']) && strpos($value['path'], $this->cdnDomain) === false) {
										$item['info']['files'][$key]['path'] = $this->cdnDomain.$value['path'];
									}
								}
							}
							$list[] = $item;
						}
					}
				}
				$data['items'] = $list;
			}

			if ($dataType == "json") {
				return $data;
			}

			return $this->boxSetting($skin, $data);
		}
	}

	/**
	 * 배너 json 조회
	 */
	protected function getBannerJson($id)
	{
		$path = $this->dataPath."/banner";
		return $this->json->readJsonFile($path, $id);
	}

	/**
	 * html Box 생성
	 */
	protected function makeHtmlBox($data, $dataType="html")
	{
		if (empty($skin)) $skin = 'html';	// 기본스킨 적용

		if (!empty($skin)) {
			$readId = $this->prefixBox.$data['objId'];
			$data['content'] = str_replace('../data/','/data/', $data['content']);
			$data['items'] = $this->json->readJsonFile($this->dataPath."/layout/box", $readId);
			if ($dataType == "json") {
				return $data;
			}
			return $this->boxSetting($skin, $data);
		}
	}

	/**
	 * articleList Box 생성
	 */
	protected function makeArticleList($data, $request)
	{
		// 스킨이 있을때만 처리
		$skin = empty($data['pcSkin'])?null:$data['pcSkin'];
		if ($this->deviceType == 'mobile') $skin = empty($data['mobileSkin'])?null:$data['mobileSkin'];
		if (!empty($skin)) {
			$box = $data;
			$box['skin'] = $skin;
			// pc/mobile 설정
			$box['showArticleNum'] = $this->deviceType=='pc'?$box['showArticleNumPC']:$box['showArticleNumMobile'];
			$box['contentCutNum'] = $this->deviceType=='pc'?$box['contentCutNumPC']:$box['contentCutNumMobile'];
			$box['showThumb'] = $this->deviceType=='pc'?$box['showThumbPC']:$box['showThumbMobile'];
			$box['thumbWidth'] = $this->deviceType=='pc'?$box['thumbWidthPC']:$box['thumbWidthMobile'];
			$box['thumbHeight'] = $this->deviceType=='pc'?$box['thumbHeightPC']:$box['thumbHeightMobile'];
			// 기본값
			$box['showArticleNum'] = empty($box['showArticleNum'])?0:$box['showArticleNum'];
			$box['contentCutNum'] = empty($box['contentCutNum'])?150:$box['contentCutNum'];
			// $box['removeHeadline'] = empty($box['removeHeadline'])?'Y':$box['removeHeadline'];
			$box['onlyImage'] = empty($box['onlyImage'])?'N':$box['onlyImage'];
			$box['onlyVideo'] = empty($box['onlyVideo'])?'N':$box['onlyVideo'];
			$box['type_isFlash'] = empty($box['type_isFlash'])?'N':$box['type_isFlash'];
			$box['articleMerge'] = empty($box['articleMerge'])?'':$box['articleMerge'];
			// $box['removeSquareBrackets'] = empty($box['removeSquareBrackets'])?'N':$box['removeSquareBrackets'];
			$box['conCapDel'] = empty($box['conCapDel'])?'Y':$box['conCapDel'];	// 본문 자를 때 캡션 제거 여부
			$box['showThumb'] = empty($box['showThumb'])?'Y':$box['showThumb'];
			$box['thumbWidth'] = empty($box['thumbWidth'])?'':$box['thumbWidth'];
			$box['thumbHeight'] = empty($box['thumbHeight'])?'':$box['thumbHeight'];
			// $box['useRelation'] = empty($box['useRelation'])?'':$box['useRelation'];
			$data = $this->getArticleList($request, $box);

			$data["showArticleNum"] = $box["showArticleNum"];

			if ($request['dataType'] == 'json') {
				return $data;
			}
			return $this->boxSetting($box['skin'], $data);
		}
	}

	/**
	 * articleList 데이터 조회
	 */
	protected function getArticleList($req, $box)
	{
		if (empty($req['listId'])) return;
		$id = $req['listId'];

		// listIdType
		if (empty($req['listIdType'])) $req['listIdType'] = $this->common->getContentType($req['listId']);

		if ($req['listIdType'] != 'search') {
			if (!is_array($id)) $id = explode(',', $id);
		}

		// api 조회
		$request = [];
		$request['key'] = $req['listIdType'];
		$request['id'] = $id;
		if (!empty($req['inImage'])) {
			$request['inImage'] = $req['inImage'];
		} else {
			$request['inImage'] = $box['onlyImage'];
		}
		if (!empty($req['inVideo'])) {
			$request['inVideo'] = $req['inVideo'];
		} else {
			$request['inVideo'] = $box['onlyVideo'];
		}
		$request['type_isFlash'] = $box['type_isFlash'];
		$request['page'] = $req['page'];
		$request['limit'] = empty($req['limit'])?$box['showArticleNum']:$req['limit'];
		// $request['excludeAutoPlace'] = 1;
		// if ($box['removeHeadline'] == 'Y') $request['iniAids'] = $mAids;
		// $request['noCache'] = '1';
		$articleList = $this->getArticlesFromApi($request);
		if (!empty($articleList['items'])) {
			$articleList['items'] = $this->workArticleList($articleList['items'], $box);
		}

		$articleList['id'] = $id[0];
		$articleList['company'] = $this->company;
		$articleList['categoryTree'] = $this->categoryTree;

		return $articleList;
	}

	/**
	 * articleView Box 생성
	 */
	protected function makeArticleView($data, $request)
	{
		// 스킨이 있을때만 처리
		$skin = empty($data['pcSkin'])?null:$data['pcSkin'];
		if ($this->deviceType == 'mobile') $skin = empty($data['mobileSkin'])?null:$data['mobileSkin'];
		if (!empty($skin) && !empty($request['aid'])) {
			$box['skin'] = $skin;

			$data = $this->getArticleView($request['aid']);
			
			if ($request['dataType'] == 'json') {
				return $data;
			}

			return $this->boxSetting($box['skin'], $data);
		}
	}

	/**
	 * articleView 데이터 조회
	 */
	protected function getArticleView($aid)
	{
		if (!empty($this->articleView)) return $this->articleView;

		if (empty($_POST['title'])) {
			// api 기사 뷰 조회
			$request = [];
			$request['id'] = $aid;
			// $request['noCache'] = '1';	// 캐쉬 미사용(1)
			$api = new Api();
			$this->articleView = $api->data('getArticle', $request);
		} else {
			// 미리보기
			Header('X-XSS-Protection: 0');
			$this->articleView['article'] = $_POST;
			unset($this->articleView['article']['aid']);
			$this->articleView['article']['status'] = 'publish';
			$this->articleView['article']['content'] = str_replace('../', '/', $this->articleView['article']['content']);
			$this->articleView['article']['tags'] = explode(",", $this->articleView['article']["tags"]);
			// 네비게이션 조회
			$articleClass = new ApiArticle();
			$this->articleView['categoryNav'] = array_reverse($articleClass->getNavigationList($this->articleView['article']["category"][0]));
			$this->articleView['article']['category'] = $this->articleView['categoryNav'];
			// 파일 정리
			foreach ($this->articleView['article']['file_path'] as $key => $value) {
				$this->articleView['article']['files'][]=[
					'id'=>$_POST['file_id'][$key],
					'path'=>$value,
					'ext'=>$_POST['file_ext'][$key],
					'type'=>$_POST['file_type'][$key],
					'width'=>$_POST['file_width'][$key],
					'height'=>$_POST['file_height'][$key],
					'size'=>$_POST['file_size'][$key],
					'orgName'=>$_POST['file_orgName'][$key],
					'watermarkPlace'=>$_POST['file_watermarkPlace'][$key]
				];
			}
			if (!empty($_POST['relationId'])) {
				$dir = $this->dataPath.'/list/relation';
				$this->articleView['article']['relation'] = $this->json->readJsonFile($dir, $_POST['relationId']);
			}
			$this->articleView['article']['temp'] = 'preview';
		}

		$this->articleView['company'] = $this->company;

		// 소셜로그인 api url
		$member = new Member();
		$this->articleView['naverApiURL'] = $member->getNaverApiURL('login', 'withoutJoin');
		$this->articleView['kakaoApiURL'] = $member->getKakaoApiURL('login', 'withoutJoin');
		$this->articleView['googleApiURL'] = $member->getGoogleApiURL('login', 'withoutJoin');

		return $this->articleView;
	}

	/**
	 * company Box 생성
	 */
	protected function makeCompany($data, $dataType="html")
	{
		// 스킨이 있을때만 처리
		$skin = empty($data['pcSkin'])?null:$data['pcSkin'];
		if ($this->deviceType == 'mobile') $skin = empty($data['mobileSkin'])?null:$data['mobileSkin'];
		if (!empty($skin)) {
			$box['skin'] = $skin;

			$data['company'] = $this->company;
			$data['category'] = $this->category;
			$data['categoryTree'] = $this->categoryTree;
			$data['eventList'] = $this->json->readJsonFile($this->dataPath.'/event', "eventList");
			$data['eventList_m'] = $this->json->readJsonFile($this->dataPath.'/event', "eventList_m");
			
			if ($dataType == 'json') {
				return $data;
			}

			return $this->boxSetting($box['skin'], $data);
		}
	}

	/**
	 * board Box 생성
	 */
	protected function makeBoardBox($data, $dataType="html")
	{
		// 스킨이 있을때만 처리
		$skin = empty($data['pcSkin'])?null:$data['pcSkin'];
		if ($this->deviceType == 'mobile') $skin = empty($data['mobileSkin'])?null:$data['mobileSkin'];
		if (!empty($skin)) {
			$box = $data;

			// pc/mobile 설정
			$box['skin'] = $skin;
			$box['showArticleCount'] = $this->deviceType=='pc'?$box['showArticleCountPC']:$box['showArticleCountMobile'];
			$box['contentCutNum'] = $this->deviceType=='pc'?$box['contentCutNumPC']:$box['contentCutNumMobile'];
			$box['showThumb'] = $this->deviceType=='pc'?$box['showThumbPC']:$box['showThumbMobile'];
			$box['thumbWidth'] = $this->deviceType=='pc'?$box['thumbWidthPC']:$box['thumbWidthMobile'];
			$box['thumbHeight'] = $this->deviceType=='pc'?$box['thumbHeightPC']:$box['thumbHeightMobile'];

			// 기본값
			// $box['startNum'] = 0;	// 필요시 추가
			$box['showArticleCount'] = empty($box['showArticleCount'])?0:$box['showArticleCount'];
			$box['contentCutNum'] = empty($box['contentCutNum'])?150:$box['contentCutNum'];
			$box['onlyImage'] = empty($box['onlyImage'])?'N':$box['onlyImage'];
			$box['listMerge'] = empty($box['listMerge'])?'':$box['listMerge'];
			$box['listSort'] = empty($box['listSort'])?'':$box['listSort'];
			$box['showThumb'] = empty($box['showThumb'])?'Y':$box['showThumb'];
			$box['thumbWidth'] = empty($box['thumbWidth'])?'':$box['thumbWidth'];
			$box['thumbHeight'] = empty($box['thumbHeight'])?'':$box['thumbHeight'];

			if ($box['listType'] == 'M') {
				// 수동편집
				$readId = $this->prefixBox.$box['objId'];
				$temp = $this->manualBoardList($readId, $box);
				$data['items'] = empty($temp['items'])?[]:$temp['items'];
				$data["moreUrl"] = empty($data["moreUrl"])?'':$data["moreUrl"];

                if ($dataType == 'json') {
                    return $data;
                }

				return $this->boxSetting($box['skin'], $data);

			} elseif ($box['listType'] == 'A') {
				$temp = [];
				// 자동
				$temp = $this->getBoards('id', $box['boardId'], $box);
				$data['items'] = empty($temp['items'])?[]:$temp['items'];
				$data['info'] = empty($temp['info'])?[]:$temp['info'];

				if (!empty($temp['Data-Source'])) $data['Data-Source'] = $temp['Data-Source'];
				if (!empty($temp['Last-Modified'])) $data['Last-Modified'] = $temp['Last-Modified'];
				if (!empty($temp['Running-Time'])) $data['Running-Time'] = $temp['Running-Time'];

				$data["moreUrl"] = empty($data["moreUrl"])?'/board/list/'.(explode(",",$val["boardId"])[0]):$data["moreUrl"];
				$data["showArticleNum"] = $box["showArticleNum"];

                if ($dataType == 'json') {
                    return $data;
                }

				return $this->boxSetting($box['skin'], $data);
			}
		}
	}

	/**********************************************************************************
     * 데이터
     */

	/**
	 * 기사 리스트 가공
	 */
	protected function workArticleList($data, $box, $mAids=[])
	{
		if (empty($data)) {
			return [];
		}

		$box['showArticleNum'] = empty($box['showArticleNum'])?0:$box['showArticleNum'];
		$box['removeHeadline'] = empty($box['removeHeadline'])?'Y':$box['removeHeadline'];
		$box['onlyImage'] = empty($box['onlyImage'])?'N':$box['onlyImage'];
		$box['onlyVideo'] = empty($box['onlyVideo'])?'N':$box['onlyVideo'];
		$box['type_isFlash'] = empty($box['type_isFlash'])?'N':$box['type_isFlash'];
		
		// 단일 리스트
		$return = [];
		$index = 0;
		foreach ($data as $key => $value) {
			if($index >= $box['showArticleNum']) break;

			// 수동편집 기사 제거
			if ($box['removeHeadline']=="Y" && is_array($mAids) && !empty($value['aid'])) {
				if (in_array($value['aid'], $mAids)) {
					continue;
				}
			}

			// 이미지 있는 기사만
			if ($box['onlyImage']=="Y" && empty($value['thumbnail'])) {
				continue;
			}

			// 동영상 있는 기사만
			if ($box['onlyVideo']=="Y" && empty($value['inVideo'])) {
				continue;
			}

			// 속보
			if ($box['type_isFlash']=="Y" && empty($value['type']['isFlash'])) {
				continue;
			}

			$return[$index] = $this->workArticleData($value, $box);
			$index++;
		}

		return $return;
	}

	/**
	 * 기사 데이터 가공
	 */
	protected function workArticleData($data, $box)
	{
		if (empty($data)) {
			return [];
		}

		$box['removeSquareBrackets'] = empty($box['removeSquareBrackets'])?'N':$box['removeSquareBrackets'];
		$box['contentCutNum'] = empty($box['contentCutNum'])?150:$box['contentCutNum'];
		$box['conCapDel'] = empty($box['conCapDel'])?'Y':$box['conCapDel'];
		$box['showThumb'] = empty($box['showThumb'])?'Y':$box['showThumb'];
		$box['thumbWidth'] = empty($box['thumbWidth'])?'':$box['thumbWidth'];
		$box['thumbHeight'] = empty($box['thumbHeight'])?'':$box['thumbHeight'];

		// title
		$data['title'] = empty($data["expTitle"])?$data["title"]:$data["expTitle"];
		// 면편집 제목이 있으면 적용
		if ($this->deviceType == 'pc' && !empty($data["layoutTitle"]['pc'])) {
			$data['title'] = $data["layoutTitle"]['pc'];
		} elseif ($this->deviceType == 'mobile' && !empty($data["layoutTitle"]['mobile'])) {
			$data['title'] = $data["layoutTitle"]['mobile'];
		}
		// 커스텀을 진행하는 매체만 해당
		// $data['title'] = str_replace('[단독]','<span class="blank_blue">단독</span>', $data['title']);
		// $data['title'] = str_replace('[속보]','<span class="blank_red">속보</span>', $data['title']);
		// title [] 제거
		if ($box['removeSquareBrackets'] == 'Y') {
			$data['title'] = preg_replace("/\[[^]]+]/i","",$data['title']);
			$data['title'] = preg_replace("/^<[^<]+>[ ]*/i","",trim($data['title']));
		}

		// content
		$data['content'] = empty($data['content'])?'':$this->cutDesciption($data['content'], $box['contentCutNum'], $box['conCapDel']);

		// thumbnail
		if ($box['showThumb'] == 'Y' && !empty($data['thumbnail'])) {
			// 면편집 이미지가 있으면 적용
			if ($this->deviceType == 'pc' && !empty($data["layoutImage"]['pc'])) {
				$data['thumbnail'] = $data["layoutImage"]['pc'];
			} elseif ($this->deviceType == 'mobile' && !empty($data["layoutImage"]['mobile'])) {
				$data['thumbnail'] = $data["layoutImage"]['mobile'];
			}
			// resize
			$thumbWidth = $box['thumbWidth'];
			$thumbHeight = $box['thumbHeight'];
			if ($thumbWidth || $thumbHeight) {
				$thumbReplace = '.'.$thumbWidth.'x'.$thumbHeight.'.0$1';
				if (preg_match('/gif$/', $data['thumbnail'])) {
					// gif
				} else {
					$data['thumbnail'] = preg_replace('/([.][a-z]+)$/', $thumbReplace, $data['thumbnail']);
				}
			}
		} else {
			unset($data['thumbnail']);
		}

		// cdn 적용 : thumbnail
		if (!empty($this->cdnDomain) && !empty($data['thumbnail']) && strpos($data['thumbnail'], $this->cdnDomain) === false) {
			$data['thumbnail'] = $this->cdnDomain.$data['thumbnail'];
		}

		return $data;
	}

	/**
	 * box template 설정
	 */
	protected function boxSetting($skin, $data='')
	{
		if (empty($this->tpl)) {
			// box용 템플릿 class
			$template_dir = 'data/template/box';	// template_dir : 공통경로 설정
			// $template_dir = '_template/box';		// template_dir : 로컬경로 설정
			$compile_dir = '_compile/'.$this->coId.'/box';  // 디렉토리 생성해야 함
			$this->tpl = $this->common->setTemplate($template_dir, $compile_dir);
		}
		$template = '';
		if (is_file("./".$this->tpl->template_dir.'/'.$skin.'.html')) {
			// wcms에서 등록한 box템플릿
			$template = $skin.'.html';
		} else {
			echo "box template이 없습니다 : ./".$this->tpl->template_dir.'/'.$skin.'.html<br>';
		}

		if (!empty($template)) {
			$this->tpl->define('box', $template);
			if (!empty($data)) {
				$this->tpl->assign($data);
			}
			return $this->tpl->fetch('box');
		} else {
			return '';
		}
	}

	/**
	 * 더보기 링크
	 */
	protected function getMoreUrl($val)
	{
		$moreUrl = empty($val["moreUrl"])?'':$val["moreUrl"];
		if (empty($moreUrl) && $val['listType'] != 'M') {
			if ($val['autoGroupType'] == "S" && !empty($val["series"])) {
				$moreUrl = "/article/list/".explode(",",$val["series"])[0];
			} elseif ($val['autoGroupType'] == "P" && !empty($val["program"])) {
				$moreUrl = "/article/list/".explode(",",$val["program"])[0];
			} elseif ($val['autoGroupType'] == "T" && !empty($val["tags"])) {
				$moreUrl = "/article/list/!".explode(",",$val["tags"])[0];
			} elseif ($val["category"]) {
				$moreUrl = "/article/list/".explode(",",$val["category"])[0];
			}
		}
		return $moreUrl;
	}

	/**
	 * 기사본문 자름
	 * 
	 * @param conCapDel 캡션을 제거할 것인가? Y/N
	 */
	protected function cutDesciption($description, $cutNum, $conCapDel='Y')
	{
		if ($cutNum==0) {
			$description="";
		} else {
			$description = str_replace(array("\n","\r"), array(" ",""), $description);
			$description = preg_replace('/<(figure)(.*?)<\/\1>/m', '', $description);
			$description = preg_replace("/\[[^]]+]/i", "", $description);
			if ($conCapDel=="N") {
				$description = str_replace(['{figcap}','{/figcap}'], '', $description);
			} else {
				$description = preg_replace('/{(figcap)(.*?) {\/\1}/m', '', $description);
			}
			$description = trim($description);
			$description = strip_tags($description);
			$description = mb_strcut($description, 0, $cutNum);
		}
		return $description;
	}

	/**********************************************************************************
     * 기사 리스트 조회
     */

	 /**
	  * 수동편집 기사 리스트 조회
	  */
	protected function manualArticleList($id, $box, $mAids=[])
	{
		if (empty($id)) return;

		$items = [];
		$items = $this->readManualFile($id);
		if (!empty($items['items'])) {
			$items['items'] = $this->workArticleList($items['items'], $box, $mAids);
		}

		if (!empty($items['items']) && is_array($items['items'])) {
			foreach ($items['items'] as $key => $value) {
				
				// 관련기사
				if (!empty($box['useRelation']) && $box['useRelation'] == '1') {
					$maxCountRelation = 5; // 조회 할 관련기사 수
					if (!empty($value['relationId'][0])) {
						$items['items'][$key]['relation'] = $this->getRelation($value['relationId'][0]);
					}
					// 자기 자신은 제거
					if (!empty($items['items'][$key]['relation']['items']) && count($items['items'][$key]['relation']['items']) > 0) {
						$index = array_search($items['items'][$key]['aid'], array_column($items['items'][$key]['relation']['items'], 'aid'));
						if ($index !== false) {
							array_splice($items['items'][$key]['relation']['items'], $index, 1);
						}
						// maxCountRelation 갯수로 설정
						$items['items'][$key]['relation']['items'] = array_slice($items['items'][$key]['relation']['items'], 0, $maxCountRelation);
					}
				}
			}
		}

        return $items;
    }

	/**
	 * 수동편집 파일 조회
	 */
	protected function readManualFile($id)
	{
		$path = $this->dataPath."/layout/box";
		$list = $this->json->readJsonFile($path, $id);
		
		return $list;
	}

    /**
     * 관련기사 조회
     */
    protected function getRelation($id)
    {
        $dir = $this->dataPath.'/list/relation';
        return $this->json->readJsonFile($dir, $id);
    }

	/**
	 * 기사 리스트 API 조회
	 */
	protected function getArticles($key, $id, $box, $mAids=[])
	{
		if (empty($id)) return;
		if (!is_array($id)) $id = explode(',', $id);

		$items = [];

		if ($box['articleMerge']=="Y" || count($id) == 1) {
			// api 조회
			$request = [];
			$request['key'] = $key;
			$request['id'] = $id;
			$request['inImage'] = $box['onlyImage'];
			$request['inVideo'] = $box['onlyVideo'];
			$request['type_isFlash'] = $box['type_isFlash'];
			if (!empty($box['useRelation'])) $request['useRelation'] = $box['useRelation'];
			$request['excludeAutoPlace'] = 1;
			if ($box['removeHeadline'] == 'Y') $request['iniAids'] = $mAids;
			$items = $this->getArticlesFromApi($request);
			if (!empty($items['items'])) {
				$items['items'] = $this->workArticleList($items['items'], $box, $mAids);
			}
		} else {
			foreach ($id as $i => $val) {
				// api 조회
				$request = [];
				$request['key'] = $key;
				$request['id'] = $val;
				$request['inImage'] = $box['onlyImage'];
				$request['inVideo'] = $box['onlyVideo'];
				$request['type_isFlash'] = $box['type_isFlash'];
				if (!empty($box['useRelation'])) $request['useRelation'] = $box['useRelation'];
				$request['excludeAutoPlace'] = 1;
				if($key=='analytics'){
					$request['gaCategory'] = sizeof(explode("_",$request['id']))==3?explode("_",$request['id'])[1]:"";
				}
				if ($box['removeHeadline'] == 'Y') $request['iniAids'] = $mAids;
				$items['items'][$i] = $this->getArticlesFromApi($request);
				if (!empty($items['items'][$i]['items'])) {
					$items['items'][$i]['items'] = $this->workArticleList($items['items'][$i]['items'], $box, $mAids);
				}
				$items['items'][$i]['firstPublishDate'] = empty($items['items'][$i]['items'][0]['firstPublishDate'])?'':$items['items'][$i]['items'][0]['firstPublishDate'];
			}
			// 날짜 역순 소트
			array_multisort(array_column($items["items"], 'firstPublishDate'), SORT_DESC, $items["items"]);
		}

		return $items;
	}

	/**
	 * api article list
	 * 
	 * @param id
	 * @param listType
	 * @param page
	 * @param limit
	 * @return 
	 */
	protected function getArticlesFromApi($request)
	{
		// api 조회
		$api = new Api();
		// $request['noCache'] = '1';
		$request['page'] = empty($request['page'])?1:$request['page'];
		$request['limit'] = empty($request['limit'])?50:$request['limit'];	// 수동편집 기사가 많을 경우 갯수 조정해야 함
		return $api->data('getArticles', $request);
	}

	/**********************************************************************************
     * 게시판 리스트 조회
     */

	 /**
	  * 수동편집 게시판 리스트 조회
	  */
	  protected function manualBoardList($id, $box)
	  {
		  if (empty($id)) return;
  
		  $items = [];
		  $items = $this->readManualFile($id);

		  if (!empty($items['items'])) {
			  $items['items'] = $this->workBoardList($items['items'], $box);
		  }
  
		  return $items;
	  }

	/**
	 * 게시판 리스트 API 조회
	 */
	protected function getBoards($key, $id, $box)
	{
		if (empty($id)) return;
		if (!is_array($id)) $id = explode(',', $id);

		$items = [];

		if ($box['listMerge']=="Y" || count($id) == 1) {
			// api 조회
			$request = [];
			$request['key'] = $key;
			$request['id'] = $id;
			$request['inImage'] = $box['onlyImage'];
			$items = $this->getBoardsFromApi($request);
			if (!empty($items['items'])) {
				$items['items'] = $this->workBoardList($items['items'], $box);
			}
		} else {
			foreach ($id as $i => $val) {
				// api 조회
				$request = [];
				$request['key'] = $key;
				$request['id'] = $val;
				$request['inImage'] = $box['onlyImage'];
				$items['items'][$i] = $this->getBoardsFromApi($request);
				if (!empty($items['items'][$i]['items'])) {
					$items['items'][$i]['items'] = $this->workBoardList($items['items'][$i]['items'], $box);
				}
				$items['items'][$i]['firstPublishDate'] = empty($items['items'][$i]['items'][0]['firstPublishDate'])?'':$items['items'][$i]['items'][0]['firstPublishDate'];
			}
			// 날짜 역순 소트
			array_multisort(array_column($items["items"], 'firstPublishDate'), SORT_DESC, $items["items"]);
		}

		return $items;
	}

	/**
	 * api board list
	 * 
	 * @param id
	 * @param listType
	 * @param page
	 * @param limit
	 * @return 
	 */
	protected function getBoardsFromApi($request)
	{
		// api 조회
		$api = new Api();
		// $request['noCache'] = '1';
		$request['page'] = empty($request['page'])?1:$request['page'];
		$request['limit'] = empty($request['limit'])?50:$request['limit'];	// 수동편집 데이터가 많을 경우 갯수 조정해야 함
		return $api->data('getBoards', $request);
	}

	/**
	 * 게시판 리스트 가공
	 */
	protected function workBoardList($data, $box)
	{
		if (empty($data)) {
			return [];
		}

		$box['showArticleCount'] = empty($box['showArticleCount'])?0:$box['showArticleCount'];
		$box['onlyImage'] = empty($box['onlyImage'])?'N':$box['onlyImage'];
		
		// 단일 리스트
		$return = [];
		$index = 0;
		foreach ($data as $key => $value) {
			if($index >= $box['showArticleCount']) break;

			// 이미지 있는 기사만
			if ($box['onlyImage']=="Y" && empty($value['thumbnail'])) {
				continue;
			}

			$return[$index] = $this->workBoardData($value, $box);
			$index++;
		}

		return $return;
	}

	/**
	 * 게시판 데이터 가공
	 */
	protected function workBoardData($data, $box)
	{
		$box['contentCutNum'] = empty($box['contentCutNum'])?150:$box['contentCutNum'];
		$box['showThumb'] = empty($box['showThumb'])?'Y':$box['showThumb'];
		$box['thumbWidth'] = empty($box['thumbWidth'])?'':$box['thumbWidth'];
		$box['thumbHeight'] = empty($box['thumbHeight'])?'':$box['thumbHeight'];

		// content
		$data['content'] = empty($data['content'])?'':$this->cutDesciption($data['content'], $box['contentCutNum'], $box['conCapDel']);

		if (empty($data['thumbnail']) && !empty($data['files']) && is_array($data['files']) && count($data['files']) > 0) {
			$data['thumbnail'] = $this->common->getThumbnail($data['files']);
		}

		// thumbnail
		if ($box['showThumb'] == 'Y' && !empty($data['thumbnail'])) {
			// 면편집 이미지가 있으면 적용
			if ($this->deviceType == 'pc' && !empty($data["layoutImage"]['pc'])) {
				$data['thumbnail'] = $data["layoutImage"]['pc'];
			} elseif ($this->deviceType == 'mobile' && !empty($data["layoutImage"]['mobile'])) {
				$data['thumbnail'] = $data["layoutImage"]['mobile'];
			}
			// resize
			$thumbWidth = $box['thumbWidth'];
			$thumbHeight = $box['thumbHeight'];
			if ($thumbWidth || $thumbHeight) {
				$thumbReplace = '.'.$thumbWidth.'x'.$thumbHeight.'.0$1';
				if (preg_match('/gif$/', $data['thumbnail'])) {
					// gif
				} else {
					$data['thumbnail'] = preg_replace('/([.][a-z]+)$/', $thumbReplace, $data['thumbnail']);
				}
			}
		} else {
			unset($data['thumbnail']);
		}

		// 게재일
		$data['firstPublishDate'] = $data['insert']['data'];

		// cdn 적용 : thumbnail
		if (!empty($this->cdnDomain) && !empty($data['thumbnail']) && strpos($data['thumbnail'], $this->cdnDomain) === false) {
			$data['thumbnail'] = $this->cdnDomain.$data['thumbnail'];
		}

		return $data;
	}
}