<?php
namespace Kodes\Www;

// ini_set('display_errors', 1);

/**
 * Logout 클래스
 * 
 * @author  Kodes <kodesinfo@gmail.com>
 * @version 1.0
 *
 * @license 해당 프로그램은 kodes에서 제작된 프로그램으로 저작원은 코드스(https://www.kode.co.kr)
 */
class Logout
{
    /** @var Class Class */
	protected $member;

    /**
	 * 생성자
	 */
    public function __construct()
	{
		$this->member = new Member();
    }

    /**
	 * 로그아웃
	 */
	public function logout()
	{
		$this->member->logout();
	}
}