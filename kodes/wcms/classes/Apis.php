<?php 
namespace Kodes\Wcms;

// ini_set('display_errors', 1);

/**
 * API 관리
 * 
 * @author  Kodes <kodesinfo@gmail.com>
 * @version 1.0
 *
 * @license 해당 프로그램은 kodes에서 제작된 프로그램으로 저작원은 코드스(https://www.kode.co.kr)
 */
class Apis
{
    /** @var String Collection Name */
    protected const COLLECTION = "api";
    protected const MakedList_COLLECTION = "makedList";

    /** @var Class */
    protected $db;
    protected $common;
    protected $coId;
    protected $json;

    /**
     * 생성자
     */
    public function __construct()
    {
        // class
        $this->db = new DB("apiDB");
        $this->common = new Common();
        $this->json = new Json();
		
        // variable
        $this->coId = $this->common->coId;
	}

    /**
    * Api Collection의 검색조건에 맞는 Row의 갯수를 반환
    * @return Array Api Collection, Page
    */
    public function list()
    {
		try {
            $data = [];
            $i = 0;
            $filter=[];

            if(!empty($_GET['category'])){
                $filter['category']=$_GET['category'];
            }

            if($_GET['searchText']!=''){
                $filter['title']=new \MongoDB\BSON\Regex($_GET['searchText'],'i');
            }
            
            //  전체 게시물 숫자
            $data["totalCount"] = $this->db->count(self::COLLECTION, $filter);
            
            $noapp = ($_GET['noapp']==""?50:$_GET['noapp']);
            $page = $_GET["page"]==""?1:$_GET["page"];
            $pageInfo = new Page;
            $data['page'] = $pageInfo->page($noapp, 10,$data["totalCount"], $page);
            
            $options = ["skip" => ($page - 1) * $noapp, "limit" => $noapp, 'sort' => ['title' => 1]];
            
            $data['items'] = $this->db->list(self::COLLECTION, $filter,$options);
            $data['category'] = $this->getCategoryList();
        } catch(\Exception $e) {
            $data['msg'] = $this->common->getExceptionMessage($e);
        }
		
        return $data;
    }

	public function editor()
    {
        $data = [];
		try {
            if (!empty($_GET['id'])) {
                $filter=['id'=>$_GET['id']];
                $options=[];
                $data['item'] = $this->db->item(self::COLLECTION, $filter, $options);
            }
        } catch(\Exception $e) {
            $data['msg'] = $this->common->getExceptionMessage($e);
        }

        $data['category'] = $this->getCategoryList();
		return $data;
	}

	public function insert()
    {
		try {
            $item = $this->covertDataField($_POST, 'insert');
            $item['coId'] = $this->coId;
            $result = $this->db->insert(self::COLLECTION, $item);
			$data = $result->getInsertedCount();
        } catch(\Exception $e) {
            $data['msg'] = $this->common->getExceptionMessage($e);
        }
		return $data;
	}

	public function modify()
    {
		try {
            $item = $this->covertDataField($_POST, 'update');
            $filter = ['coId'=>$this->coId,'id'=>$item['id']];
            $options = ['$set'=>$item];
            $result = $this->db->update(self::COLLECTION, $filter, $options);
			$data = $result->getModifiedCount();
        } catch(\Exception $e) {
            $data['msg'] = $this->common->getExceptionMessage($e);
        }
		return $data;
	}

	public function apiLastItemUpdate(){
		$_POST['lastItemUpdate']=date("Y-m-d H:i:s");

		$filter=['id'=>$_POST['id']];
		$result = $this->db->update(self::COLLECTION, $filter, ['$set'=>$_POST]);
	}

    public function covertDataField($data, $action){
        foreach($data['tag'] as $key => $val){
            if(!empty($val) || !empty($data['field'][$key])){
                $data['items'][]=['tag'=>$val,'field'=>$data['field'][$key],'keyField'=>in_array($key, $data['keyField'])?"1":"0",'value'=>$data['value'][$key],'remark'=>$data['remark'][$key]];
            }
        }
        $data = $this->common->covertDataField($data, $action, ['tag','field','keyField','value','remark']);
        return $data;
    }

    public function getLastApiData(){
        
        try{
            $collection = $_GET['collection'];

            if(empty($collection)){
                throw new \Exception("잘못된 요청입니다.", 400);
            }
            $lastRow = $this->db->item($collection,[],['projection'=>['_id'=>0,'DATE'=>1],'sort'=>['DATE'=>-1]]);
            
            $data = $this->db->list($collection, $lastRow,['projection'=>['_id'=>0]]);

            return $data;

        }catch(\Exception $e) {
            $data['msg'] = $this->common->getExceptionMessage($e);
        }
    }

    public function category(){
        try{
            $filter['isDelete']=['$ne'=>'Y'];
            $data['item'] = $this->db->list("category", $filter,['projection'=>['_id'=>0],'sort'=>['modify.date'=>1]]);

            return $data;
        }catch(\Exception $e) {
            $data['msg'] = $this->common->getExceptionMessage($e);
        }
    }


    public function categoryUpdate(){
        try{
            if( empty($_GET['oldKeyword']) || $_GET['oldKeyword']=="undefined" || $_GET['oldKeyword']==$_GET['keyword']){
                $filter = ['category'=> trim($_GET['keyword'])];
            }else{
                $filter = ['category'=>trim($_GET['oldKeyword'])];
            }

            $options =['category'=>trim($_GET['keyword']),"modify"=>["date"=>date('Y-m-d H:i:s'),"user"=>$_SESSION['managerId']]];            
            $data = $this->db->upsert("category", $filter ,$options);

            $this->categoryMakeJson();

            return $data;
        }catch(\Exception $e) {
            $data['msg'] = $this->common->getExceptionMessage($e);
        }
    }

    public function categoryDelete(){
        try{
            $filter['category'] = $_GET['keyword'];
            $options['$set'] = ['isDelete'=>'Y', "delete"=>["date"=>date('Y-m-d H:i:s'),"user"=>$_SESSION['managerId']]];
            $data = $this->db->update("category", $filter ,$options);

            $this->categoryMakeJson();

            return $data;
        }catch(\Exception $e) {
            $data['msg'] = $this->common->getExceptionMessage($e);
        }
    }

    public function categoryMakeJson(){
        $list = $this->category();
        $this->json->makeJson('/webData/'.$this->coId.'/list/','category',$list);
    }

    public function getCategoryList(){
        $list = $this->json->readJsonFile('/webData/'.$this->coId.'/list/','category');

        return $list;
    }
}