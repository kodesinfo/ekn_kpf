<?php
namespace Kodes\Wcms;

/**
 * 세션 DB연동 클래스
 * SessionHandlerInterface를 구현
 * 
 * 사용법 : 세션 시작 시 다음과 같이 설정
   session_set_save_handler(new \Kodes\Wcms\DBSessionHandler(), true);
   session_start();
 * 
 * @author  Kodes <kodesinfo@gmail.com>
 * @version 1.0
 *
 * @license 해당 프로그램은 kodes에서 제작된 프로그램으로 저작원은 코드스(https://www.kode.co.kr)
 */
class DBSessionHandler implements \SessionHandlerInterface
{
    /** const */
    const COLLECTION = "session";

    /** @var variable */
    protected $ttl = 1440;  // 세션유지 시간(seconds)

    /**
     * 세션 시작
     */
    public function open($savePath, $sessionName)
    {
        $this->ttl = ini_get('session.gc_maxlifetime');
        $this->db = new DB();
        $this->common = new Common();

        $this->coId = $this->common->coId;

        return true;
    }

    /**
     * 세션 닫기
     */
    public function close()
    {
        return true;
    }

    /**
     * 세션 읽기
     */
    public function read($id)
    {
        $filter = ['sessionId'=>$id, 'sessionExpires'=>['$gte'=>date('Y-m-d H:i:s')]];
        $options = [];
        $data = $this->db->item(self::COLLECTION, $filter, $options);

        if (empty($data['sessionData'])) {
            return '';
        } else {
            return $data['sessionData'];
        }
    }

    /**
     * 세션 쓰기
     */
    public function write($id, $data)
    {
        $sessionExpires = date('Y-m-d H:i:s',strtotime('+'.$this->ttl.' seconds'));
        $filter = ['sessionId'=>$id];
        $options = [
            'sessionId'=>$id,
            'sessionExpires'=>$sessionExpires,
            'sessionData'=>$data
        ];
        $result = $this->db->upsert(self::COLLECTION, $filter, $options);

        return true;
    }

    /**
     * 세션 삭제
     */
    public function destroy($id)
    {
        $filter = ['sessionId'=>$id];
        $result = $this->db->delete(self::COLLECTION, $filter);

        return true;
    }

    /**
     * 가비지 콜렉터
     */
    public function gc($maxlifetime)
    {
        $sessionExpires = date('Y-m-d H:i:s');
        // $sessionExpires = date('Y-m-d H:i:s',strtotime('+'.$maxlifetime.' seconds'));

        $filter = ['sessionExpires'=>['$lt'=>$sessionExpires]];
        $data = $this->db->delete(self::COLLECTION, $filter, false);

        return true;
    }
}