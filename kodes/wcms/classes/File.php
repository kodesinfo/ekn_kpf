<?php 
namespace Kodes\Wcms;

//ini_set('display_errors', 1);
//ini_set('display_startup_errors', 1);
//error_reporting(E_ALL & ~E_DEPRECATED & ~E_NOTICE);
/**
 * 업로드 처리 설정
 * 
 * 업로드 파일크기 관련하여 php.ini 에서 아래 값을 확인할 것
    upload_max_filesize = 100M
    post_max_size = 110M
    max_execution_time = 600
    max_input_time = 600
 */
ini_set('memory_limit','2048M');

/**
 * 파일 클래스
 * 
 * @file
 * @author  Kodes <kodesinfo@gmail.com>
 * @version 1.0
 *
 * @license 해당 프로그램은 kodes에서 제작된 프로그램으로 저작원은 코드스(https://www.kode.co.kr)
 * https://www.kodes.co.kr
 */
class File
{
	/** const */
	const DS = DIRECTORY_SEPARATOR;
	const MAX_WIDTH = 1920;	// 이미지 가로 사이즈 제한
	const MAX_IMAGE_SIZE = 2 * 1048576;	// 이미지 크기 제한
	const FILE_COLLECTION = 'file';
	
    /** @var Class DB Class */
    protected $db;
    /** @var Class Common Class */
    protected $common;
	protected $log;

    public function __construct()
	{
        $this->db = new DB();
        $this->common = new Common();
		$this->log = new Log();

		$this->coId = $this->common->coId;

		$this->imageExt = ['jpg','jpeg','png','gif','bmp','ico'];
    }

	/**
	 * 파일을 업로드한다.
	 */
    public function upload()
	{
		try {
			if (!empty($_POST["coId"])) $this->coId = $_POST["coId"];

			$ds          = self::DS;
			$storeFolder = '/webData/'.$this->coId.'/image';
			$targetFolder = '/data/'.$this->coId.'/image';
			$targetFileName = '';
			$fileInfo = [];

			if (!empty($_FILES)) {
				// File Upload
				$tempFile = $_FILES['file']['tmp_name'];
				$storeFullPath = $storeFolder.$ds.date('Y').$ds.date('m').$ds.date('d');
				$targetFullPath = $targetFolder.$ds.date('Y').$ds.date('m').$ds.date('d');
				if (!is_dir($storeFullPath)) {mkdir($storeFullPath,0755,true);}

				$ext = substr(strrchr($_FILES['file']['name'],"."),1);
				$ext = strtolower($ext);
				$misec = explode(" ", microtime());
				$mediaId=$this->getFileId($this->coId);
				$targetFileName=$mediaId.'.'.$ext;
				$targetFilePath = $targetFullPath.$ds.$targetFileName;
				$storeFilePath = $storeFullPath.$ds.$targetFileName;
				$originFilePath = $storeFullPath.$ds.$mediaId.'_o.'.$ext;
				move_uploaded_file($tempFile, $storeFilePath);
				//copy($storeFullPath.$ds.$targetFileName, $originFilePath);
				$size = getimagesize($storeFilePath);

				// if ($ext == "pdf") {
				// 	$pdfsource = $targetFileStore;
				// 	$pdftarget = $storeFullPath.$ds.str_replace('.pdf','.jpg', $targetFileName);
				// 	$pdf = new \Spatie\PdfToImage\Pdf($targetFileStore);
					
				// 	$this->fileInfo["paper"]=[];
				// 	foreach (range(1, $pdf->getNumberOfPages()) as $pageNumber) {
				// 		$pdf->setCompressionQuality(100);
				// 		$pdf->setPage($pageNumber)->setColorspace(\Imagick::COLORSPACE_RGB)->setOutputFormat('jpg')->saveImage($storeFullPath.$ds.str_replace('.pdf','', $targetFileName).'_'.$pageNumber.'.jpg');
				// 		$fileInfo["paper"][$pageNumber]=str_replace('/webData/','/data/',$storeFullPath.$ds.str_replace('.pdf','', $targetFileName).'_'.$pageNumber.'.jpg');
				// 	}
				// }
				
				//File Information DB Insert
				//Media Id, File Name, File Path, Create Date
				$fileInfo["coId"]=$this->coId;
				$fileInfo["id"]=$mediaId;
				$fileInfo["name"]=$targetFileName;
				$fileInfo["orgName"]= $_FILES['file']['name'];
				$fileInfo["ext"]= $ext;
				$fileInfo["path"]=$targetFilePath;
				$fileInfo["type"]=in_array($ext, $this->imageExt)?"image":"doc";
				$fileInfo["mimeType"]=$_FILES['file']['type'];
				$fileInfo['title']="";
				$fileInfo['caption']=$_FILES['file']['caption'];
				$fileInfo['description']=$_FILES['file']['memo'];
				$fileInfo['watermarkPlace']=0;
				$fileInfo["size"]=$_FILES['file']['size'];
				$fileInfo["width"]=$size[0];
				$fileInfo["height"]=$size[1];
				$fileInfo['copyright']="";
				$fileInfo['categoryId']="";
				$fileInfo['paper']['date']="";
				$fileInfo['paper']['edit']="";
				$fileInfo['paper']['page']="";
				$fileInfo['isDisplay']=$_POST["isDisplay"]=="N"?(Bool)false:(Bool)true;
				$fileInfo['isCopy']=$_POST["isCopy"]=="Y"?(Bool)true:(Bool)false;
				$fileInfo['copyFileId']=$_POST["copyFileId"]!=""?$_POST["copyFileId"]:"";
				$fileInfo["insert"]["managerId"]=$_SESSION["managerId"];
				$fileInfo["insert"]["managerName"]=$_SESSION["managerName"];
				$fileInfo["insert"]["date"]=date('Y-m-d H:i:s');

				// 리사이징
				if (in_array($ext, ['jpg','jpeg','png','bmp'])) {
					// 이미지 길이 제한
					$resizeInfo = $this->resizeMaxImage($storeFilePath, $storeFilePath, self::MAX_WIDTH);
					if ($resizeInfo) {
						$fileInfo["size"] = $resizeInfo["size"];
						$fileInfo["width"] = $resizeInfo["width"];
						$fileInfo["height"] = $resizeInfo["height"];
					}

					// 이미지 크기 제한
					if (intval($fileInfo["size"]) > self::MAX_IMAGE_SIZE) {
						$width = intval(intval($fileInfo["width"]) >= intval($fileInfo["height"])?$fileInfo["width"]:$fileInfo["height"]);
						$resizeInfo = $this->resizeMaxImage($storeFilePath, $storeFilePath, $width, true);
						if ($resizeInfo) {
							$fileInfo["size"] = $resizeInfo["size"];
							$fileInfo["width"] = $resizeInfo["width"];
							$fileInfo["height"] = $resizeInfo["height"];
						}
					}
				}

				// getFileId 로직 변경으로 upsert로 변경
				$this->db->upsert(self::FILE_COLLECTION, ['id'=>$fileInfo["id"]], $fileInfo);
				//$this->db->insert($this->collection, $fileInfo);
				
				$msg = 'File '.$mediaId.'('.$targetFileName.') '.$_SESSION['managerId'].' '."Upload";
				$this->log->writeLog($this->coId, $msg, 'File_upload');

				$data = $fileInfo;
			}
        } catch(\Exception $e) {
            $data['msg'] = $this->common->getExceptionMessage($e);
        }

        return $data;
	}

	/**
	 * 이미지 파일을 지정 경로로 업로드, DB저장 안함
	 */
	public function save()
	{
		try {
			if (!empty($_POST["coId"])) $this->coId = $_POST["coId"];

			$ds = self::DS;
			$global = $_POST['global'];
			// 상위경로 접근 방지를 위해 '.' 제거
			$path = (empty($_POST['path'])?'':$ds.str_replace('.','',$_POST['path']));
			if ($global!='1') {
				$path = $ds.$this->coId.$ds.'upload'.$ds.'save'.$path;
			}
			if (!empty($path)) {
				$storeFolder = '/webData'.$path;
				$targetFolder = '/data'.$path;
				$targetFileName = '';
	
				if (!empty($_FILES)) {
					// File Upload
					$tempFile = $_FILES['file']['tmp_name'];
					if (!is_dir($storeFolder)) {mkdir($storeFolder,0755,true);}
	
					$ext = substr(strrchr($_FILES['file']['name'],"."),1);
					$ext = strtolower($ext);
					$fileId = str_replace('.','',microtime(true));
					$targetFileName = $this->coId.$fileId.'.'.$ext;
					$storeFilePath = $storeFolder.$ds.$targetFileName;
					$targetFilePath = $targetFolder.$ds.$targetFileName;
					move_uploaded_file($tempFile,$storeFilePath);
					$size = getimagesize($storeFilePath);

					$fileInfo["orgName"] = $_FILES['file']['name'];
					$fileInfo["size"] = $_FILES['file']['size'];
					$fileInfo["width"] = $size[0];
					$fileInfo["height"] = $size[1];
					$fileInfo["mimeType"] = $_FILES['file']['type'];
	
					// 리사이징
					if (in_array($ext, ['jpg','jpeg','png','bmp'])) {
						// 이미지 길이 제한
						$resizeInfo = $this->resizeMaxImage($storeFilePath, $storeFilePath, self::MAX_WIDTH);
						if ($resizeInfo) {
							$fileInfo["size"] = $resizeInfo["size"];
							$fileInfo["width"] = $resizeInfo["width"];
							$fileInfo["height"] = $resizeInfo["height"];
						}

						// 이미지 크기 제한
						if (intval($fileInfo["size"]) > self::MAX_IMAGE_SIZE) {
							$width = intval(intval($fileInfo["width"]) >= intval($fileInfo["height"])?$fileInfo["width"]:$fileInfo["height"]);
							$resizeInfo = $this->resizeMaxImage($storeFilePath, $storeFilePath, $width, true);
							if ($resizeInfo) {
								$fileInfo["size"] = $resizeInfo["size"];
								$fileInfo["width"] = $resizeInfo["width"];
								$fileInfo["height"] = $resizeInfo["height"];
							}
						}
					}
	
					$data = [
						'path'=>$targetFilePath,
						'name'=>$targetFileName,
						'orgName'=>$fileInfo["orgName"],
						'width'=>$fileInfo["width"],
						'height'=>$fileInfo["height"],
						'size'=>$fileInfo["size"],
						'ext'=>$ext,
						'mimeType'=>$fileInfo["mimeType"],
						'type'=>in_array($ext, $this->imageExt)?"image":"doc",
					];
				}
			}
        } catch(\Exception $e) {
            $data['msg'] = $this->common->getExceptionMessage($e);
        }

        return $data;
	}

	/**
	 * 파일을 지정 경로로 업로드, DB저장 안함
	 */
	public function attach()
	{
		try {
			if (!empty($_POST["coId"])) $this->coId = $_POST["coId"];
			
			$ds = self::DS;
			// 상위경로 접근 방지를 위해 '.' 제거
			$path = (empty($_POST['path'])?'':$ds.str_replace('.','',$_POST['path']));
			if (!empty($path)) {

				$storeFolder = '/webData/'.$this->coId.$path;
				$targetFolder = '/data/'.$this->coId.$path;
				$targetFileName = '';

				if (!empty($_FILES)) {
					// File Upload
					$tempFile = $_FILES['file']['tmp_name'];
					if (!is_dir($storeFolder)) {mkdir($storeFolder,0755,true);}

					if (empty($_POST['useOrgName'])) {
						$ext = substr(strrchr($_FILES['file']['name'],"."),1);
						$ext = strtolower($ext);
						$fileId = str_replace('.','',microtime(true));
						$targetFileName = $this->coId.$fileId.'.'.$ext;
						$storeFilePath = $storeFolder.$ds.$targetFileName;
						$targetFilePath = $targetFolder.$ds.$targetFileName;
					} else {
						$targetFileName = $_FILES['file']['name'];
						$storeFilePath = $storeFolder.$ds.$targetFileName;
						$targetFilePath = $targetFolder.$ds.$targetFileName;
					}

					move_uploaded_file($tempFile,$storeFilePath);

					$data = [
						'path'=>$storeFilePath,
						'name'=>$targetFileName,
						'orgName'=>$_FILES['file']['name'],
					];
				}
			}
        } catch(\Exception $e) {
            $data['msg'] = $this->common->getExceptionMessage($e);
        }

        return $data;
	}

	/**
     * 이미지 제한 크기로 리사이즈
     * 이미지 비율은 유지
     * 
     * @param sourceFileStore 원본 파일 (절대경로 권장)
     * @param targetFileStore 생성 파일 (절대경로 권장)
     * @param maxImageLength 이미지 길이 제한
	 * @param force 강제실행
     */
    protected function resizeMaxImage($sourceFileStore, $targetFileStore, $maxImageLength=1920, $force=false)
	{
        list($width, $height) = getimagesize($sourceFileStore);

        // 이미지의 긴 방향이 길이 제한보다 큰지 판단
        if ($width >= $height && $width > $maxImageLength) {
            $resizeWidth = $maxImageLength;
        } else if ($width < $height && $height > $maxImageLength) {
            $resizeHeight = $maxImageLength;
        } elseif ($force) {
			// 강제 실행
			if ($width >= $height) {
				$resizeWidth = $maxImageLength;
			} else {
				$resizeHeight = $maxImageLength;
			}
		}

        if ($resizeWidth || $resizeHeight) {
            // 사이즈 계산
            $src_x = 0;
            $src_y = 0;
            if ($resizeWidth && $resizeHeight) {
                $wRate = $resizeWidth / $width;
                $hRate = $resizeHeight / $height;
                // 이미지 비율 유지
                if ($wRate > $hRate) {
                    $rate =  $wRate;
                    $src_y = ( ( $height * $rate ) - $resizeHeight ) / 2;
                } else {
                    $rate =  $hRate;
                    $src_x = ( ( $width * $rate ) - $resizeWidth ) / 2;
                }
            } elseif ($resizeWidth) {
                $rate = $resizeWidth / $width;
                $resizeHeight = $height * $rate;
            } elseif ($resizeHeight) {
                $rate = $resizeHeight / $height;
                $resizeWidth = $width * $rate;
            }

            // 이미지 리사이즈
            $this->resizeImage($sourceFileStore, $targetFileStore, $src_x, $src_y, ($width*$rate), ($height*$rate), $width, $height);

            // 리사이즈 이후 값 설정
            $fileInfo["size"] = filesize($targetFileStore);
            $fileInfo["width"] = $resizeWidth;
            $fileInfo["height"] = $resizeHeight;
            return $fileInfo;
        }
        return false;
    }

    /**
     * 이미지 리사이즈
     */
    protected function resizeImage($file, $newfile, $src_x, $src_y, $dst_w, $dst_h, $width='', $height='')
	{
        if (empty($width) || empty($height)) {
            list($width, $height) = getimagesize($file);
        }
		$dst_image = imagecreatetruecolor($dst_w, $dst_h);
        $imageType = exif_imagetype($file);
        switch ($imageType) {
            case 1:
                $src_image = imagecreatefromgif ($file);
                break;
            case 2:
                $src_image = imagecreatefromjpeg($file);
                break;
            case 3:
				@imagealphablending($dst_image, false);
                @imagesavealpha($dst_image, true);
                $src_image = imagecreatefrompng($file);
                break;
        }
        imagecopyresampled($dst_image, $src_image, 0, 0, $src_x, $src_y, $dst_w, $dst_h, $width, $height);
        switch ($imageType) {
            case 1:
                imagegif ($dst_image, $newfile);
                break;
            case 2:
                imagejpeg($dst_image, $newfile, 100);   // quality 설정
                break;
            case 3:
                imagepng($dst_image, $newfile);
                break;
        }
        imagedestroy($src_image);
        imagedestroy($dst_image);
    }

	/**
	 * 파일을 카피해서 새로운 이미지로 등록한다.
	 */
	public function copy()
	{
		try {
			$fileName = $_POST['fileName'];
			$filePath = $_POST['filePath'];
			$orginalFile = ".".$_POST['filePath'];
			
			$ds = DIRECTORY_SEPARATOR;
			$ext = substr(strrchr($fileName,"."),1);
			$ext = strtolower($ext);
			$fileId=$this->getFileId($this->coId);
			$targetFileName=$fileId.'.'.$ext;
			$targetFolder = '/data/'.$this->coId.'/image'.$ds.date('Y').$ds.date('m').$ds.date('d');
			$storeFolder = '/webData/'.$this->coId.'/image';
			$targetFullPath = $storeFolder.$ds.date('Y').$ds.date('m').$ds.date('d');
			if (!is_dir($targetFullPath)) {mkdir($targetFullPath,0755,true);}
			$targetFile = $targetFullPath.$ds.$targetFileName;
			
			copy($orginalFile, $targetFile);

			$imageInfo = getimagesize($targetFullPath.$ds.$targetFileName);

			$fileInfo["coId"]=$this->coId;
			$fileInfo["id"]=$fileId;
			$fileInfo["name"]=$targetFileName;
			$fileInfo["orgName"]=$fileName;
			$fileInfo["ext"]= $ext;
			$fileInfo["path"]=$targetFolder.$ds.$targetFileName;
			$fileInfo["type"]="image";
			$fileInfo["mimeType"]=$imageInfo['mime'];
			$fileInfo['title']="";
			$fileInfo['caption']="";
			$fileInfo['description']="";
			$fileInfo['watermarkPlace']=0;
			$fileInfo["size"]="";
			$fileInfo["width"]=$imageInfo[0];
			$fileInfo["height"]=$imageInfo[1];
			$fileInfo['copyright']="";
			$fileInfo['categoryId']="";
			$fileInfo['paper']['date']="";
			$fileInfo['paper']['edit']="";
			$fileInfo['paper']['page']="";
			$fileInfo['isDisplay']=(Bool)false;
			$fileInfo['isCopy']=(Bool)true;
			$fileInfo['copyFileId']=explode(".", $fileName)[0];
			$fileInfo["insert"]["managerId"]=$_SESSION["managerId"];
			$fileInfo["insert"]["managerName"]=$_SESSION["managerName"];
			$fileInfo["insert"]["date"]=date('Y-m-d H:i:s');
			
			// getFileId 로직 변경으로 upsert로 변경
			$this->db->upsert(self::FILE_COLLECTION, ['id'=>$fileInfo["id"]], $fileInfo);
			//$this->db->insert($this->collection, $fileInfo);
			
			$msg = 'File '.$fileId.'('.$targetFileName.') '.$_SESSION['managerId'].' '."File Copy";
			$this->log->writeLog($this->coId, $msg, 'File_copy');

			$fileInfo['sfilePath'] = preg_replace('/([.][a-z]+)$/','.120x120.0$1',$fileInfo["path"]);

			$data = $fileInfo;
        } catch(\Exception $e) {
            $data['msg'] = $this->common->getExceptionMessage($e);
        }

        return $data;
	}

    public function getFileId($coId, $date="")
	{
        $fileDate = ($date==""?date("Ymd"):date("Ymd",strtotime($date)));
        $fileId = "";
        $regex = new \MongoDB\BSON\Regex($coId.$fileDate,'');
        $filter = ["id"=>$regex];
        $options = ["sort"=>["id"=>-1],"limit"=>1];
		$result = $this->db->item(self::FILE_COLLECTION, $filter, $options);
        if (array_key_exists('id',$result)) {
			$fileId = $result["id"];
            $fileId++;
        } else {
            $fileId = $coId. $fileDate."000001";
        }
		$result = $this->db->insert(self::FILE_COLLECTION, ['id'=>$fileId]);
		if ($result->getInsertedCount() == 0) {
			// 재귀호출
			return $this->getFileId($coId, $date);
		}
        return $fileId;
    }

	public function externalImg()
	{
		try {
			$data = [];

			$storeFolder = '/webData/'.$this->coId.'/image/'.date("Y/m/d/");
			if (!is_dir($storeFolder)) {mkdir($storeFolder,0755,true);}
			$targetFolder = str_replace('/webData','/data',$storeFolder);
			$mediaId=$this->getFileId($this->coId);
			$orgFile = $_POST['url'];
			
			preg_match('/[.]([a-z0-9]+)$/i',$orgFile,$tmp);
			$ext = $tmp[1];
			$ext = strtolower($ext);
			$misec = explode(" ", microtime());
			$mediaId=$this->getFileId($this->coId);
			$targetFileName=$mediaId.'.'.$ext;
			$storeFilePath = $storeFolder.$targetFileName;

			if (copy($orgFile,$storeFilePath)) {

				$imgSize = getimagesize($storeFilePath);
				$size = filesize($storeFilePath);

				$fileInfo["coId"]=$this->coId;
				$fileInfo["id"]=$mediaId;
				$fileInfo["name"]=$targetFileName;
				$fileInfo["orgName"]= $orgFile;
				$fileInfo["ext"]= $ext;
				$fileInfo["path"]=$targetFolder.$targetFileName;
				$fileInfo["type"]="image";
				$fileInfo["mimeType"]=$imgSize['mime'];
				$fileInfo['title']=$_POST['cation'];
				$fileInfo['caption']=$_POST['cation'];
				$fileInfo['description']=$_POST['description'];
				$fileInfo['watermarkPlace']=0;
				$fileInfo["size"]=$size;
				$fileInfo["width"]=$imgSize[0];
				$fileInfo["height"]=$imgSize[1];
				$fileInfo['copyright']="";
				$fileInfo['categoryId']="";
				$fileInfo['paper']['date']="";
				$fileInfo['paper']['edit']="";
				$fileInfo['paper']['page']="";
				$fileInfo['isDisplay']=(Bool)true;
				$fileInfo['isCopy']=(Bool)true;
				$fileInfo['copyFileId']=$_POST["copyFileId"]!=""?$_POST["copyFileId"]:"";
				$fileInfo["insert"]["managerId"]=$_SESSION["managerId"];
				$fileInfo["insert"]["managerName"]=$_SESSION["managerName"];
				$fileInfo["insert"]["date"]=date('Y-m-d H:i:s');

				// 리사이징
				if (in_array($ext, ['jpg','jpeg','png','bmp'])) {
					// 이미지 길이 제한
					$resizeInfo = $this->resizeMaxImage($storeFilePath, $storeFilePath, self::MAX_WIDTH);
					if ($resizeInfo) {
						$fileInfo["size"] = $resizeInfo["size"];
						$fileInfo["width"] = $resizeInfo["width"];
						$fileInfo["height"] = $resizeInfo["height"];
					}

					// 이미지 크기 제한
					if (intval($fileInfo["size"]) > self::MAX_IMAGE_SIZE) {
						$width = intval(intval($fileInfo["width"]) >= intval($fileInfo["height"])?$fileInfo["width"]:$fileInfo["height"]);
						$resizeInfo = $this->resizeMaxImage($storeFilePath, $storeFilePath, $width, true);
						if ($resizeInfo) {
							$fileInfo["size"] = $resizeInfo["size"];
							$fileInfo["width"] = $resizeInfo["width"];
							$fileInfo["height"] = $resizeInfo["height"];
						}
					}
				}
			
				// getFileId 로직 변경으로 upsert로 변경
				$this->db->upsert(self::FILE_COLLECTION, ['id'=>$fileInfo["id"]], $fileInfo);
				//$this->db->insert($this->collection, $fileInfo);
				
				$msg = 'File '.$mediaId.'('.$targetFileName.') '.$_SESSION['managerId'].' '."Upload";
				$this->log->writeLog($this->coId, $msg, 'File_externalImg');

				$data = $fileInfo;
			}
        } catch(\Exception $e) {
            $data['msg'] = $this->common->getExceptionMessage($e);
        }
		return $data;
	}

	/**
	 * 파일 다운로드
	 * 
	 * @param path 파일경로
	 * @param orgName 원본파일명
	 */
	public function download()
	{
		$path = trim($_GET['path'], '.');
		$orgName = $_GET['orgName'];

		// path가 없으면 실행하지 않음
		if (empty($path)) exit;
		// orgName이 없으면 path의 파일명으로 다운로드
		if (empty($orgName)) {
			$orgName = basename($path);
		}
		$file_name = str_replace("/data","/webData",$path);

		// make sure it's a file before doing anything!
		if (!is_file($file_name)) exit;

		// required for IE
		if (ini_get('zlib.output_compression')) 
			ini_set('zlib.output_compression', 'Off');	

		$mime = mime_content_type('$file_name');

		header("Pragma: public");
		header("Expires: 0");
		header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
		header('Last-Modified: '.gmdate ('D, d M Y H:i:s', filemtime($file_name)).' GMT');
		header("Content-Type: application/octet-stream");
		header("Content-Disposition: attachment; filename=".$orgName);
		header("Content-Transfer-Encoding: binary");
		header("Content-Length: ".filesize($file_name));

		ob_clean();
		flush();
		readfile($file_name);
		exit;
	}
}