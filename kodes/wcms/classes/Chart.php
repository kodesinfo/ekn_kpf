<?php 
namespace Kodes\Wcms;

/**
 * 차트 클래스
 * 
 * @author  Kodes <kodesinfo@gmail.com>
 * @version 1.0
 *
 * @license 해당 프로그램은 kodes에서 제작된 프로그램으로 저작원은 코드스(https://www.kode.co.kr)
 */
class Chart
{
    /** @var String Collection Name */
    const COLLECTION = "api";
    const MakedList_COLLECTION = "makedList";

    /** @var Class */
    protected $db;
    protected $common;
    protected $json;

    /**
     * 생성자
     */
    public function __construct()
    {
        // class
        $this->db = new DB();

		$this->db->chageDatabase("api");
        $this->common = new Common();
        $this->json = new Json();

        // variable
        $this->coId = $this->common->coId;
	}
	
	public function chart(){
		$data = $this->getApiList();
        $data['category'] = $this->getCategoryList();
		return $data;
	}

	public function getApiList()
    {
		try {
            $field = [];
            $option = ['sort' => [ 'updateDate' => -1]];
            $data = $this->db->list(self::COLLECTION, $field, $option);
        } catch(\Exception $e) {
            $data['msg'] = $this->common->getExceptionMessage($e);
        }


		return $data;
	}

	/**
    * Api 차트 제작시 필요한 필드 리턴
	* input : 
	* GET apiIdx : api index
    * @return Array  [ api Field List] 
    */
	public function field()
    {
		try {
            $field = ['collection'=>$_GET["apiIdx"]];
            $option = [];
            $data = $this->db->list(self::COLLECTION, $field, $option);
            
			$result = [];
            foreach($data[0]['items'] as $key => $val){
                if($val['field']!=""){
                    array_push($result, $val);
                }
            }
        } catch(\Exception $e) {
            $result['msg'] = $this->common->getExceptionMessage($e);
        }


		return $result;
	}

	/**
    * Api 일자 필드의 전체 리스트 리턴
	* input : 
	* GET apiIdx : api index
  	* field : 일자(X축) 데이터 필드명
    * @return Array  [ Date List] 
    */
	public function dates()
	{
		
		try {
            $field = ['collection'=>$_GET["apiIdx"]];
            $option = [];
            $data = $this->db->list(self::COLLECTION, $field, $option);
            
			$collection = $data[0]['collection'];
			$field = $_GET['field'];

            //$field2 = ['collection'=>$_GET["apiIdx"]];
            //$option2 =  ['sort' => [ $field => -1]];
            //$result = $this->db->list($collection, $field2, $option2);
			$aggregate = [
				'aggregate' => $collection,
				'pipeline' => [
					['$group' => ['_id' => '$'.$field]],
					['$sort' => ['_id' =>1]],
				],
				'cursor' => new \stdClass,
			];

            $result = $this->db->command("api", $aggregate);

			foreach ($result as $key => $val){
				$data = $val['_id'];
				$result[$key][$field]=$data;
				$result[$key][$field."Dp"]=$this->common->changeDateFormat($data);
			}


        } catch(\Exception $e) {
            $result['msg'] = $this->common->getExceptionMessage($e);
        }

		return $result;
	}

	/**
    * Api 조건 필드의 전체 리스트 리턴
	* input : 
	* GET apiIdx : api index
  	* field : 조건 데이터 필드명
    * @return Array  [ Date List] 
    */
	public function condition()
	{		
		try {
			$collection = $_GET["apiIdx"];
			$field = $_GET['field'];

			$aggregate = [
				'aggregate' => $collection,
				'pipeline' => [
					['$group' => ['_id' => '$'.$field]],
					['$sort' => ['_id' =>1]],
				],
				'cursor' => new \stdClass,
			];

            $result = $this->db->command("api", $aggregate);
        } catch(\Exception $e) {
            $result['msg'] = $this->common->getExceptionMessage($e);
        }

		return $result;
	}

	/**
    * Api 차트 제작시 데이터 리턴
	* input : 
	* GET apiIdx : api index
  	*	  field1 : X축 데이터 필드명
  	*	  field2 : Y축 데이터 필드명
	*	  startDate : X축 시작 일자
	*	  endDate : X축 끝 일자
    * @return Array  [ API Data List] 
    */
	public function data()
	{	
		try {
            $field = ['collection'=>$_GET["apiIdx"]];
            $option = [];
            $data = $this->db->list(self::COLLECTION, $field, $option);
            
			$collection = $data[0]['collection'];

			$field1 = $_GET['field1'];
			$field2 = $_GET['field2'];
			$field3 = $_GET['field3'];
			$condition = $_GET['condition'];

			$tags=$field1.", ".$field2;
			$_GET['startDate'] = ( $_GET['startDate'] == "날짜선택"?"":$_GET['startDate']);
			$_GET['endDate'] = ( $_GET['endDate'] == "최신자"?"":$_GET['endDate']);

			$filter = [];
			if( !empty($field1)){
				if( !empty($_GET['startDate']!="") && !empty($_GET['endDate']!=""))
				{
					$filter[$field1] = ['$gte'=>$_GET['startDate'],'$lte'=>$_GET['endDate']];
				}
				else if( empty($_GET['startDate']!="") && !empty($_GET['endDate']!=""))
				{
					$filter[$field1] = ['$lte'=>$_GET['endDate']];
				}
				else if( !empty($_GET['startDate']!="") && empty($_GET['endDate']!=""))
				{
					$filter[$field1] = ['$gte'=>$_GET['startDate']];
				}
			}

			if( !empty($field3) && !empty($condition)){
				$filter[$field3] = ['$in'=>explode(',', $condition)];
			}
            $option = ['sort' => [ $field1 => 1]];
            $result = $this->db->list($collection, $filter, $option);
			
			foreach($result as $key => $val){
				$result[$key][$field1]=$this->common->changeDateFormat($val[$field1]);
			}	
		}catch(\Exception $e) {
            $result['msg'] = $this->common->getExceptionMessage($e);
        }

		return $result;
	}

    /**
    * 차트 정보 저장
	* input : 
	*          POST : 만들어진 차트 정보
    * @return Array  [ Data List] 
    */
	public function recode()
    {
		$item;
		try {
			$item = $_POST;
            $item['idx'] = !empty($item['idx'])?$item['idx']:uniqid();
			$item['coId'] = $_SESSION['coId'];
            $item = $this->common->covertDataField($item, "insert", []);
            $item['insert']['IP'] = $_SERVER['REMOTE_ADDR'];

			if( empty($_POST['idx']) ){
				$result = $this->db->insert(self::MakedList_COLLECTION, $item);
			}else{
				unset($item['insert']);
				$item = $this->common->covertDataField($item, "update", []);
				$item['update']['IP'] = $_SERVER['REMOTE_ADDR'];
				$filter['idx'] = $item['idx'];
				$object = ['$set'=>$item];
				$result = $this->db->update(self::MakedList_COLLECTION, $filter, $object);
			}

			$this->makeJsonFile($item);
			$this->makeCategoryListFile($item['category']);
        } catch(\Exception $e) {
            $data['msg'] = $this->common->getExceptionMessage($e);
        }

		return $item;
	}

	/**
    * 차트 정보 저장
	* input : 
	*          POST idx : chart index
    * @return Array  [ Data List] 
    */
	public function makeJsonFile($item)
	{
		if( !empty($item) ){
			$directory = '/webData/'.$this->coId.'/chart/'.date("Ym").'/';
			$fileName = $item['idx'];
			$this->json->makeJson($directory, $fileName, $item);
		}
	}

	public function makeCategoryListFile($category)
	{
		try {
			$field = ['category'=>$category];
			$option =  ['sort' => [ 'insert.date' => -1], 'projection'=>['_id'=>0, 'insert'=>0, 'update'=>0]];
			$directory = '/webData/'.$this->coId.'/list/category/';

			$result['count'] = $this->db->count(self::MakedList_COLLECTION, $field);
			$result['list'] = $this->db->list(self::MakedList_COLLECTION, $field, $option);
			$this->json->makeJson($directory, $category, $result);
        } catch(\Exception $e) {
            $data['msg'] = $this->common->getExceptionMessage($e);
        }
	}

	/**
    * 차트 이미지 저장 및 썸네일 생성
	* input : 
	*          POST idx : chart index
    * @return Array  [ Data List] 
    */
	public function toImage(){
		$photo = $_POST['photo'];
		$thumbWidth = 300;

		if (!empty($photo)) {
			$photo = str_replace('data:image/png;base64,', '', $photo);
			$entry = base64_decode($photo);
			$image = imagecreatefromstring($entry);
			$oWidth = imagesX($image);
	        $oHeight = imagesY($image);

			$rate = 1200 /  $oWidth;
			$sWidth = 1200;
			$sHeight = ceil($oHeight * $rate);
			//$sHeight = 400;

			$scaled = imageCreateTrueColor($sWidth, $sHeight);
			$background = imagecolorallocate($scaled, 255, 255, 255);
			imagefill($scaled, 0, 0, $background);
			imageCopyResampled($scaled, $image, 0, 0, 0, 0, $sWidth, $sHeight, $oWidth, $oHeight);

			$fileName = $_POST['idx'].".png";
			$directory = '/webData/'.$this->coId.'/chart/'.date("Ym").'/';
			if (!is_dir($directory)) {
				mkdir($directory, 0777, true);
			}
			
			$path = $directory.$fileName;
			if (file_exists($path)) {
				unlink($path);
			}
			$saveImage = imagepng($scaled, $path);

			// thumbnail 만들기
			$rate = $thumbWidth / $oWidth;
			$thumbHeight = ceil($oHeight * $rate);

			$scaledTumb = imageCreateTrueColor($thumbWidth, $thumbHeight);
			imagefill($scaledTumb, 0, 0, $background);
			imageCopyResampled($scaledTumb, $image, 0, 0, 0, 0, $thumbWidth, $thumbHeight, $oWidth, $oHeight);
			$thumbDir = preg_replace("/([.][a-z]+)$/i",'_thumb$1',$directory.$fileName);
			$saveImage = imagepng($scaledTumb, $thumbDir);

			imagedestroy($image);
			imagedestroy($scaled);
			imagedestroy($scaledTumb);

			if ($saveImage) {
				return $fileName;
			} else {
				return false; // image not saved
			}
		}
	}

	/**
    * 차트 파일 html 저장
	* input : 
	*          POST idx : chart index
    * @return Array  [ Data List] 
    */
	public function toFile(){
		$fileName = $_POST['idx'].".html";

		$directory = '/webData/'.$this->coId.'/chart/'.date("Ym")."/";
		if (!is_dir($directory)) {
            mkdir($directory, 0777, true);
        }

		$_POST['html']='<html lang="ko"><head><meta charset="utf-8"><title>Kodes Chart</title><script src="/lib/jquery/jquery-3.6.0.min.js"></script><script src="https://cdn.jsdelivr.net/npm/chart.js"></script></head><body><canvas id="chart"></canvas>'.$_POST['html'].'</body></html>';
		file_put_contents($directory.$fileName, $_POST['html']);
		echo $fileName;
	}

	/**
    * 사용된 차트 리스트 
	* input : 
	*          GET noapp : 페이지에 보요줄 리스트 갯수
	*			   keyword : 검색 키워드
    * @return Array  [ noapp, maxnum, this Page, maked Chart List ] 
    */
	public function makedlist()
	{
		
		try {
            //$filter['insert.managerId'] = $_SESSION['managerId'];
			if( !empty($_GET['keyword'])){
				$filter['title'] = new \MongoDB\BSON\Regex(preg_quote($_GET['keyword'], '/'),'i');
			}
			$filter['coId'] = $_SESSION['coId'];

            // count 조회
            $result["totalCount"] = $this->db->count(self::MakedList_COLLECTION, $filter);

			// paging
			$pageNavCnt = empty($_GET['pageNavCnt'])?10:$_GET['pageNavCnt'];
			$noapp = empty($_GET['noapp'])?10:$_GET['noapp'];
			$page = empty($_GET['page'])?1:$_GET['page'];
			$pageInfo = new Page;
			$result['page'] = $pageInfo->page($noapp, $pageNavCnt, $result["totalCount"], $page);

			// options
			$options = ['skip' => ($page - 1) * $noapp, 'limit' => $noapp, 'sort' => ["insert.date" => -1]];
            $result['list'] = $this->db->list(self::MakedList_COLLECTION, $filter, $options);
        } catch(\Exception $e) {
            $result['msg'] = $this->common->getExceptionMessage($e);
        }

		return $result;
	}

	/**
    * 만들어진 차트 정보 json 
	* input : 
	*          GET idx : chart index
    * @return Array  [ Data List] 
    */
	public function makedview()
	{
		try{
            $field = ['idx'=>$_GET["idx"]];
            $option = [];
            $result = $this->db->item(self::MakedList_COLLECTION, $field, $option);
		}catch(\Exception $e) {
            $result['msg'] = $this->common->getExceptionMessage($e);
        }

		return $result;
	}

    public function getCategoryList(){
        $list = $this->json->readJsonFile('/webData/'.$this->coId.'/list/','category');

        return $list;
    }
}