<?php
namespace Kodes\Wcms;

class Dashboard
{
    /** @var String Collection Name */
    const COLLECTION = "makedList";

    /** @var Class Json Class */
    protected $db;

    public function __construct()
    {
        // class
        $this->db = new DB("apiDB");

        // variable
		$this->coId = $this->common->coId;
    }

    public function dashboard()
    {
		
		try {
			$filter['coId'] = $_SESSION['coId'];

			// options
			$options = [ 'limit' => 6, 'sort' => ["insert.date" => -1]];
            $result['items'] = $this->db->list(self::COLLECTION, $filter, $options);
        } catch(\Exception $e) {
            $result['msg'] = $this->common->getExceptionMessage($e);
        }

		return $result;
    }
}