<?php 
namespace Kodes\Wcms;

class Common
{
	/** @var Class */
    protected $json;
	protected $log;

	/** @var variable */
	public $config;
	public $coId;

	/**
     * 생성자
     */
	public function __construct()
	{
		$this->json = new Json();
		$this->log = new Log();
		$this->coId = empty($_SESSION['coId'])?'':$_SESSION['coId'];
		$this->config = $this->getConfigCommon();
		$this->dataDir = $this->config['path']['data'].'/'.$this->coId;
	}

    /**
     * 템플릿 class 객체 생성
     */
    public function setTemplate($template_dir=null, $compile_dir=null)
    {
        $tpl = new Template_();
		if (empty($template_dir)) {
			$tpl->template_dir = '_template';
		} else {
			$tpl->template_dir = $template_dir;
		}
		if (empty($compile_dir)) {
			$tpl->compile_dir = '_compile';
		} else {
			$tpl->compile_dir = $compile_dir;
		}
		if (!is_dir($tpl->compile_dir)) {
			mkdir($tpl->compile_dir, 0777, true);
		}
		return $tpl;
    }

	/**
	 * common
	 */
	public function getConfigCommon()
	{
		if (empty($GLOBALS['common'])) {
			$GLOBALS['common'] = $this->json->readJsonFile('/webSiteSource/kodes/wcms/config', 'common');
		}
		return $GLOBALS['common'];
	}

	/**
	 * 회사
	 */
	public function getCompany()
	{
		if (empty($GLOBALS['company'])) {
			$GLOBALS['company'] = $this->json->readJsonFile($this->dataDir.'/config', $this->coId."_company");
		}
		return $GLOBALS['company'];
	}

	/**
	 * 카테고리
	 */
	public function getCategory()
	{
		if (empty($GLOBALS['category'])) {
			$GLOBALS['category'] = $this->json->readJsonFile($this->dataDir, $this->coId."_category");
		}
		return $GLOBALS['category'];
	}

	/**
	 * 카테고리 Tree
	 */
	public function getCategoryTree()
	{
		if (empty($GLOBALS['categoryTree'])) {
			$GLOBALS['categoryTree'] = $this->json->readJsonFile($this->dataDir, $this->coId."_categoryTree");
		}
		return $GLOBALS['categoryTree'];
	}

	/**
	 * 게시판 정보 목록
	 */
	public function getBoardInfoList()
	{
		if (empty($GLOBALS['boardInfoList'])) {
			$GLOBALS['boardInfoList'] = $this->json->readJsonFile($this->dataDir, $this->coId."_board");
		}
		return $GLOBALS['boardInfoList'];
	}

	/**
	 * 데이터를 변환시킨다.
	 * 
	 * @param $val Array 변환시킬 데이터
	 * @param $flag 입력/수정/삭제
	 * @param $removeField 제거시킬 필드
     * @return Array 변환된 데이터
	 */
	public function covertDataField($val, $flag, $removeField=[])
	{
		if ($flag == "insert") {
			$val["insert"]["date"]			= date('Y-m-d H:i:s');
			$val["insert"]["managerId"]		= $_SESSION["managerId"];
			$val["insert"]["managerName"]	= $_SESSION["managerName"];
		} elseif ($flag == "update") {
			$val["update"]["date"]			= date('Y-m-d H:i:s');
			$val["update"]["managerId"]		= $_SESSION["managerId"];
			$val["update"]["managerName"]	= $_SESSION["managerName"];
		} elseif ($flag == "delete") {
			$val["delete"]["is"]			= true;
			$val["delete"]["date"]			= date('Y-m-d H:i:s');
			$val["delete"]["managerId"]		= $_SESSION["managerId"];
			$val["delete"]["managerName"]	= $_SESSION["managerName"];
		}

		// 필드 제거
		foreach ($removeField as $key => $value) {
			unset($val[$value]);
		}

		return $val;
	}

	/**
	 * requestMethod 체크
	 * 
	 * @param String http method
	 * @return void
	 */
	public function checkRequestMethod($requestMethod)
	{
		if ($requestMethod != $_SERVER['REQUEST_METHOD']) {
            throw new \Exception('허용되지 않는 요청입니다. (Method Not Allowed)', 405);
        }
	}

	/**
	 * 예외 메시지 처리 및 로그 출력
	 * 
	 * @param \Exception $e 예외 객체
	 * @return String $msg 사용자에게 전달 할 메시지
	 */
	public function getExceptionMessage(\Exception $e)
	{
		http_response_code($e->getCode());
		$msg = '';
		if ($e->getCode() < 500) {
			$msg = $e->getMessage();
		} else {
			$msg = "요청 처리에 실패하였습니다.";
			$this->log->writeLog($this->coId, $e->getCode().' : '.$e->getMessage(), "Exception");
		}
		return $msg;
	}

	/*public function checkBool($param)
	{
		$boolval = ( is_string($val) ? filter_var($val, FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE) : (bool) $val );
		return ( $boolval===null ? false : $boolval );
	}*/

	/**
	 * ip 가져옴
	 */
	public function getRemoteAddr()
	{
		return !empty($_SERVER['HTTP_X_FORWARDED_FOR'])?$_SERVER['HTTP_X_FORWARDED_FOR']:$_SERVER['REMOTE_ADDR'];
	}

	/**
	 * 2차원 배열 내 값을 key/value로 검색하여, 일치하는 첫번째 row를 반환
	 * 검색하려는 key가 row에 존재해야 함
	 * 
	 * @param Array $array 검색대상 배열
	 * @param String $key 검색할 필드명
	 * @param String $value 검색할 값
	 * @return Array 검색결과 row
	 */
	public function searchArray2D($array, $key, $value)
	{
		if (empty($array) || !is_array($array) || empty($key) || empty($value)) {
			return null;
		}
		$index = array_search($value, array_column($array, $key));
		if ($index !== false) {
			return $array[$index];
		} else {
			return null;
		}
	}

	/**
	 * strpos에서 needle을 배열로 사용한 함수
	 * 
	 * @param String $str 문자열
	 * @param Array $needleArray 배열
	 * @return Bool 결과 true/false
	 */
	public function strposArray($str, $needleArray)
	{
		if (empty($str)) {
			return true;
		}
		if (empty($needleArray) || !is_array($needleArray) || count($needleArray) == 0) {
			return false;
		}
		$return = false;
		foreach ($needleArray as $key => $value) {
			if (!empty($value) && strpos($str, $value) !== false) {
				$return = true;
			}
		}
		return $return;
	}

	/**
     * php://input 처리
     * php://input 으로 수신되는 변수를 일반적인 파라미터(get, post) 배열로 변환
     * 
     * ajax로 전송 시 data를 다음과 같이 설정해야 한다.
     * data: JSON.stringify($('#form').serializeArray()),
     */
    public function convertRequestByInput()
    {
        $datas = [];
        $input = json_decode(file_get_contents('php://input'), true);
        foreach ($input as $key => $value) {
            $temp = preg_match('/^([^\[\]]+)(\[.*\])?$/', $value['name'], $maches);
            if (empty($maches[2])) {
                $datas[$value['name']] = $value['value'];
            } else {
                $temp2 = preg_match_all('/\[([^\[\]]*)\]/', $maches[2], $maches2);
                if (!empty($maches2[1])) {
                    $curArray = &$datas[$maches[1]];  // 현재 배열을 $datas로 설정
                    foreach ($maches2[1] as $key2 => $value2) {
                        if (empty($value2) && $value2 != '0') {
                            $curArray[] = [];  // 새로운 다차원 배열 생성
                            $curArray = &$curArray[count($curArray)-1];  // 현재 배열을 새로운 다차원 배열로 변경
                        } else {
                            if (is_numeric($value2)) $value2 = intval($value2);
                            if (empty($curArray[$value2])) $curArray[$value2] = [];  // 새로운 다차원 배열 생성
                            $curArray = &$curArray[$value2];  // 현재 배열을 새로운 다차원 배열로 변경
                        }
                    }
                    $curArray = $value['value'];
                }
            }
        }
        return $datas;
    }

	/**
	 * 기사 상태명
	 */
    public function getStatusName($status)
	{
		$val = '';
        switch ($status) {
            case 'save':
                $val = '저장';
                break;
            case 'desk':
                $val = '데스크';
                break;
            case 'embargo':
                $val = '예약전송';
                break;
            case 'publish':
                $val = '발행';
                break;
            case 'delete':
                $val = '삭제';
                break;
			case 'auto':
				$val = '저장(자동)';
				break;
            default:
                $val = '저장';
                break;
        }

        return $val;
    }

	/**
	 * 본문을 Text형태로 변환
	 */
	public function convertTextContent($content)
	{
		return trim(str_replace(["&nbsp;", "\r", "\n"], [" ", "", " "], strip_tags(preg_replace('/<(figure).*?<\/\1>/s', '', $content))));
	}

	/**
	 * 파일 목록에서 썸네일(첫번째 이미지) 가져옴
	 */
	public function getThumbnail($files)
	{
		$thumbnail = '';
		foreach ($files as $key => $value) {
			if (!empty($value['type']) && $value['type'] == 'image') {
				$thumbnail = $value['path'];
				break;
			}
		}
		return $thumbnail;
	}

	/**
	 * 파일 목록에서 썸네일 캡션(첫번째 이미지) 가져옴
	 */
	public function getThumbnailCaption($files)
	{
		$caption = '';
		foreach ($files as $key => $value) {
			if (!empty($value['type']) && $value['type'] == 'image'){
				$caption = $value['caption'];
				break;
			}
		}
		return $caption;
	}

	/**
	 * 진행 상태를 구해온다.
	 *
	 * @param string $startTime 시작시간
	 * @param string $endTime 종료시간
	 * @return string $status 상태값
	 */
	public function checkProgress($startTime, $endTime)
	{
		$status = match(true){
			$endTime == '' => "success",
			$endTime < date("Y-m-d H:i:s") => "danger",
			$startTime > date("Y-m-d H:i:s") => "info",
			default => "success"
		};
		return $status;
	}

	/**
     * 지역 코드에 맞는 지역명을 반환한다.
     *
     * @param string $code
     * @return string 지역명
     */
	public function setCountry($code){
		$mapping = [];
		$mapping['KR-11'] = ['서울특별시'];
		$mapping['KR-26'] = ['부산광역시'];
		$mapping['KR-27'] = ['대구광역시'];
		$mapping['KR-28'] = ['인천광역시'];
		$mapping['KR-29'] = ['광주광역시'];
		$mapping['KR-30'] = ['대전광역시'];
		$mapping['KR-31'] = ['울산광역시'];
		$mapping['KR-41'] = ['경기도'];
		$mapping['KR-42'] = ['강원도'];
		$mapping['KR-43'] = ['충청북도'];
		$mapping['KR-44'] = ['충청남도'];
		$mapping['KR-45'] = ['전라북도'];
		$mapping['KR-46'] = ['전라남도'];
		$mapping['KR-47'] = ['경상북도'];
		$mapping['KR-48'] = ['경상남도'];
		$mapping['KR-49'] = ['제주특별자치도'];
		$mapping['KR-50'] = ['세종특별자치시'];

		$result = $mapping[$code] != ""?$mapping[$code] :"기타";
		return $result;
	}

	/**
	 * 권한 체크
	 */
	public function checkAuth($menu_id)
	{
		if (!empty($_SESSION['isSuper'])) return;	// 슈퍼유저
		if (!empty($_GET['returnType']) && $_GET['returnType'] == 'ajax') return;
		if (empty($menu_id)) throw new \Exception('메뉴ID가 없습니다.', 400);
		if (empty($_SESSION['auth']['menu']) || !in_array($menu_id, $_SESSION['auth']['menu'])) {
			throw new \Exception('권한이 없습니다.', 400);
		}
	}

	/**
	 * WCMS 메뉴 조회
	 */
	public function getWcmsMenu()
	{
		if (empty($GLOBALS['wcms_menu'])) {
			$wcms_menu = $this->json->readJsonFile('../config', 'wcms_menu');

			if($this->coId=='jtb'){
				$wcms_menu = $this->json->readJsonFile("../config","wcms_menu_jtb");
			}

			if (!empty($this->coId)) {
				$wcmsBoardMenu = $this->json->readJsonFile($this->config['path']['data'].'/'.$this->coId.'/config', 'wcmsBoardMenu');	// 게시판 메뉴
				// 게시판 메뉴 추가 처리
				if (!empty($wcmsBoardMenu) && is_array($wcmsBoardMenu)) {
					foreach ($wcms_menu as $key => &$value) {
						$item = empty($wcmsBoardMenu[$value['menuId']])?null:$wcmsBoardMenu[$value['menuId']];
						$childTemplate = empty($value['childTemplate'])?'':$value['childTemplate'];
						if (!empty($item) && !empty($childTemplate)) {
							foreach ($item as $key2 => &$value2) {
								if (!empty($value['menuId']) && !empty($value2['menuName'])) {
									$depth2 = $childTemplate;
									$depth2['menuId'] = $value2['menuId'];
									$depth2['menuName'] = $value2['menuName'];
									$depth2['parent'] = $value2['parent'];
									$depth2['link'] = $value2['link'];
									$depth2['datalink'] = $value2['datalink'];
									$value['child'][] = $depth2;
									unset($depth2);
								}
							}
							unset($value2);
						}
					}
					unset($value);
				}
			}

			$GLOBALS['wcms_menu'] = $wcms_menu;
		}
		return $GLOBALS['wcms_menu'];
	}

	/**
    * 숫자로 되어 있는 값을 년, 년 분기, 년 월, 년 월 일 로 형식을 변경하여 리턴
	* input : 
	*		date : 날짜
    * @return Array  [ Change Date] 
    */
	function changeDateFormat($data){
		if(is_numeric(str_replace('-','',$data))){
			$num = strlen($data);

			switch($num){
				case 4: // 년도
					$data=$data."년";
					break;
				case 5: // 분기
					$data=substr($data,0,4)."년".substr($data,4,1)."분기";
					break;
				case 6: // 월
					$data=substr($data,0,4)."년".substr($data,4,2)."월";
					break;
				case 8: // 년월일
					$data=substr($data,0,4)."년".substr($data,4,2)."월".substr($data,6,2)."일";
					break;
				case 10: // 년월일
					$data=date("Y년 m월 d일",strtotime($data));
					break;
			}
		}
		return $data;
	}
}