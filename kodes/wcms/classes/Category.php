<?php 
namespace Kodes\Wcms;

// ini_set('display_errors', 1);

/**
 * 카테고리 클래스
 * 
 * @author  Kodes <kodesinfo@gmail.com>
 * @version 1.0
 *
 * @license 해당 프로그램은 kodes에서 제작된 프로그램으로 저작원은 코드스(https://www.kode.co.kr)
 */
class Category
{

    /** @var Class */
    protected $db;
    protected $common;
    protected $json;

    /** @var Class Article관련 */
    protected $articlePublish;
    
    /** @var variable */
	protected $collection = 'category';
    protected $coId;

    /**
     * 생성자
     */
    function __construct()
    {
        // class
        $this->db = new DB();
        $this->common = new Common();
        $this->json = new Json();

        // variable
        $this->coId = $this->common->coId;
	}
    
    /**
     * 카테고리 목록
     *
     * @return Array $return[categoryTree, categoryList]
     */
    public function list()
    {
        $return = [];
        $filter = [];
        $options = [];

        $return['defaultColor'] = '#000000';    // 카테고리 기본 컬러
		
        // filter
        if(!empty($_GET['searchText'])) {
            $filter['name'] = new \MongoDB\BSON\Regex($_GET['searchText'],'i');
        }
        $filter['coId'] = $this->coId;
		$options = ['sort'=>['sort'=>1,'id'=>1], 'projection' => ['_id'=>0]];

        // parentId, sort 처리를 위해 depth별 조회
        $filter['depth'] = 1;
        $depth1 = $this->db->list($this->collection, $filter, $options);
        $filter['depth'] = 2;
        $depth2 = $this->db->list($this->collection, $filter, $options);
        $filter['depth'] = 3;
        $depth3 = $this->db->list($this->collection, $filter, $options);
        $category = [];

        foreach ($depth1 as $key1 => $value1) {
            $category[] = $value1;
            foreach ($depth2 as $key2 => $value2) {
                if($value1['id'] == $value2['parentId']) {
                    $category[] = $value2;
                    foreach ($depth3 as $key3 => $value3) {
                        if($value2['id'] == $value3['parentId']) $category[] = $value3;
                    }
                }
            }
        }

        // tree Data
		if(empty($_GET['searchText'])) {
            $pcategory = $this->getCategoryTree();
		}else{
			$pcategory = $category;
		}

        $return['categoryTree'] = $pcategory;
        $return['categoryList'] = $category;

        return $return;
    }

    /**
     * 카테고리 저장
     *
     * @param String [POST] $id
     * @return Array $return[msg]
     */
    public function saveProc()
    {
        $return = [];
        try {
            // requestMethod 체크
            $this->common->checkRequestMethod('POST');

            $data = $_POST;
            unset($_POST);

            // trim
            $data['name'] = trim($data['name']);
            
            // 필수값
            if(empty($data['coId'])) {
                throw new \Exception("회사코드가 없습니다.", 400);
            }
            if(empty($data['name'])) {
                throw new \Exception("카테고리 이름을 입력하세요.", 400);
            }

            $removeField = [];

            $action = 'update';
            if(empty($data['id'])) {
                // insert
                $action = 'insert';
                // depth 구함
                $depth = 0;
                if ($data['parentId'] == '0') {
                    $depth = 1;
                } else if (substr($data['parentId'], -6, 3) == '000') {
                    $depth = 2;
                } else {
                    $depth = 3;
                }
                $data['id'] = $this->generateId($data['parentId'], $data['coId'], $depth);
                $data['depth'] = $depth;
            }else{
                // update
                $action = 'update';
                // DB 미입력 필드 제거
                $removeField[] = 'parentId';
                $removeField[] = 'depth';
            }

            // String -> Bool
            $data['isUse'] = (Bool) $data['isUse'];
            $data['isNews'] = (Bool) $data['isNews'];
            $data['isGnb']['pc'] = (Bool) $data['isGnb']['pc'];
            $data['isGnb']['mobile'] = (Bool) $data['isGnb']['mobile'];

            // String -> int
            $data['sort'] = (int) $data['sort'];

            $data = $this->common->covertDataField($data, $action, $removeField);
            
            // DB 저장
            $filter['id'] = $data['id'];
            $result = $this->db->upsert($this->collection, $filter, ['$set'=>$data]);
    
            $this->coId = $data['coId'];
            $this->makeJson();

            // api publish 업데이트
            $param = [];
            $param['coId'] = $this->coId;
            $param['category'][] = $data;
            $this->articlePublish->setApiPublish($param, 0);

            unset($data);

            $return['msg'] = "저장되었습니다.";
        } catch(\Exception $e) {
            $return['msg'] = $this->common->getExceptionMessage($e);
        }

        return $return;
    }
    
    /**
     * 카테고리 삭제
     * 운영 시 카테고리 삭제를 지원하지 않는 것을 권장하며, 완전삭제가 필요한 경우 개발에 요청하여 검토 진행
     *
     * @param String [POST] $id
     * @return Array $return[msg]
     */
    public function deleteProc()
    {
        $return = [];
        try {
            // 카테고리 삭제를 금지할 경우
            // throw new \Exception("카테고리 삭제는 금지되어 있습니다.\n미사용 처리하세요.", 400);

            // requestMethod 체크
            $this->common->checkRequestMethod('POST');

            $id = $_POST['id'];
            unset($_POST);

            if(empty($id)) {
                throw new \Exception("id가 없습니다.", 400);
            }

            // 하위 카테고리 체크
            $filter['parentId'] = $id;
            $subCount = (int) $this->db->count($this->collection, $filter);
            if($subCount > 0) {
                throw new \Exception("해당 카테고리의 하위 카테고리가 존재하므로 삭제할 수 없습니다.", 400);
            }

            // 사용중인 기사 체크
            $filter['category.id'] = $id;
            $subCount = (int) $this->db->count('article', $filter);
            if($subCount > 0) {
                throw new \Exception("해당 카테고리를 사용중인 기사가 존재하므로 삭제할 수 없습니다.", 400);
            }

            $result = $this->db->delete($this->collection, ["id"=>$id]);
    
            $this->coId = substr($id, 0, -9);
            $this->makeJson();

            $return['msg'] = "카테고리가 삭제되었습니다.";
        } catch(\Exception $e) {
            $return['msg'] = $this->common->getExceptionMessage($e);
        }

        return $return;
    }

    /**
     * 카테고리 ID 생성
     * 생성규칙 : coId(회사코드)+1depth(숫자3자리)+2depth(숫자3자리)+3depth(숫자3자리)
     * 
     * @param String $parentId
     * @param String $coId
     * @param String $depth
     * @return String $id 카테고리 ID
     */
    protected function generateId($parentId, $coId, $depth)
    {
        $id = '';
        $regex = '';
        if($depth == 1) {
            $regex = new \MongoDB\BSON\Regex($coId, 'i');
        } else if ($depth == 2) {
            $regex = new \MongoDB\BSON\Regex(substr($parentId, 0, -6), 'i');
        } else {
            $regex = new \MongoDB\BSON\Regex(substr($parentId, 0, -3), 'i');
        }
        $filter = ["id" => $regex];
        $options = ["sort" => ["id" => -1], "limit" => 1];
        $result = $this->db->item($this->collection, $filter, $options);
        $tempId = $result['id'];
        if (empty($tempId)) {
            $id = $coId . '001' . '000' . '000';
        } else if ($depth == 1) {
            $tempInt = (int) substr($tempId, -9, 3);
            $id = substr($tempId, 0, -9) . sprintf('%03d', (string) $tempInt+1) . '000' . '000';
        } else if ($depth == 2) {
            $tempInt = (int) substr($tempId, -6, 3);
            $id = substr($tempId, 0, -6) . sprintf('%03d', (string) $tempInt+1) . '000';
        } else if ($depth == 3) {
            $tempInt = (int) substr($tempId, -3, 3);
            $id = substr($tempId, 0, -3) . sprintf('%03d', (string) $tempInt+1);
        }
        return $id;
    }

    /**
     * 카테고리 정보 입력, 수정, 삭제시 JSON 파일을 만든다.
     * 
     * @return void
     */
    protected function makeJson()
    {
        $filter['coId'] = $this->coId;
        $options = ['sort'=>['sort'=>1, 'id'=>1], 'projection' => ['_id'=>0]];
        // parentId, sort 처리를 위해 depth별 조회
        $filter['depth'] = 1;
        $depth1 = $this->db->list($this->collection, $filter, $options);
        $filter['depth'] = 2;
        $depth2 = $this->db->list($this->collection, $filter, $options);
        $filter['depth'] = 3;
        $depth3 = $this->db->list($this->collection, $filter, $options);
        $category = [];
        foreach ($depth1 as $key1 => $value1) {
            $category[] = $value1;
            foreach ($depth2 as $key2 => $value2) {
                if($value1['id'] == $value2['parentId']) {
                    $category[] = $value2;
                    foreach ($depth3 as $key3 => $value3) {
                        if($value2['id'] == $value3['parentId']) $category[] = $value3;
                    }
                }
            }
        }
        $this->json->makeJson($this->common->config['path']['data'].'/'.$this->coId, $this->coId.'_category', $category);
        $this->json->makeJson($this->common->config['path']['data'].'/'.$this->coId, $this->coId.'_categoryTree', $this->getCategoryTree());
        // $this->json->makeJson($this->db->path['config'],$this->coId.'_category', $category); // 미사용 : wcms에서는 카테고리를 DB에서 조회 할 것이므로
        unset($category);
    }

    /**
     * 카테고리 트리 생성
     */
    public function getCategoryTree()
    {
        $category = $this->db->list('category', ['coId'=>$this->coId], ['sort'=>['sort'=>1, 'id'=>1], 'projection' => ['_id'=>0]]);
        $pcategory = [];
        $pcategory2 = [];
        for ($i=0;$i<sizeof($category);$i++) {
            $tempCategory = $category[$i];
            if ($tempCategory['depth']=='1') {
                $pcategory[] = $tempCategory;
            }
            if ($tempCategory['depth']=='2') {
                $pcategory2[] = $tempCategory;
            }
        }
        $cnum = 0;
        for ($i=0;$i<sizeof($category);$i++) {
            $tempCategory = $category[$i];
            for ($j=0;$j<sizeof($pcategory2);$j++) {
                if ($pcategory2[$j]['id'] == $tempCategory['parentId']) {
                    $pcategory2[$j]['child'][$cnum++] = $tempCategory;
                }
            }
        }
        $cnum = 0;
        for ($i=0;$i<sizeof($pcategory2);$i++) {
            $tempCategory = $pcategory2[$i];
            for ($j=0;$j<sizeof($pcategory);$j++) {
                if ($pcategory[$j]['id'] == $tempCategory['parentId']) {
                    $pcategory[$j]['child'][$cnum++] = $tempCategory;
                }        
            }
        }
        return $pcategory;
    }

    /**
     * 매체 신규 등록 시 기본 카테고리 생성
     */
    public function makeDefaultCategory(){
        $this->coId= $_POST['coId'];
        
        // 정치, 경제, 사회, 생활/문화, IT/과학, 세계
        $data = [ ["name" => "정치", "id" => $this->coId."001000000","coId" => $this->coId,"color" => "#000000","depth" => 1,"description" => "","headerHtml" => "",
                    "imagePath" => "","insert" => ["date" => date("Y-m-d h:i:S"),"managerId" => "admin","managerName" => "슈퍼 관리자"],
                    "isGnb" => ["pc" => true,"mobile" => true],"isNews" => true,"isUse" => true,"link" => ["pc" => "","mobile" => ""],
                    "oldCategory" => "","parentId" => "0","sendCode" => ["naver" => "","daum" => "","nate" => "","zum" => ""],
                    "skin" => ["pc" => "news","mobile" => "news"],"sort" => 1,"target" => ["pc" => "_self","mobile" => "_self"]],

                    ["name" => "경제", "id" => $this->coId."002000000","coId" => $this->coId,"color" => "#000000","depth" => 1,"description" => "","headerHtml" => "",
                    "imagePath" => "","insert" => ["date" => date("Y-m-d h:i:S"),"managerId" => "admin","managerName" => "슈퍼 관리자"],
                    "isGnb" => ["pc" => true,"mobile" => true],"isNews" => true,"isUse" => true,"link" => ["pc" => "","mobile" => ""],
                    "oldCategory" => "","parentId" => "0","sendCode" => ["naver" => "","daum" => "","nate" => "","zum" => ""],
                    "skin" => ["pc" => "news","mobile" => "news"],"sort" => 1,"target" => ["pc" => "_self","mobile" => "_self"]],

                    ["name" => "사회", "id" => $this->coId."003000000","coId" => $this->coId,"color" => "#000000","depth" => 1,"description" => "","headerHtml" => "",
                    "imagePath" => "","insert" => ["date" => date("Y-m-d h:i:S"),"managerId" => "admin","managerName" => "슈퍼 관리자"],
                    "isGnb" => ["pc" => true,"mobile" => true],"isNews" => true,"isUse" => true,"link" => ["pc" => "","mobile" => ""],
                    "oldCategory" => "","parentId" => "0","sendCode" => ["naver" => "","daum" => "","nate" => "","zum" => ""],
                    "skin" => ["pc" => "news","mobile" => "news"],"sort" => 1,"target" => ["pc" => "_self","mobile" => "_self"]],

                    ["name" => "생활/문화", "id" => $this->coId."004000000","coId" => $this->coId,"color" => "#000000","depth" => 1,"description" => "","headerHtml" => "",
                    "imagePath" => "","insert" => ["date" => date("Y-m-d h:i:S"),"managerId" => "admin","managerName" => "슈퍼 관리자"],
                    "isGnb" => ["pc" => true,"mobile" => true],"isNews" => true,"isUse" => true,"link" => ["pc" => "","mobile" => ""],
                    "oldCategory" => "","parentId" => "0","sendCode" => ["naver" => "","daum" => "","nate" => "","zum" => ""],
                    "skin" => ["pc" => "news","mobile" => "news"],"sort" => 1,"target" => ["pc" => "_self","mobile" => "_self"]],

                    ["name" => "IT/과학", "id" => $this->coId."005000000","coId" => $this->coId,"color" => "#000000","depth" => 1,"description" => "","headerHtml" => "",
                    "imagePath" => "","insert" => ["date" => date("Y-m-d h:i:S"),"managerId" => "admin","managerName" => "슈퍼 관리자"],
                    "isGnb" => ["pc" => true,"mobile" => true],"isNews" => true,"isUse" => true,"link" => ["pc" => "","mobile" => ""],
                    "oldCategory" => "","parentId" => "0","sendCode" => ["naver" => "","daum" => "","nate" => "","zum" => ""],
                    "skin" => ["pc" => "news","mobile" => "news"],"sort" => 1,"target" => ["pc" => "_self","mobile" => "_self"]],

                    ["name" => "세계", "id" => $this->coId."006000000","coId" => $this->coId,"color" => "#000000","depth" => 1,"description" => "","headerHtml" => "",
                    "imagePath" => "","insert" => ["date" => date("Y-m-d h:i:S"),"managerId" => "admin","managerName" => "슈퍼 관리자"],
                    "isGnb" => ["pc" => true,"mobile" => true],"isNews" => true,"isUse" => true,"link" => ["pc" => "","mobile" => ""],
                    "oldCategory" => "","parentId" => "0","sendCode" => ["naver" => "","daum" => "","nate" => "","zum" => ""],
                    "skin" => ["pc" => "news","mobile" => "news"],"sort" => 1,"target" => ["pc" => "_self","mobile" => "_self"]]
                ];
        $result = $this->db->upsert($this->collection, $data);

        


    }
}