<?php
namespace Kodes\Wcms;

// ini_set('display_errors', 1);

/**
 * 로그인 클래스
 * 
 * @author  Kodes <kodesinfo@gmail.com>
 * @version 1.0
 *
 * @license 해당 프로그램은 kodes에서 제작된 프로그램으로 저작원은 코드스(https://www.kode.co.kr)
 */
class Login
{
    /** @var Class */
    protected $db;
    protected $common;
    protected $json;
    protected $manager;

    /** @var variable */
	protected $collection = 'manager';
    protected $coId;
    protected $ip;

    /**
     * 생성자
     */
    function __construct()
    {
        // class
        $this->db = new DB();
        $this->common = new Common();
        $this->json = new Json();
        $this->manager = new Manager();

        // variable
        $this->ip = $this->common->getRemoteAddr();

        $this->coId = $this->common->coId;
    }

    /**
     * 로그인 화면
     */
    public function login()
    {
        // 로그인 coId
        $coId = empty($_GET['coId'])?'':$_GET['coId'];
        if (!empty($coId)) {
            $return['company'] = $this->json->readJsonFile($this->common->config['path']['data'].'/'.$coId.'/config', $coId.'_company');
        }
        return $return;
    }

    /**
     * 로그인 체크
     */
    public function check()
    {
        $return = [];
        try {
            // requestMethod 체크
            $this->common->checkRequestMethod('POST');
            
            $id = $_POST['id'];
            $password = $_POST['password'];
            unset($_POST);

            // 관리자 조회
            $filter = ["id" => $id];
            $options = ['projection'=>['_id'=>0]];
            $loginData = $this->db->item($this->collection, $filter, $options);

            // ID 검증
            if (empty($loginData['id'])) {
                throw new \Exception("로그인 정보가 틀립니다.", 400);
            }
            // 비밀번호 검증
            if ($loginData['password'] != $this->manager->encryptPassword($password, $loginData['salt'])) {
                throw new \Exception("로그인 정보가 틀립니다.", 400);
            }

            // 로그인정보 세션에 등록
            $return['managerId'] = $loginData['id'];
            $return['link'] = $this->setSessionLogin($loginData);

            // 로그인 기록
            $filter = ['id'=>$id];
            $data = ['latestLogin.date'=>date('Y-m-d H:i:s'), 'latestLogin.ip'=>$this->ip];   // @todo 입력값 확인
            $result = $this->db->update($this->collection, $filter, ['$set'=>$data]);

            $return['msg'] = "로그인 성공";

        } catch(\Exception $e) {
            $return['msg'] = $this->common->getExceptionMessage($e);
        }

        return $return;
    }

    /**
     * 로그인정보 세션에 등록
     */
    protected function setSessionLogin($loginData)
    {
        $loginData['currentAuthIds'] = [];
        $loginData['allowDepartmentIds'] = [];
        if (!empty($loginData['authId'])) $loginData['currentAuthIds'][] = $loginData['authId'];
        if (!empty($loginData['departmentId'])) $loginData['allowDepartmentIds'][] = $loginData['departmentId'];
        // 회사정보 처리
        if (!empty($this->coId)) {
            $currentCompany = $this->common->searchArray2D($loginData['allowCompany'], 'coId', $this->coId);
            if (!empty($currentCompany)) {
                if(strpos($loginData['currentAuthIds'][0], $this->coId) === false) {
                    $loginData['currentAuthIds'] = $currentCompany['authId'];
                    $loginData['allowDepartmentIds'] = $currentCompany['departmentId'];
                } else {
                    if (!empty($currentCompany['authId']) && is_array($currentCompany['authId'])) {
                        $loginData['currentAuthIds'] = array_values(array_unique(array_merge($loginData['currentAuthIds'], $currentCompany['authId'])));
                    }
                    if (!empty($currentCompany['departmentId']) && is_array($currentCompany['departmentId'])) {
                        $loginData['allowDepartmentIds'] = array_values(array_unique(array_merge($loginData['allowDepartmentIds'], $currentCompany['departmentId'])));
                    }
                }
            }

            // 현재 부서ID
            if (empty($loginData['currentDepartmentId']) || strpos($loginData['currentDepartmentId'], $this->coId) === false) {
                if (!empty($loginData['allowDepartmentIds'][0])) {
                    $loginData['currentDepartmentId'] = $loginData['allowDepartmentIds'][0];
                } else {
                    $loginData['currentDepartmentId'] = null;
                }
            }

            // 현재 부서 조회
            if (!empty($loginData['currentDepartmentId'])) {
                $loginData['department'] = $this->json->readJsonFile($this->common->config['path']['data'].'/'.$this->coId.'/department', $loginData['currentDepartmentId']);
            }
            // if (!empty($loginData['departmentId'])) {
            //     $loginData['department'] = $this->json->readJsonFile($this->common->config['path']['data'].'/'.$this->coId.'/department', $loginData['departmentId']);
            // }
        }
        
        // 권한 조회
        $filter = ['id'=>['$in'=>$loginData['currentAuthIds']]];
        $options = ['projection'=>['_id'=>0]];
        
        $authItems = $this->db->list('auth', $filter, $options);
        $authClass = new Auth();
        $loginData['auth'] = $authClass->mergeAuth($authItems);  // 권한 merge

        // 세션 처리
        $loginData['managerId'] = $loginData['id'];
        $loginData['managerName'] = $loginData['name'];
        unset($loginData['id']);
        unset($loginData['name']);
        unset($loginData['password']);
        unset($loginData['salt']);
        
        $_SESSION = $loginData;

        if (!empty($_SESSION['isSuper']) && $_SESSION['isSuper']) {
            // isSuper
            return $this->makeLeftMenu([], true);
        } elseif (empty($loginData['auth']["menu"])) {
            return [];
        } else {
            return $this->makeLeftMenu($loginData['auth']["menu"]);
        }
    }

	/**
	 * 좌측메뉴 조회하여 세션에 저장
     * 
     * @param Array $auth_menu 메뉴권한
     * @param Bool 메뉴권한 무시(운영: false, 테스트: true)
     * @return String $link 랜딩페이지 링크
	 */
    public function makeLeftMenu($auth_menu, $ignorePermission = false)
    {
		// $ignorePermission = false;

        $menu = $this->common->getWcmsMenu();
        
        $html ="";
        $flag = false;
        $link = "";

        if (!empty($menu) && is_array($menu)) {
            foreach ($menu as $i => $tmp) {
                if ($ignorePermission || (!empty($auth_menu) && is_array($auth_menu) && in_array($tmp['menuId'], $auth_menu))) {
                    if (empty($link) && $tmp['link']) {
                        $link = $tmp['link'];
                    }

                    $html .= '<a class="br-menu-link" href="'.($tmp['link']==""?"javascript:void(0);":$tmp['link']).'" target="'.$tmp['target'].'"  data-link="'.(isset($tmp['datalink'])?$tmp['datalink']:'').'">';
                    $html .= '	<div class="br-menu-item">';
                    $html .= '		<i class="'.$tmp['icon'].'"></i>';
                    // $html .= '		<i class="'.((isset($tmp['child']) && count($tmp['child']) > 0) ?'menu-item-arrow ':'br-menu-icon icon ').$tmp['icon'].'"></i>';
                    $html .= '		<span class="menu-item-label">'.$tmp ['menuName'].'</span>';
                    if (isset($tmp['child']) && count($tmp['child']) > 0) {
                        $html .= '<i class="menu-item-arrow fa fa-angle-down"></i>';
                    }
                    $html .= '	</div>';
                    $html .= '</a>';

                    if (isset($tmp['child']) && count($tmp['child']) > 0) {
                        $html .= '<ul class="br-menu-sub nav flex-column">';
                        foreach ($tmp['child'] as $j => $child) {
                            if (empty($link) && $child['link']) {
                                $link = $child['link'];
                            }
        
                            if ($ignorePermission || (!empty($auth_menu) && is_array($auth_menu) && in_array($child['menuId'], $auth_menu))) {
                                $html .= '<li class="nav-item"><a class="nav-link" href="'.$child['link'].'" target="'.$child['target'].'" data-link="'.(isset($child['datalink'])?$child['datalink']:'').'"><i class="'.$child['icon'].'"></i> '.$child['menuName'].'</a></li>';
                            }
                        }
                        $html .= '</ul>';
                    }
                }
            }
        }

        $_SESSION['leftmenu'] = $html;

		return $link;
    }

    /**
     * 계정/권한 정보 갱신
     * 페이지 이동 시 호출
     */
    public function refreshLogin()
    {
        if (empty($_SESSION['managerId'])) {
            return;
        }

        // 계정 조회
        $filter = ["id" => $_SESSION["managerId"]];
        $options = ['projection'=>['_id'=>0]];
        $loginData = $this->db->item($this->collection, $filter, $options);

        // 페이지 이동 시 유지할 세션 값
        $loginData['coId'] = $_SESSION['coId'];
        if (!empty($_SESSION['currentDepartmentId'])) $loginData['currentDepartmentId'] = $_SESSION['currentDepartmentId'];

        // 로그인정보 세션에 등록
        $link = $this->setSessionLogin($loginData);
        
        return $link;
    }

    /**
     * 세션의 coId 변경
     */
    public function changeSessionCoId()
    {
        $result = [];
        try {
            // requestMethod 체크
            $this->common->checkRequestMethod('POST');

            if (empty($_POST['coId'])) {
                throw new \Exception("유효하지 않은 접근입니다.", 400);
            }

            if (!empty($_SESSION['isSuper']) || (!empty($_SESSION['allowCoId']) && in_array($_POST['coId'], $_SESSION['allowCoId']))) {
                $_SESSION['coId'] = $_POST['coId'];
                $result['link'] = $this->refreshLogin();
            } else {
                throw new \Exception("권한이 없습니다.", 400);
            }
        } catch (\Exception $e) {
            $result['msg'] = $this->common->getExceptionMessage($e);
        }

        return $result;
    }

    /**
     * 세션의 부서ID 변경
     */
    public function changeSessionDepartmentId()
    {
        $result = [];
        try {
            // requestMethod 체크
            $this->common->checkRequestMethod('POST');

            if (empty($_POST['departmentId'])) {
                throw new \Exception("유효하지 않은 접근입니다.", 400);
            }

            if (!empty($_SESSION['allowDepartmentIds']) && in_array($_POST['departmentId'], $_SESSION['allowDepartmentIds'])) {
                $_SESSION['currentDepartmentId'] = $_POST['departmentId'];
                // $result['link'] = $this->refreshLogin();
            } else {
                throw new \Exception("권한이 없습니다.", 400);
            }
        } catch (\Exception $e) {
            $result['msg'] = $this->common->getExceptionMessage($e);
        }

        return $result;
    }

    /**
     * 세션의 template 변경
     */
    public function changeSessionTemplate()
    {
        $result = [];
        try {
            // requestMethod 체크
            $this->common->checkRequestMethod('POST');

            if (empty($_POST['template'])) {
                throw new \Exception("유효하지 않은 접근입니다.", 400);
            }

            $_SESSION['template'] = $_POST['template'];
            $result['template'] = $_SESSION['template'];

        } catch (\Exception $e) {
            $result['msg'] = $this->common->getExceptionMessage($e);
        }

        return $result;
    }
}