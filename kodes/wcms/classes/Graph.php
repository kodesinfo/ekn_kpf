<?php 
namespace Kodes\Wcms;

//use \PhpOffice\PhpSpreadsheet\Spreadsheet;
//use \PhpOffice\PhpSpreadsheet\Writer\Xlsx;

//ini_set('display_errors', 1);

/**
 * 그래픽 클래스
 * 
 * @author  Kodes <kodesinfo@gmail.com>
 * @version 1.0
 *
 * @license 해당 프로그램은 kodes에서 제작된 프로그램으로 저작원은 코드스(https://www.kode.co.kr)
 */
class Graph
{
    /** @var String Collection Name */
    const COLLECTION = "makedList";

    /** @var Class */
    protected $db;
    protected $common;
    protected $chart;

    /**
     * 생성자
     */
    public function __construct()
    {
        // class
        $this->db = new DB("apiDB");
        $this->common = new Common();
        $this->chart = new Chart();

        // variable
        $this->coId = $this->common->coId;
	}
	
	/**
    * 사용된 차트 리스트 
	* input : 
	*          GET noapp : 페이지에 보요줄 리스트 갯수
	*			   keyword : 검색 키워드
    * @return Array  [ noapp, maxnum, this Page, maked Chart List ] 
    */
	public function list()
	{
		
		try {
            //$filter['insert.managerId'] = $_SESSION['managerId'];
			if( !empty($_GET['searchText'])){
				$filter['title'] = new \MongoDB\BSON\Regex(preg_quote($_GET['searchText'], '/'),'i');
			}
			$filter['coId'] = $_SESSION['coId'];
;
            // count 조회
            $result["totalCount"] = $this->db->count(self::COLLECTION, $filter);

			// paging
			$pageNavCnt = empty($_GET['pageNavCnt'])?10:$_GET['pageNavCnt'];
			$noapp = empty($_GET['noapp'])?20:$_GET['noapp'];
			$page = empty($_GET['page'])?1:$_GET['page'];
			$pageInfo = new Page;
			$result['page'] = $pageInfo->page($noapp, $pageNavCnt, $result["totalCount"], $page);

			// options
			$options = ['skip' => ($page - 1) * $noapp, 'limit' => $noapp, 'sort' => ["insert.date" => -1]];
            $result['items'] = $this->db->list(self::COLLECTION, $filter, $options);
        } catch(\Exception $e) {
            $result['msg'] = $this->common->getExceptionMessage($e);
        }

		return $result;
	}

	/**
     * 조회
     */
    public function item($id='')
    {
        $data = [];
        $id = empty($id)?(!empty($_GET['id'])?$_GET['id']:''):$id;
        if (!empty($id)) {
            $filter = ['coId'=>$this->coId,'idx' => $id];
            $options = ["projection" => ['_id'=>0]];
            $data = $this->db->item(self::COLLECTION, $filter, $options);
        }

        return $data;
    }

	/**
    * 만들어진 차트 정보 삭제
	* input : 
	*          GET idx : chart index
    * @return Array  [ Data List] 
    */
	public function delete()
	{
		try{
            $filter['coId'] = $_POST["coId"];
            $filter['idx'] = $_POST["idx"];
            $option = [];
            $result = $this->db->delete(self::COLLECTION, $filter, $option);

			$this->chart->makeCategoryListFile($_POST['category']);
		}catch(\Exception $e) {
            $result['msg'] = $this->common->getExceptionMessage($e);
        }

		return $result;
	}


	/**
	 * Excel 다운로드
	 */
	public function excel()
	{
		$filter['coId'] = $_POST['coId'];
		$filter['idx'] = $_POST['idx'];
		$data = $this->db->item(self::COLLECTION, $filter, []);
		
		$fieldSize = sizeof($data['axisX'])+1;
		$rowSize = sizeof($data['axisX'][0]);
		
		$col1='<td height="26" class="xl68" width="{{width}}" style="font-size:9.0pt;
		font-weight:700;
		font-family:굴림체, monospace;
		mso-font-charset:129;
		text-align:center;
		border:1px hairline black;
		background:#DDEBF7;
		mso-pattern:black none;
		white-space:normal; height:20.1pt;">{{val}}</td>';
		$col2='<td class="xl64" width="{{width}}" style="height:20.1pt; font-size:9.0pt;
		font-family:굴림체, monospace;
		mso-font-charset:129;
		text-align:center;
		border:1px hairline black;
		white-space:normal;width:60pt" {{rowspan}}>{{val}}</td>';

		$sheet= '<meta charset="utf-8">';
		$sheet.='<p></p><table class="__se_tbl_ext" border="0" cellpadding="0" cellspacing="0" width="3096" style="border-collapse:
		collapse;width:2325pt">';
		$sheet.='<tbody><tr height="26" style="mso-height-source:userset;height:20.1pt">';
		$sheet.='<tr><td colspan="10">'.$data['title'].'</td></tr>';

		$sheet.='<tr>';
		for($i = 0 ; $i < $fieldSize ; $i++ ){
			if($i == 0){
				$sheet.=str_replace(['{{width}}','{{val}}'],['36',''],$col1);
			}else{
				$sheet.=str_replace(['{{width}}','{{val}}'],['36',$data['fieldTitle'][($i-1)]],$col1);
			}
		}
		$sheet.='</tr>';

		for($i = 0 ; $i < $rowSize ; $i++  ){
					
			$sheet.='<tr>';
			for($j = 0 ; $j < $fieldSize ; $j++  ){
				$tmp = '';
				if($j == 0 ){
					$tmp =$data['axisY'][$i];
				}else{
					$tmp =$data['axisX'][$j-1][$i];
				}
				$sheet.=str_replace(['{{width}}','{{val}}','{{rowspan}}'],['36',$tmp,''],$col2);
			}
			$sheet.='</tr>';
		}

		$sheet.='</tbody>';
		$sheet.='</table>';

		$filename = iconv("UTF-8", "EUC-KR", $data['title']);
		header('Content-Type: application/vnd.ms-excel; charset=utf-8');
		header('Content-Disposition: attachment; filename="'.$filename.'.xlsx"');
		header('Cache-Control: max-age=0');

		print_r($sheet);
	
		/*$spreadsheet = new \PhpOffice\PhpSpreadsheet\Spreadsheet();
		$sheet = $spreadsheet->getActiveSheet();

		$sheet->mergeCells('A1:D1');
		$sheet->getColumnDimension("A1")->setWidth(20);
		$sheet->getRowDimension('1')->setRowHeight(18);
		$sheet->setCellValue("A1", $data['title']);
		$sheet->getStyle("A1")->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
		$sheet->getStyle("A1")->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);

		$cellName = ["A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z"];
		$cells = [
            "A"=>[18,"",""]
		];

		$fieldSize = sizeof($data['axisX'])+1;
		$rowSize = sizeof($data['axisX'][0]);

		for($i = 0 ; $i < $fieldSize ; $i++ ){
			if($i == 0){
			}else{
				$cells[$cellName[$i]] = [50, $data['fieldTitle'][($i-1)], $data['fieldTitle'][($i-1)] ];
			}
		}
		
		for($i = 0 ; $i < $rowSize ; $i++  ){
			$temp = [];
					
			$cnt = $i+4;
			for($j = 0 ; $j < $fieldSize ; $j++  ){
				$tmp = '';
				if($j == 0 ){
					$tmp =$data['axisY'][$i];
				}else{
					$tmp =$data['axisX'][$j-1][$i];
				}
				$sheet->setCellValue($cellName[$j].$cnt, $tmp);
			}
		}*/

		/*$cells = [
            "A"=>[18,"id","기자ID"],
            "B"=>[50,"title","기자명"],
            "C"=>[15,"writeName","기사수"],
            "D"=>[15,"writeName","자체기사"],
            "E"=>[15,"writeName","자체기사비율"],
            "F"=>[15,"categoryName","총PV"]
		];

		//print_r($cells);die();

		foreach ($cells as $key => $val) {
			$cellName = $key.'3';
			$sheet->getColumnDimension($key)->setWidth($val[0]);
			$sheet->getRowDimension('1')->setRowHeight(18);
			$sheet->setCellValue($cellName, $val[2]);
			$sheet->getStyle($cellName)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
			$sheet->getStyle($cellName)->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
		}*/

		/*for($i=0;$i<sizeof($result);$i++){
    		$temp = $result[$i];

			$cnt = $i+4;
			$sheet->setCellValue("A$cnt", $temp["reporterId"]);
            $sheet->setCellValue("B$cnt", $temp["reporterName"]);
			$sheet->setCellValue("C$cnt", $temp["count"]);
			$sheet->setCellValue("D$cnt", $temp["count2"]);
			$sheet->setCellValue("E$cnt", (floor(($temp["count2"]/$temp["count"])*100)).'%');
			$sheet->setCellValue("F$cnt", $temp["pv"]);
		}

		// 파일의 저장형식이 utf-8일 경우 한글파일 이름은 깨지므로 euc-kr로 변환해준다.
		$filename = iconv("UTF-8", "EUC-KR", $data['title']);

		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment; filename="'.$filename.'.xlsx"');
		header('Cache-Control: max-age=0');

		$writer = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($spreadsheet);
		$writer->save('php://output');*/

	}
}