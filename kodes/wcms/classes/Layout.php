<?php 
namespace Kodes\Wcms;

/**
 * 레이아웃 편집, 면편집에서 사용되는 클래스
 * 
 * @author  Kodes <kodesinfo@gmail.com>
 * @version 1.0
 *
 * @license 해당 프로그램은 kodes에서 제작된 프로그램으로 저작원은 코드스(https://www.kode.co.kr)에 있습니다.
 */
class Layout
{
    /** @var String Collection Name */
    protected const COLLECTION = "layout";
    protected const LAYOUT_BOX_COLLECTION = "layoutBox";
    protected const HISTORY_COLLECTION = "layoutHistory";
    protected const BANNER_COLLECTION = "banner";
    protected const EVENT_COLLECTION = "event";

    protected const TEMPLATE_LAYOUT_COLLECTION = "templateLayout";
    protected const TEMPLATE_LAYOUT_BOX_COLLECTION = "templateLayoutBox";

    /** @var Class DB Class */
    protected $db;
    /** @var Class Json Class */
    protected $json;
    /** @var Class Common Class */
    protected $common;
    /** @var Class Log Class */
    protected $log;
    /** @var Class BoardInfo Class */
    protected $boardInfo;

    protected $coId;

    protected $siteDocPath;
    public $layoutPath;

    protected $categoryList;
    protected $seriesList;
    protected $programList;
    protected $boardInfoList;

    /**
     *  Layout 생성자. DB, 시스템 path 세팅.
     */
	public function __construct()
    {
        $this->db = new DB();
        $this->json = new Json();
        $this->common = new Common();
        $this->log = new Log();

        $this->coId = $this->common->coId;

		$this->siteDocPath = $this->common->config['path']['data']."/".$this->coId;
        $this->layoutPath = $this->siteDocPath."/layout";

        $this->categoryList = $this->json->readJsonFile($this->siteDocPath, $this->coId.'_category');
    }

    /**************************************** 면 구성 layout/edit ****************************************/
    /**
     * 면 구성 화면(면 편집 > 면구성)
     */
	public function edit()
    {
        try {
            $data['layoutKind'] = $this->list();
            $data['categoryList'] = $this->categoryList;
        } catch (\Exception $e) {
            $data['msg'] = $this->common->getExceptionMessage($e);
        }

        return $data;
    }

    /**
     * 면 정보 추가.
     * layout 정보 DB 입력 및 json 파일(layoutKind.json) 생성
     * 
     * @return $data 입력된 정보 갯수. 실패시 실패 메세지.
     */
    public function add($input=null)
    {
        $data = [];
        try {
            if (empty($input)) {
                $input = $_POST;
            }
            $input['id'] = trim($input['id']);
            $input['title'] = trim($input['title']);

            if (empty($input['id'])) {
                throw new \Exception("ID를 입력하세요.", 400);
            }
            if (empty($input['title'])) {
                throw new \Exception("이름을 입력하세요.", 400);
            }

			$temp = $this->db->item(self::COLLECTION, ['coId'=>$this->coId, 'id'=>$input['id']], ["projection" => ['_id'=>0]]);
            if (!empty($temp)) {
                throw new \Exception("ID가 중복되었습니다.", 400);
            }
            $temp = $this->db->item(self::COLLECTION, ['coId'=>$this->coId, 'title'=>$input['title']], ["projection" => ['_id'=>0]]);
            if (!empty($temp)) {
                throw new \Exception("이름이 중복되었습니다.", 400);
            }

            $item = $this->common->covertDataField($input, "insert", []);
            $result = $this->db->insert(self::COLLECTION, $item);
			$data = $result->getInsertedCount();

            $items = $this->db->list(self::COLLECTION, ['coId' => $this->coId], ['sort' => ['insert.date' => 1]]);
			$this->json->makeJson($this->layoutPath, 'layoutKind', $items);

            $data = ["message"=>"ok"];
        } catch (\Exception $e) {
            $data['msg'] = $this->common->getExceptionMessage($e);
        }

        return $data;
    }

    /**
     * 페이지 정보 저장
     * 
     * @return $data 페이지 정보를 해당 page에 업데이트한 정보 갯수. 실패시 실패 메세지.
     */
    public function savePage()
    {
        try {
            $pageId = $_POST['pageId'];
            $pageInfo = json_decode($_POST['pageInfo'], true);

            // pageInfo 파일저장
            $this->json->makeJson($this->layoutPath, $pageId."Info", $pageInfo);
            // template html 파일저장
            $templateDir = $this->siteDocPath."/template";
            if (!is_dir($templateDir)) mkdir($templateDir);
            file_put_contents($templateDir.'/'.$pageId.".html", $_POST["template"]);

            // pageInfo, template 내용 DB 업데이트
            $filter = ["coId" => $this->coId, "id" => $pageId];
            $options = ['$set' => [
                "pageInfo" => $pageInfo,
                "template" => $_POST['template']
            ]];
            $result = $this->db->update(self::COLLECTION, $filter, $options );
			$data = $result->getModifiedCount();

            $historyData = [
                "coId"          => $this->coId,
                "id"            => $pageId,
                "flag"          => "edit",
                "type"          => "layout",
                "pageInfo"      => $pageInfo, // type=layout 이면 pageInfo 저장
                "description"   => $pageId.' 정보 저장(Edit)',
                "update"        => $this->common->covertDataField([], 'update')['update']
            ];
            $this->setHistory($historyData);
        } catch (\Exception $e) {
            $data['msg'] = $this->common->getExceptionMessage($e);
        }

        return $data;
	}
    /**************************************** 면 구성 layout/edit ****************************************/
    
    /**************************************** 컨텐츠 편집 layout/patch ****************************************/
    /**
     * 컨텐츠 편집 화면(면 편집 > 컨텐츠 편집)
     */
	public function patch()
    {
        try {
            $data['layoutKind'] = $this->list();
            $data['categoryList'] = $this->categoryList;
        } catch (\Exception $e) {
            $data['msg'] = $this->common->getExceptionMessage($e);
        }

        return $data;
    }

    /**
     * box의 설정 정보를 저장한다.
     */
	public function boxSave()
    {
        try {
            $request = file_get_contents('php://input');
            $jsonData = json_decode($request, true);
            $pageId = $jsonData['pageId'];
            unset($jsonData['pageId']);

            // pageInfo 수정할 정보 설정
            //$pageInfo = $this->json->readJsonFile($this->layoutPath, $pageId."Info");
            $filter['coId'] = $this->coId;
            $filter['id'] = $pageId;
            $pageInfo = $this->db->item(self::COLLECTION, $filter, ["projection" => ['_id'=>0]])['pageInfo'];

            $idx = array_column($pageInfo, "objId");
            $key = array_search($jsonData['objId'], $idx);
            foreach ($jsonData as $k => $v) {
                $pageInfo[$key][$k] = $v;
            }

            // json 저장
            $this->json->makeJson($this->layoutPath, $pageId."Info", $pageInfo);
            
            // pageInfo 내용 DB 업데이트
            $update = $this->common->covertDataField([], 'update');
            $filter = ["coId" => $this->coId, "id" => $pageId];
            $options = ['$set' => ["pageInfo"=>$pageInfo, "update"=>$update['update']]];
            $result = $this->db->update(self::COLLECTION, $filter, $options);
            $data = $result->getModifiedCount();

            // history 저장
            $historyData = [
                "coId"          => $this->coId,
                "id"            => $pageId,
                "flag"          => "patch",
                "type"          => "layout",
                "pageInfo"      => $pageInfo, // type=layout 이면 pageInfo 저장
                "description"   => $jsonData['title'].'('.$jsonData['objId'].') 설정 정보 변경',
                "update"        => $update['update']
            ];
            $this->setHistory($historyData);

            $data = ["message"=>"ok"];
        } catch (\Exception $e) {
            $data['msg'] = $this->common->getExceptionMessage($e);
        }

        return $data;
	}

    /**
     * box 편집 개별 저장
     */
    public function patchSave()
    {
        try {
            // 수정 후
            $request = file_get_contents('php://input');
            $jsonData = json_decode($request, true);

            $saveType = $jsonData['saveType'];
            $pageId = $jsonData['pageId'];

            foreach ($jsonData['box'] as  $obj) {
                $boxId = $obj['boxId'];
                $boxItem = $obj['item'];

                switch (substr($boxId,0,2)) {
                    case 'AC':
                        $boxName=($saveType=="tmp"?"tmp_":"").$boxId;
                        $this->saveArticleBox($boxName, $boxItem, $pageId);
                    break;
                    case 'BC':
                        $boxName=($saveType=="tmp"?"tmp_":"").$boxId;
                        $this->saveBannerBox($boxName, $boxItem);
                    break;
                    case 'BO':
                        $boxName=($saveType=="tmp"?"tmp_":"").$boxId;
                        $this->saveBoardBox($boxName, $boxItem);
                    break;
                    case 'PG':
                        $boxName=($saveType=="tmp"?"tmp_":"").$boxId;
                        $this->saveProgramBox($boxName, $boxItem);
                    case 'GR':
                        $boxName=($saveType=="tmp"?"tmp_":"").$boxId;
                        $this->saveGraphBox($boxName, $boxItem);
                    break;
                }
            }

            $msg = 'main save '.$saveType.' '.$_SESSION['managerId'].' '.$flag."\n";
            $msg .= $jsonData."\n--------------------------------------------------------------\n";
            $this->log->writeLog($this->coId, $msg, "layout");

            // @todo 권한체크를 할 것인가?
            if ($saveType == 'article') {
                // 네이버 뉴스스탠드 생성
                //$naver = new Naver();
                //$naver->makeStand();
            }

            // history 저장
            if ($saveType != "tmp") {
                $historyData = [
                    "coId"          => $this->coId,
                    "id"            => $pageId,
                    "flag"          => "patch",
                    "type"          => $saveType,
                    "data"          => $jsonData['box'], // type 이 박스영역이면 data 저장
                    "description"   => $saveType.' 저장',
                    "update"        => $this->common->covertDataField([], 'update')['update']
                ];
                $this->setHistory($historyData);
            }

            $data['msg'] = '저장이 완료되었습니다.';

        } catch (\Exception $e) {
            $data['msg'] = $this->common->getExceptionMessage($e);
        }

        return $data;
    }

    /**
     * Article Json 파일을 읽어온다.
     */
	public function getArticle($aid)
    {
		if (empty($aid)) {return null;}
		$articleDir = $this->siteDocPath.'/article'.preg_replace("/^".$this->coId."(\d{4})(\d{2})(\d{2})[\d]+$/", "/$1/$2/$3", $aid);
        $data = $this->json->readJsonFile($articleDir, $aid);
        // thumbnail
        $data['thumbnail'] = $this->common->getThumbnail($data['files']);
        $data['thumbnailCaption'] = $this->common->getThumbnailCaption($data['files']);
        // content
        $data["content"] = $this->common->convertTextContent($data["content"]);
    	return $data;
	}

    /**
     * Article Box 저장
     */
    public function saveArticleBox($boxId, $boxItem, $pageId=null)
    {
		$list=[];
		$art = new Article();

        if( empty($boxItem)){
            $list["items"][] = [];
        }else{
            //수정 후
            foreach ($boxItem as $val) {
                $art->setArticlePlace($pageId, $boxId, $val['bindId']);
                $data = $this->getArticle($val['bindId']);
                $data['layoutTitle']['pc'] =  $val['pcTitle'];
                $data['layoutTitle']['mobile'] =  $val['mobileTitle'];
                $data['layoutSubTitle']['pc'] =  $val['pcSubTitle'];
                $data['layoutSubTitle']['mobile'] =  $val['mobileSubTitle'];
                $data['layoutImage']['pc'] =  $val['pcImage'];
                $data['layoutImage']['mobile'] =  $val['mobileImage'];
                $list["items"][] = $data;
            }
        }

		$list["totalCount"] = array_key_exists("items", $list)?count($list["items"]):0;
		$list["makeDate"] = date('Y-m-d H:i:s');
		$list["maker"] = $_SESSION["managerId"];

		$this->json->makeJson($this->layoutPath."/box", $boxId, $list);
        
        $update = $this->common->covertDataField([], 'update');
        $list["update"] = $update['update'];
        $list["type"] = "article";

        $filter = ["coId" => $this->coId, "boxId" => $boxId];
        $options = ['$set' => $list];
        print_r($filter);
        print_r($options);
        $result = $this->db->upsert(self::LAYOUT_BOX_COLLECTION, $filter, $options);
        $data = $result->getModifiedCount();
	}

    /**
     * Banner Box를 생성한다.
     */
	public function saveBannerBox($boxId, $boxItem)
    {
		$bn = new Banner();
		$list = [];
		$this->json->makeJson($this->layoutPath."/box", $boxId, $boxItem);

        $update = $this->common->covertDataField([], 'update');
        $list["update"] = $update['update'];
        $list["type"] = "banner";
        $list["items"] = $boxItem;

        $filter = ["coId" => $this->coId, "boxId" => $boxId];
        $options = ['$set' => $list];
        $result = $this->db->upsert(self::LAYOUT_BOX_COLLECTION, $filter, $options);
    }

    /**
     * HTML Box 저장
     */
	public function saveHtml()
    {
        try {
            $dataInfo = json_decode($_POST['data'],true);
            $this->json->makeJson($this->layoutPath."/box", $dataInfo['objId'], $_POST['data']);

            $pageId = $dataInfo['pageId'];
            $filter['coId'] = $this->coId;
            $filter['id'] = $pageId;
            $pageInfo = $this->db->item(self::COLLECTION, $filter, ["projection" => ['_id'=>0]])['pageInfo'];

            $ids = array_column ($pageInfo,"objId");
            $key = array_search($dataInfo['objId'], $ids);
            $pageInfo[$key]["title"] = $dataInfo['title'];
            $pageInfo[$key]["content"] = $dataInfo['content'];
            $pageInfo[$key]["image"] = $dataInfo['image'];
            $pageInfo[$key]["showPC"] = $dataInfo['showPC'];
            $pageInfo[$key]["showMobile"] = $dataInfo['showMobile'];
            $this->json->makeJson($this->layoutPath, $pageId."Info", $pageInfo);

            $data["result"]= true;
            $data["objId"] = $dataInfo['objId'];

            // DB에 저장
            $update = $this->common->covertDataField([], 'update');
            $list["update"] = $update['update'];
            $list["type"] = "html";
            $list["items"]["title"] = $dataInfo['title'];
            $list["items"]["content"] = $dataInfo['content'];
            $list["items"]["image"] = $dataInfo['image'];
            $list["items"]["showPC"] = $dataInfo['showPC'];
            $list["items"]["showMobile"] = $dataInfo['showMobile'];

            $filter = ["coId" => $this->coId, "boxId" => $dataInfo['objId']];
            $options = ['$set' => $list];
            $result = $this->db->upsert(self::LAYOUT_BOX_COLLECTION, $filter, $options);
            
        } catch (\Exception $e) {
            $data['msg'] = $this->common->getExceptionMessage($e);
        }

        return $data;
	}

    /**
     * Board 내용 조회
     */
	public function getBoard($id, $no)
    {
		if (empty($id) && empty($no)) {return null;}

        $board = new Board();
        $data = $board->item($id, $no);

        // thumbnail
        $data['thumbnail'] = $this->common->getThumbnail($data['files']);
        // content
        $data["content"] = $this->common->convertTextContent($data["content"]);

    	return $data;
	}

    /**
     * Board Box 저장
     */
    public function saveBoardBox($boxName, $boxItem)
    {
		$list=[];

        //수정 후
		foreach ($boxItem as $val) {
			$data = $this->getBoard($val['id'], $val['no']);
            $list["items"][] = $data;
		}

		$list["totalCount"] = array_key_exists("items", $list)?count($list["items"]):0;
		$list["makeDate"] = date('Y-m-d H:i:s');
		$list["maker"] = $_SESSION["managerId"];

		$this->json->makeJson($this->layoutPath."/box", $boxName, $list);
        
        $update = $this->common->covertDataField([], 'update');
        $list["update"] = $update['update'];
        $list["type"] = "board";

        $filter = ["coId" => $this->coId, "boxId" => $boxName];
        $options = ['$set' => $list];
        $result = $this->db->upsert(self::LAYOUT_BOX_COLLECTION, $filter, $options);
        $data = $result->getModifiedCount();
	}

    /**
     * 
     * Grpah Box 저장
     */
    public function saveGraphBox($boxName, $boxItem)
    {
		$list=[];

        print_r($boxItem);
		foreach ($boxItem as $val) {
			$data = $this->getGraph($val['id']);
            $list["items"][] = $data;
		}

		$list["totalCount"] = array_key_exists("items", $list)?count($list["items"]):0;
		$list["makeDate"] = date('Y-m-d H:i:s');
		$list["maker"] = $_SESSION["managerId"];

		$this->json->makeJson($this->layoutPath."/box", $boxName, $list);
        
        $update = $this->common->covertDataField([], 'update');
        $list["update"] = $update['update'];
        $list["type"] = "graph";

        $filter = ["coId" => $this->coId, "boxId" => $boxName];
        $options = ['$set' => $list];
        $result = $this->db->upsert(self::LAYOUT_BOX_COLLECTION, $filter, $options);
        $data = $result->getModifiedCount();
	}

    /**
     * Graph 내용 조회
     */
	public function getGraph($id)
    {
		if (empty($id)) {return null;}

        $graph = new Graph();
        $data = $graph->item($id);

        print_r($data);

    	return $data;
	}

    public function patchEditInfo()
    {
        $data = [];
        try {
            // requestMethod 체크
            $this->common->checkRequestMethod('POST');

            if (empty($_POST['layoutId'])) {
                throw new \Exception("id가 없습니다.", 400);
            }
			// options
            $filter = ['id' => $_POST["layoutId"]];
            $options = ["projection" => ['_id'=>0, 'editing'=>1]];
			$data = $this->db->item(self::COLLECTION, $filter, $options);
        } catch (\Exception $e) {
            $data['msg'] = $this->common->getExceptionMessage($e);
        }

        return $data;
    
    }
    
    public function patchEditing()
    {
        $return = [];
        try {
            // requestMethod 체크
            $this->common->checkRequestMethod('POST');

            if (empty($_POST['layoutId'])) {
                throw new \Exception("id가 없습니다.", 400);
            }
			// action
			$action = '$'.$_POST['action'];
			// filter
			$filter = [
				'id' => $_POST['layoutId']
			];
			// options
			$options = [
				$action => [
					'editing'=>[
						'date' => date("Y-m-d H:i:s"),
						'managerId' => $_SESSION['managerId'],
						'managerName' => $_SESSION['managerName'],
					]
				]
			];
			$result = $this->db->update(self::COLLECTION, $filter, $options);

            $return['msg'] = "적용되었습니다.";
        } catch (\Exception $e) {
            $return['msg'] = $this->common->getExceptionMessage($e);
        }

        return $return;
    }

    /**
     * 레이아웃 복사
     * 
     * POST
    Array
    (
        [source] => layout
        [target] => layoutTemplate
        [sourceId] => main
        [title] => 테스트
        [id] => test
        [copyBox] => N
        [imagePath] => /data/cbs/upload/save/layout/cbs16825123413663.jpg
    )
    * sourceLayout : layout
    Array
    (
        [coId] => cbs
        [id] => main
        [title] => 메인
        [type] => pageEdit
        [insert] => Array
            (
                [date] => 2023-04-15 18:07:49
                [managerId] => admin
                [managerName] => 관리자
            )

        [pageInfo] => Array
            (
                [0] => Array
                    (
                        [parentId] => 
                        [dinNum] => 
                        [objId] => DIN_1682213610705
                        [objType] => din
                        [objectKind] => din1
                        [objClass] => din100
                    )

                [1] => Array
                    (
                        [parentId] => DIN_1682213610705
                        [dinNum] => 1
                        [objId] => PG_1681550033149
                        [objType] => program
                        [title] => TOP sliding box
                        [listType] => M
                        [autoGroupType] => broadcastType
                        [listMerge] => Y
                        [listSort] => Y
                        [broadcastType] => radio
                        [media] => 
                        [showPC] => Y
                        [showMobile] => Y
                        [showArticleCountPC] => 3
                        [showArticleCountMobile] => 3
                        [showThumbPC] => Y
                        [showThumbMobile] => Y
                        [thumbWidthPC] => 
                        [thumbHeightPC] => 760
                        [thumbWidthMobile] => 720
                        [thumbHeightMobile] => 
                        [contentCutNumPC] => 0
                        [contentCutNumMobile] => 0
                        [pcSkin] => box000008
                        [mobileSkin] => box000008
                    )
            )
        [template] => <div class="din din1 din100" data-objid="DIN_1682213610705"><div data-dinnum="1">{PG_1681550033149}</div></div><div class="din din1 " data-objid="DIN_1682213613164"><div data-dinnum="1">{HC_1682337348430}</div></div><div class="din din2-21 pd-bt-90" data-objid="DIN_1682246403471"><div data-dinnum="1">{AC_1682481424745}</div><div data-dinnum="2">{BC_1682246460029}</div></div><div class="din din1 din100" data-objid="DIN_1682213615429"><div data-dinnum="1">{BO_1682246665366}{BO_1682246666953}<div class="din din1 bnn-left" data-objid="DIN_1682247141257"><div data-dinnum="1">{BC_1682247147264}</div></div></div></div><div class="din din3-111 pd-bt-90" data-objid="DIN_1682246969874"><div data-dinnum="1">{BC_1682246972738}</div><div data-dinnum="2">{BC_1682246975082}</div><div data-dinnum="3">{BC_1682246977566}</div></div><div class="din din1 " data-objid="DIN_1681550031035"><div data-dinnum="1">{HC_1682337456138}<div class="din din1 " data-objid="DIN_1682337477121"><div data-dinnum="1">{PG_1682337491198}{PG_1682337492822}</div></div><div class="din din1 " data-objid="DIN_1682337479315"><div data-dinnum="1">{PG_1682337496580}{PG_1682337498244}</div></div><div class="din din1 " data-objid="DIN_1682337481470"><div data-dinnum="1">{PG_1682337502297}{PG_1682337503860}</div></div><div class="din din1 " data-objid="DIN_1682337483760"><div data-dinnum="1">{PG_1682337506494}{PG_1682337508229}</div></div></div></div><div class="din din3-111 pd-bt-90" data-objid="DIN_1682254295463"><div data-dinnum="1">{BC_1682254301877}</div><div data-dinnum="2">{BC_1682254305308}</div><div data-dinnum="3">{BC_1682254307994}</div></div><div class="din din1 din100 bg-bage pd-tp-40  pd-bt-40" data-objid="DIN_1682254560673"><div data-dinnum="1"><div class="din din2-11 dinnum1-border-right" data-objid="DIN_1682254578367"><div data-dinnum="1">{BO_1682254686469}</div><div data-dinnum="2"><div class="din din2-21 " data-objid="DIN_1682254610591"><div data-dinnum="1">{HC_1682337829518}{BC_1682337805112}</div><div data-dinnum="2">{BC_1682254720843}</div></div></div></div><div class="din din1 " data-objid="DIN_1682338242710"><div data-dinnum="1">{HC_1682338188802}<div class="din din1 " data-objid="DIN_1682338267772"><div data-dinnum="1">{BO_1682254770441}</div></div><div class="din din1 " data-objid="DIN_1682338271091"><div data-dinnum="1">{BO_1682338206784}</div></div></div></div></div></div><div class="din din1 din100" data-objid="DIN_1682255557141"><div data-dinnum="1">{PG_1682338110975}</div></div><div class="din din1 " data-objid="DIN_1682255756966"><div data-dinnum="1">{BO_1682255788057}</div></div><div class="din din3-111 pd-bt-90" data-objid="DIN_1682255760809"><div data-dinnum="1">{BC_1682255790808}</div><div data-dinnum="2">{BC_1682255793128}</div><div data-dinnum="3">{BC_1682255795761}</div></div><div class="din din1 bnn-fixed-right" data-objid="DIN_1682255764685"><div data-dinnum="1">{BC_1682255799136}</div></div><div class="din din1 din100" data-objid="DIN_1682255767442"><div data-dinnum="1">{BO_1682255803925}</div></div>
        [update] => Array
            (
                [date] => 2023-04-26 13:25:29
                [managerId] => admin
                [managerName] => 관리자
            )
    )
     */
    public function copyLayout()
    {
        $return = [];
        try {
            // requestMethod 체크
            $this->common->checkRequestMethod('POST');

            if (empty($_POST['id'])) {
                throw new \Exception("id가 없습니다.", 400);
            }
            if (empty($_POST['title'])) {
                throw new \Exception("이름이 없습니다.", 400);
            }

            // source 컬랙션
            $sourceLayout = null;
            $sourceBox = null;
            if ($_POST['source'] == 'layout') {
                $sourceLayout = self::COLLECTION;
                $sourceBox = self::LAYOUT_BOX_COLLECTION;
            } elseif ($_POST['source'] == 'layoutTemplate') {
                $sourceLayout = self::TEMPLATE_LAYOUT_COLLECTION;
                $sourceBox = self::TEMPLATE_LAYOUT_BOX_COLLECTION;
            }
            // target 컬랙션
            $targetLayout = null;
            $targetBox = null;
            if ($_POST['target'] == 'layout') {
                $targetLayout = self::COLLECTION;
                $targetBox = self::LAYOUT_BOX_COLLECTION;
            } elseif ($_POST['target'] == 'layoutTemplate') {
                $targetLayout = self::TEMPLATE_LAYOUT_COLLECTION;
                $targetBox = self::TEMPLATE_LAYOUT_BOX_COLLECTION;
            }

            if (empty($sourceLayout) || empty($targetLayout)) {
                throw new \Exception("source 또는 target 이 없습니다.", 400);
            }

            $id = trim($_POST['id']);
            $title = trim($_POST['title']);

            // target 중복체크
            if ($_POST['target'] == 'layoutTemplate') {
                $filter = ['coId'=>$this->coId, 'id'=>$id];
                $options = ["projection" => ['_id'=>0]];
                $temp = $this->db->item($targetLayout, $filter, $options);
                if (!empty($temp['id'])) {
                    throw new \Exception("id가 중복되었습니다.", 400);
                }
                $filter = ['coId'=>$this->coId, 'title'=>$title];
                $options = ["projection" => ['_id'=>0]];
                $temp = $this->db->item($targetLayout, $filter, $options);
                if (!empty($temp['id'])) {
                    throw new \Exception("이름이 중복되었습니다.", 400);
                }
            }

            // [source] layout 조회
            $filter = ['coId'=>$this->coId, 'id'=>$_POST['sourceId']];
            $options = ["projection" => ['_id'=>0]];
			$layout = $this->db->item($sourceLayout, $filter, $options);

            if (empty($layout['id'])) {
                throw new \Exception("복사 할 원본이 없습니다.", 400);
            }

            // 데이터 변환
            $layout['coId'] = $this->coId;
            $layout['id'] = $id;
            $layout['title'] = $title;
            $layout['isCopy'] = true;
            if (!empty($_POST['imagePath'])) $layout['imagePath'] = $_POST['imagePath'];
            $layout['source'] = $_POST['source'];
            $layout['target'] = $_POST['target'];
            $layout['sourceId'] = $_POST['sourceId'];
            $layout['insert'] = ['date' => date('Y-m-d H:i:s'), 'managerId' => $_SESSION['managerId'], 'managerName' => $_SESSION['managerName']];
            foreach ($layout['pageInfo'] as $key => $value) {
                if (!empty($value['objId'])) {
                    $type = explode('_',$value['objId'])[0];
                    $originalId = $layout['pageInfo'][$key]['originalId'] = $value['objId'];
                    $objId = $layout['pageInfo'][$key]['objId'] = str_replace('.', '', uniqid($type.'_', true)); // objId 재발급
                    if ($type == 'DIN') {
                        // din
                        foreach ($layout['pageInfo'] as $key2 => $value2) {
                            if (!empty($value2['parentId']) && $value2['parentId'] == $originalId) {
                                $layout['pageInfo'][$key2]['parentId'] = $objId;
                            }
                        }
                    } else {
                        // [source] box 조회
                        $filter = ['coId'=>$this->coId, 'boxId'=>$originalId];
                        $options = ["projection" => ['_id'=>0]];
                        $box = $this->db->item($sourceBox, $filter, $options);
                        if (!empty($box['boxId'])) {
                            // [target] box 저장
                            $box['coId'] = $this->coId;
                            $box['boxId'] = $objId;
                            $box['insert'] = ['date' => date('Y-m-d H:i:s'), 'managerId' => $_SESSION['managerId'], 'managerName' => $_SESSION['managerName']];
                            if ($_POST['copyBox'] == 'N') {
                                unset($box['items']);
                                $box['items'] = [];
                            }
                            $box['isCopy'] = true;
                            $box['layoutId'] = $id;
                            unset($box['update']);
                            $filter = ['coId'=>$box['coId'], 'boxId'=>$box['boxId']];
                            $temp = $this->db->upsert($targetBox, $filter, ['$set'=>$box]);
                            // box 파일 저장
                            if ($_POST['target'] == 'layout') {
                                // box
                                $this->json->makeJson($this->layoutPath."/box", $box['boxId'], $box);
                            }
                        }
                    }
                    $layout['template'] = str_replace($originalId, $objId, $layout['template']);
                }
            }
            unset($layout['update']);

            // [target] layout 저장
            $filter = ['coId'=>$layout['coId'], 'id'=>$layout['id']];
            $temp = $this->db->upsert($targetLayout, $filter, ['$set'=>$layout]);

            // 파일 저장
            if ($_POST['target'] == 'layout') {
                // layoutKind
                $items = $this->db->list(self::COLLECTION, ['coId' => $this->coId], ['sort' => ['insert.date' => 1]]);
			    $this->json->makeJson($this->layoutPath, 'layoutKind', $items);

                // layout Info
                $filter['coId'] = $this->coId;
                $filter['id'] = $id;
                $pageInfo = $this->db->item(self::COLLECTION, $filter, ["projection" => ['_id'=>0]])['pageInfo'];
                $idx = array_column($pageInfo, "objId");
                $key = array_search($jsonData['objId'], $idx);
                foreach ($jsonData as $k => $v) {
                    $pageInfo[$key][$k] = $v;
                }
                // json 저장
                $this->json->makeJson($this->layoutPath, $id."Info", $pageInfo);

                $historyData = [
                    "coId"          => $this->coId,
                    "id"            => $id,
                    "flag"          => "edit",
                    "type"          => "layout",
                    "pageInfo"      => $pageInfo, // type=layout 이면 pageInfo 저장
                    "description"   => $id.' 레이아웃 템플릿 복사(Edit)',
                    "update"        => $this->common->covertDataField([], 'update')['update']
                ];
                $this->setHistory($historyData);
            }

            $return['msg'] = "복사 완료되었습니다.";
        } catch (\Exception $e) {
            $return['msg'] = $this->common->getExceptionMessage($e);
        }

        return $return;
    }

    /**
     * 템플릿 레이아웃 리스트
     */
    public function getTemplateLayoutList()
    {
        $return = [];
        try {
            $filter = ['coId' => $this->coId];
            $options = ['sort' => ['insert.date' => 1], "projection" => ['_id'=>0]];
            $return['data'] = $this->db->list(self::TEMPLATE_LAYOUT_COLLECTION, $filter, $options);
        } catch (\Exception $e) {
            $return['msg'] = $this->common->getExceptionMessage($e);
        }
        return $return;
    }

    /**
     * 템플릿 레이아웃
     */
    public function getTemplateLayout($id=null)
    {
        $return = [];
        try {
            if (empty($id) && !empty($_POST['id'])) {
                $id = $_POST['id'];
            }
            if (empty($id)) {
                throw new \Exception("ID가 없습니다.", 400);
            }
            $filter = ['coId' => $this->coId, 'id' => $id];
            $options = ["projection" => ['_id'=>0]];
            $return['data'] = $this->db->item(self::TEMPLATE_LAYOUT_COLLECTION, $filter, $options);
        } catch (\Exception $e) {
            $return['msg'] = $this->common->getExceptionMessage($e);
        }
        return $return;
    }
    /**************************************** //컨텐츠 편집 layout/patch ****************************************/

    /**************************************** 면정보, 컨텐츠 편집 공용 함수 ****************************************/
    /**
     * 면정보 목록 조회
     * 면편집, 컨텐츠 편집에서 면정보 셀렉트박스 표출 위한 정보
     * 권한이 있는 면만 조회
     * 
     * @return $data 면정보 리스트
     */
    public function list()
    {
        try {
            $filter['coId'] = $this->coId;
            if (!$_SESSION['isSuper']) $filter['id'] = ['$in' => $_SESSION['auth']['layout']['patch']];
            $options = ['sort' => ['insert.date' => 1]];
            $data = $this->db->list(self::COLLECTION, $filter, $options);
        } catch (\Exception $e) {
            $data['msg'] = $this->common->getExceptionMessage($e);
        }

        return $data;
    }

    /**
     * 면정보 단일 조회
     * 
     * @return $data 면정보
     */
    public function item($id)
    {
        try {
            if (empty($id)) {
                return null;
            }
            $filter = ['coId'=>$this->coId, 'id'=>$id];
            $options = ["projection" => ['_id'=>0]];
            $data = $this->db->item(self::COLLECTION, $filter, $options);
        } catch (\Exception $e) {
            $data['msg'] = $this->common->getExceptionMessage($e);
        }
        return $data;
    }
    
    /**
     * pageId에 해당하는 페이지 정보를 로딩한다.
     * 
     * @param String [$GET]pageId 조회할 페이지 정보의 ID
     * @return $data 페이지 정보
     */
    public function pageInfo()
    {
		try {
            if (!empty($_POST['pageId']))
            {
                $filter = ['coId'=>$this->coId, 'id' => $_POST['pageId']];
                $options = ["projection" => ['_id'=>0, 'pageInfo'=>1]];
                $data = $this->db->item(self::COLLECTION, $filter, $options);
                $data = $data["pageInfo"];
            } else {
                $data = [];
            }
        } catch(\Exception $e) {
            $data['msg'] = $this->common->getExceptionMessage($e);
        }

        return $data;
    }

    public function boxInfo()
    {
        $data = [];
		try {
            if (!empty($_GET['boxId'])) {
                $filter = ['coId'=>$this->coId, 'boxId'=>$_GET['boxId']];
                $options = ["projection" => ['_id'=>0]];
                $data = $this->db->item(self::LAYOUT_BOX_COLLECTION, $filter, $options);
                if (!empty($data)) {
                    if ($data['type']=='banner') {
                        $items = [];
                        foreach ($data['items'] as $key => $value) {
                            $filter = ['coId'=>$this->coId, 'id'=>$value['bnid']];
                            $options = ["projection" => ['_id'=>0]];
                            $item = $this->db->item(self::BANNER_COLLECTION, $filter, $options);
                            $item['progress']=$this->common->checkProgress($item['startTime'], $item['endTime']);
                            $items[] = $item;
                        }
                        $data['items'] = $items;
                    }
                    if ($data['type']=='board') {
                        $items = [];
                        foreach ($data['items'] as $key => $value) {
                            $filter = ['coId'=>$this->coId, 'id'=>$value['id'], 'no'=>$value['no'], 'status'=>'publish'];
                            $options = ["projection" => ['_id'=>0]];
                            $item = $this->db->item(self::BOARD_COLLECTION, $filter, $options);
                            $items[] = $item;
                        }
                        $data['items'] = $items;
                    }
                }
            }
        } catch(\Exception $e) {
            $data['msg'] = $this->common->getExceptionMessage($e);
        }

        return $data;
    }
    /**************************************** 면정보, 컨텐츠 편집 공용 호출 함수 ****************************************/

    
    /**************************************** 이벤트 페이지 ****************************************/
    /**
     * 이벤트 페이지
     *
     * @return void
     */
    public function event()
    {
		try {
            $data=[];
        
            $filter['coId'] = $this->coId;
            $options = ['sort' => ['insert.date' => 1]];
            $data = $this->db->list(self::EVENT_COLLECTION, $filter, $options);
        
        } catch(\Exception $e) {
            $data['msg'] = $this->common->getExceptionMessage($e);
        }

		return $data;
    }

    public function eventInfo()
    {
		try {
            if (!empty($_GET['eventId'])) {
                $filter = ['coId'=>$this->coId, 'id' => $_GET['eventId']];
                $options = ["projection" => ['_id'=>0]];
                $data = $this->db->item(self::EVENT_COLLECTION, $filter, $options);
            } else {
                $data = [];
            }
        } catch(\Exception $e) {
            $data['msg'] = $this->common->getExceptionMessage($e);
        }

        return $data;
    }
    
    /**
     * 이벤트 페이지 내용 저장
     *
     * @return void
     */
    public function eventSave()
    {	
        $data = [];
        try {
            $eventName=($_POST['saveType']=="tmp"?"tmp_":"").$_POST['id'];
    
            $savePath = $this->siteDocPath."/event/";
            $saveFilePath = $savePath.$eventName.".json";

            $item["coId"]=$this->coId;
            $item["id"]=$_POST['id'];
            $item["title"]=$_POST['title'];
            $item["content"]=str_replace("../data/","/data/", $_POST['content']);
            $item["content"]=str_replace("../event/","/event/", $item["content"]);
            $item["css"]=$_POST["css"];
            $item["js"]=$_POST["js"];
            $item["useYN"]["pc"]=$_POST["useYN"]["pc"];
            $item["sort"]["pc"]=$_POST["sort"]["pc"];
            $item["useYN"]['mobile']=$_POST["useYN"]['mobile'];
            $item["sort"]['mobile']=$_POST["sort"]['mobile'];

            $item = $this->common->covertDataField($item, "insert");

            $this->json->makeJson($savePath, $eventName, $item);
            
            $filter["id"] = $item["id"];
            $filter["coId"] = $item["coId"];
            $result = $this->db->upsert(self::EVENT_COLLECTION, $filter, $item);
			$result->getUpsertedCount();

            // event list 파일 저장
            $filter = [];
            $options = [];
            $filter['coId'] = $this->coId;
            $filter["useYN.pc"]='Y';
            $options = ['sort' => ['sort.pc' => 1], "projection" => ['_id'=>0, "id"=>1, "title"=>1]];
            $data = $this->db->list(self::EVENT_COLLECTION, $filter, $options);
            $this->json->makeJson($savePath, "eventList", $data);
            
            $filter = [];
            $filter['coId'] = $this->coId;
            $filter["useYN.mobile"]='Y';
            $options = ['sort' => ['sort.mobile' => 1], "projection" => ['_id'=>0, "id"=>1, "title"=>1]];
            $data = $this->db->list(self::EVENT_COLLECTION, $filter, $options);
            $this->json->makeJson($savePath, "eventList_m", $data);

            
            $data["message"] = "저장완료";
        } catch (\Exception $e) {
            $data['msg'] = $this->common->getExceptionMessage($e);
        }

		return $data;
    }
    

    public function eventDelete()
    {
        try {
            $filter['id'] = $_POST['id'];
            $filter["coId"]=$this->coId;
            $data = $this->db->delete(self::EVENT_COLLECTION, $filter, true);

            unlink($this->siteDocPath."/event/".$_POST['id'].".json");
        } catch (\Exception $e) {
            $data['msg'] = $this->common->getExceptionMessage($e);
        }
    }
    /**************************************** 이벤트 페이지 ****************************************/

    /**************************************** 히스토리 처리 ****************************************/
    /**
     * 박스 설정 변경시, 기사 저장 정보시
     */
    public function setHistory($data)
    {
		try {
            $filter = ["id" =>$data['id'],"coId"=>$data["coId"]];
            $options = ["sort" => ["historyId" => -1], "limit" => 1];
            $cursor = $this->db->item(self::HISTORY_COLLECTION, $filter, $options);

            $latestId = 0;
            if ( array_key_exists('historyId',$cursor)) {
                $latestId = $cursor["historyId"];
            }
            $data['historyId'] = $latestId+1;
            
            $this->db->insert(self::HISTORY_COLLECTION, $data);
		} catch (\Exception $e) {
			echo 'Message: ' .$e->getMessage();
		}
    }

    public function getHistory()
    {
		try {
            $filter = ["id" =>$_GET['pageId'], "coId"=>$this->coId, 'type'=>'layout', 'flag'=>'patch'];
            $options = ["sort" => ["historyId" => -1], "limit"=>20];
            $items['layout'] = $this->db->list(self::HISTORY_COLLECTION, $filter, $options);

            $filter['type'] ='article';
            $items['article'] = $this->db->list(self::HISTORY_COLLECTION, $filter, $options);

            $filter['type'] ='banner';
            $items['banner'] = $this->db->list(self::HISTORY_COLLECTION, $filter, $options);

            $filter['type'] ='board';
            $items['board'] = $this->db->list(self::HISTORY_COLLECTION, $filter, $options);
			return $items;
		} catch (\Exception $e) {
			echo 'Message: ' .$e->getMessage();
		}
    }

    /**
     * 히스토리 정보로 미리 보기한다.
     */
    public function previewHistory()
    {
		try {
            $pageId = $_GET['pageId'];
            $filter = ["id" =>$_GET['pageId'],"historyId" =>(integer)$_GET['historyId'],"coId"=>$this->coId];
            $options = ["sort" => ["historyId" => -1], "limit"=>1];
            $item = $this->db->item(self::HISTORY_COLLECTION, $filter, $options);

            if ($item["type"]=="layout") {
                $this->json->makeJson($this->layoutPath, "history"."_".$pageId."Info", $item['pageInfo']);
            } else {
                foreach ($item['data'] as  $obj) {
                    $boxId = $obj['boxId'];
                    $boxItem = $obj['item'];
    
                    switch (substr($boxId,0,2)) {
                        case 'AC':
                            $boxName="history_".$boxId;
                            $this->saveArticleBox($boxName, $boxItem);
                        break;
                        case 'BC':
                            $boxName="history_".$boxId;
                            $this->saveBannerBox($boxName, $boxItem);
                        break;
                        case 'BO':
                            $boxName="history_".$boxId;
                            $this->saveBoardBox($boxName, $boxItem);
                        break;
                        case 'PG':
                            $boxName="history_".$boxId;
                            $this->saveProgramBox($boxName, $boxItem);
                        break;
                    }
                }
            }

            return $item['type'];
		} catch (\Exception $e) {
			echo 'Message: ' .$e->getMessage();
		}
    }

    /**
     * 선택한 히스토리 정보로 복구한다
     */
    public function restoreHistory()
    {
		try {
            $pageId = $_GET['pageId'];
            $hid = (integer)$_GET['historyId'];
            $type = $_GET['type'];

            $filter = ["id" =>$_GET['pageId'],"historyId" =>(integer)$_GET['historyId'],"coId"=>$this->coId];
            $options = ["sort" => ["historyId" => -1], "limit"=>1];
            $item = $this->db->item(self::HISTORY_COLLECTION, $filter, $options);

            if ($type=="layout") {
                $pageInfo = $item['pageInfo'];
                // json 저장
                $this->json->makeJson($this->layoutPath, $pageId."Info", $pageInfo);
                
                // pageInfo 내용 DB 업데이트
                $update = $this->common->covertDataField([], 'update');
                $filter = ["coId" => $this->coId, "id" => $pageId];
                $options = ['$set' => ["pageInfo"=>$pageInfo, "update"=>$update['update']]];
                $result = $this->db->update(self::COLLECTION, $filter, $options);
                $data = $result->getModifiedCount();
            } else {
                foreach ($item['data'] as $key => $value) {
                    $boxId = $value['boxId'];
                    $boxItem = $value['item'];
                    if ($type=="article") {
                        $this->saveArticleBox($boxId, $boxItem, $pageId);
                    } elseif ($type=="banner") {
                        $this->saveBannerBox($boxId, $boxItem);
                    } elseif ($type=="board") {
                        $this->saveBoardBox($boxId, $boxItem);
                    } elseif ($type=="program") {
                        $this->saveProgramBox($boxId, $boxItem);
                    }
                }
            }
		} catch (\Exception $e) {
			echo 'Message: ' .$e->getMessage();
		}
    }
    /**************************************** 히스토리 처리 ****************************************/
}