/**
 * 댓글 관련 스크립트
 */

// 로그인 후 처리 (콜백에서 호출 함)
function afterLogin(){
    location.reload();
}

// 정렬
$(document).on('click','[data-sort]',function(e){
    e.preventDefault();
    if($(this).hasClass('active')){
        return;
    }
    $('.sort .item').removeClass('active');
    $(this).addClass('active');
    var order = $(this).data('sort');
    var $item = $(this).closest('.comment_area');
    var aid = $item.data('aid');
    var params = 'aid='+aid+'&order='+order;
    $.ajax({
        url : '/comment/list',
        data : params,
        type : 'post',
        dataType : "json",
        async : false
    })
    .done(function (data, textStatus, errorThrown){
        if(data.result == 'success'){
            var html = '';
            $(data.comment).each(function(i, item){
                html += getCommentHtml(item);
            });
            $('.comment_area .list').html(html);
        }else{
            alert(data.msg);
        }
    });
});

// 댓글입력 체크
$(document).on('change keyup paste click','#comment',function(e){
    e.preventDefault();
    var $form = $('#comment_form');
    var maxLength = 300;
    if(!$form.find('#memberId').val()){
        alert('로그인을 해주세요.');
        $(this).val('');
    }else if($(this).val().length > maxLength){
        $(this).val($(this).val().substring(0, maxLength));
    }
    $('.comment_area .input .button_wrap .length .comment_length').text($(this).val().length);
});

// 댓글 저장
$(document).on('click','#btnSaveComment',function(e){
    e.preventDefault();
    var $form = $('#comment_form');
    if(!$form.find('#memberId').val()){
        alert('로그인을 해주세요.');
    }else if(!$.trim($form.find('#comment').val())){
        alert('댓글을 입력하세요.');
        $form.find('#comment').focus();
    }else{
        $.ajax({
            url : '/comment/insert',
            data : $form.serialize(),
            type : 'post',
            dataType : "json",
            async : false
        })
        .done(function (data, textStatus, errorThrown) {
            if(data.result == 'success'){
                var html = getCommentHtml(data.comment);
                $('.comment_area .list').prepend(html);
                $('.comment_area .list').show();
                $('.comment_area .sort').show();
                $('.comment_area .count .num').text(parseInt($('.comment_area .count .num').text()) + 1);
            }else{
                alert(data.msg);
            }
        });
        $('#comment').val('');
    }
});

// more 버튼
$(document).on('click','.more .open',function(e){
    e.preventDefault();
    $(this).closest('.more').find('.more_menu').toggle();
});

// 추천/비추천
$(document).on('click','.like .up, .like .down',function(e){
    e.preventDefault();
    var $item = $(this).closest('.item');
    var type = '';
    if($(this).hasClass('up')){
        type = 'up';
    }else if($(this).hasClass('down')){
        type = 'down';
    }
    var cid = $item.data('cid');
    var params = 'cid='+cid+'&type='+type;
    $.ajax({
        url : '/comment/addLike',
        data : params,
        type : 'post',
        dataType : "json",
        async : false
    })
    .done(function (data, textStatus, errorThrown){
        if(data.result == 'success'){
            $item.find('.like .up').text(data.comment.up);
            $item.find('.like .down').text(data.comment.down);
        }else{
            alert(data.msg);
        }
    });
});

// 댓글 삭제
$(document).on('click','[data-comment-delete]',function(e){
    e.preventDefault();
    var $item = $(this).closest('.item');
    var cid = $item.data('cid');
    var aid = $item.data('aid');
    var params = 'cid='+cid+'&aid='+aid;
    $.ajax({
        url : '/comment/delete',
        data : params,
        type : 'post',
        dataType : "json",
        async : false
    })
    .done(function (data, textStatus, errorThrown){
        $item.remove();
        $('.comment_area .count .num').text(parseInt($('.comment_area .count .num').text()) - 1);
        alert(data.msg);
    });
});

// 댓글 html 생성
function getCommentHtml(item){
    var memberId = $('.comment_area').data('member-id');
    var html = 
        '<li class="item" data-cid="'+item.cid+'" data-aid="'+item.aid+'" data-write-id="'+item.writeId+'">'
        +'    <img class="thumb" src="'+(item.profileImage?item.profileImage:'/img/man.png')+'" alt="'+item.writeName+'">'
        +'    <div class="text_box">'
        +'        <div class="title">'
        +'            <span class="name">'+item.writeName+'</span>'
        +'            <span class="date">'+item.writeDate+'</span>';
        if(memberId == item.writeId){
            html += 
                '            <div class="more">'
                +'                <div class="open"></div>'
                +'                <div class="more_menu">'
                // +'                    <button class="more_item" data-comment-update>수정하기</button>'
                +'                    <button class="more_item" data-comment-delete>삭제</button>'
                +'                </div>'
                +'            </div>';
        }
        html +=
         '        </div>'
        +'        <div class="comment">'+item.comment.replace(/\n/gi, "<br>")+'</div>'
        +'        <div class="like">'
        +'            <span class="up">'+item.up+'</span>'
        +'            <span class="down">'+item.down+'</span>'
        +'        </div>'
        +'    </div>'
        +'</li>';
    return html;
}