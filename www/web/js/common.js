$(document).ready(function(){
	$('div').removeClass('objEnable ui-sortable');
	
	$("*[data-bannerkind]").each(function(index, obj){
		var kind=$(obj).attr("data-bannerkind");
		var bid=$(obj).attr("data-objid");
		var slideTime=$(obj).attr("data-slidetime");
		slideTime = (slideTime == "" ? 5 : slideTime);
		var size=$(obj).find("li").length;

		switch(kind){
			case "one":
				$(obj).find("li:nth-child(n+2)").hide();
				break;
			case "random":
				var rNum=String(Math.ceil(Math.random()*size));
				$(obj).find("li").hide();
				$(obj).find("li:nth-child("+rNum+")").show();
				break;
			case "slideRight":
				$(obj).closest(".banner").css("overflow","hidden");
				$(obj).css("width",size+"00%");
				$(obj).find("li").css("width",String(100/size)+"%");
				$(obj).find("li").css("float","left");
				$(obj).css("position","relative");
				if(size > 1){
					setInterval(function(){rightSlideBanner(obj);}, (slideTime*1000));
				}
				break;
			case "stack":
				$(obj).closest(".banner").find("li").css("margin-bottom","10px")
				break;
		}
	});

	// 광고영역 소재가 없을 경우 히든
	$('.banner > ul').each(function(){
		var isRemove = false;
		if($(this).children('li').length == 0){
			isRemove = true;
		}
		if($(this).children('li').length == 1){
			if($(this).children('li').html() == ''){
				isRemove = true;
			}
		}
		if(isRemove){
			$(this).closest('.local_box').remove();
			$(this).closest('.banner').remove();
		}
	});
});


function rightSlideBanner(obj){
	/* requestAnimationFrame을 지원하는 브라우저에서는 애니메이션이 일시중지되며 queue에 쌓이게 됨
	   queue에 쌓이지 않도록 체크 
	   https://stackoverflow.com/questions/7434186/jquery-animation-issue-with-loop-with-setinterval-in-firefox-if-tab-loses-focus
	*/
	if($(obj).queue('fx').length == 0){
		$(obj).animate({left:"-100%"},1000,function(){
			$(obj).append($(obj).find("li:first-child"));
			$(obj).css('left',"0");
		});
	}
}

// 이미지명에 사이즈 추가
function setThumbSize(img, w, h, p){
	if(img){
		if(img.indexOf('.jpg') > -1){
			img.replace('.jpg','.'+w,+'x'+h+'.'+p+'.jpg');
		}
	}
	return img;
}

// Onair Tv Popup
// let openOnairTvPopup = () => {
// 	const onairTvPopup = window.open('/onair/tv', 'onairTvPopup', 'width=780, height=600');
// 	onairTvPopup.onload;
// };

// Onair Radio Popup
// let openOnairRadioPopup = () => {
// 	const onairRadioPopup = window.open('/onair/radio', 'onairRadioPopup', 'width=520, height=430');
// 	onairRadioPopup.onload;
// };

// let year = new Date().getFullYear();
// let month = new Date().getMonth()+1;
// month = String(month).padStart(2,'0');
// let day = new Date().getDate();
// day = String(day).padStart(2,'0');
// let hour = new Date().getHours();
// hour = String(hour).padStart(2,'0');
// let minutes = new Date().getMinutes();
// minutes = String(minutes).padStart(2,'0');


// // 프로그램 정보를 읽어서 네비게이션 구성.
// $(function(){
//     $.ajax({
// 		type: "GET",
// 		url : "/nav/?returnType=ajax",
// 		dataType:"json",
// 		success : function(response) {
// 		    let data = response.result.tv;
//             for(let cnt=0;cnt < data.length; cnt++){
// 				let link = '/program/home/'+data[cnt]['id'];
// 				let target = '';
// 				if(data[cnt]['navLink']){
// 					link = data[cnt]['navLink'];
// 					target = 'target="'+data[cnt]['navLinkTarget']+'"';
// 				}
// 				$('li[data-id="kbc002000000"]').find('ul').append('<li data-id="'+data[cnt]['id']+'"><a href="'+link+'" '+target+'>'+data[cnt]['name']+'</a></li>');
// 			}

// 		    let data2 = response.result.radio;
// 			$('li[data-id="kbc003000000"]').find('ul').empty();
//             for(let cnt=0;cnt < data2.length; cnt++){
// 				let link = '/program/home/'+data2[cnt]['id'];
// 				let target = '';
// 				if(data2[cnt]['navLink']){
// 					link = data2[cnt]['navLink'];
// 					target = 'target="'+data2[cnt]['navLinkTarget']+'"';
// 				}
// 				$('li[data-id="kbc003000000"]').find('ul').append('<li data-id="'+data2[cnt]['id']+'"><a href="'+link+'" '+target+'>'+data2[cnt]['name']+'</a></li>');
// 			}

// 			// $('li[data-id="kbc001000000"] [href="/article/list/kbc001010000"]').append('<ul></ul>');
// 			$(response.result.news).each(function(i, item){
// 				let link = '/program/home/'+item.id;
// 				let target = '';
// 				if(item.navLink){
// 					link = item.navLink;
// 					target = 'target="'+item.navLinkTarget+'"';
// 				}
// 				$('li[data-id="kbc001000000"] [href="/article/list/kbc001010000"]').closest('ul').append('<li data-id="'+item.id+'"><a href="'+link+'" '+target+'>'+item.name+'</a></li>');
// 				// $('li[data-id="kbc001000000"] [href="/article/list/kbc001010000"] ul').append('<li data-id="'+item.id+'"><a href="'+link+'" '+target+'>'+item.name+'</a></li>');
// 			});
//         }
//     })
// })