/**
 * 회원 관련 스크립트
 */

// 소셜 로그인
$(document).on('click','[data-social-login]',function(){
    var memberId = $(this).closest('[data-member-id]').data('member-id');
    var provider = $(this).closest('[data-provider]').data('provider');
    if(memberId){
        if (provider) {
            alert(provider+' 계정으로 로그인 되어 있습니다.');
        } else {
            alert('이미 로그인 되어 있습니다.');
        }
    }else{
        var provider = $(this).data('social-login');
        var apiUrl = $(this).data('api-url');
        if(provider == 'KAKAO'){
            window.open(apiUrl,'login','width=464,height=682,top=100,left=100');
        }else{
            window.open(apiUrl,'login','width=600,height=700,top=100,left=100');
        }
    }
});