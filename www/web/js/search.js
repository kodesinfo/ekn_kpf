$(function(){
    $(document).on('click',"#searchBtn",function(){
        search();
    });
    
    $(document).on('keyup', '#searchText', function(e){
        if(e.keyCode == 13){
            search();
        }
    })
    
    let search = () =>{
        if($("#searchText").val()==''){
            alert('검색어를 입력해주세요.');
            return false;
        }
        location.href="/article/search?searchText="+$("#searchText").val();
    }
})