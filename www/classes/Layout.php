<?php 
class Layout extends \Kodes\Www\Layout
{
    /**
     * Layout View
     */
    public function view()
	{
        // 상속받은 함수
        $return = parent::view();

		return $return;
	}

     /**
     * Layout photoView
     */
    public function photoView()
	{
        // 상속받은 함수
        $return = parent::photoView();

		return $return;
	}
}