<?php
ini_set('display_errors', 0);

/**
 * Index 클래스
 * 
 * @author  Kodes <kodesinfo@gmail.com>
 * @version 1.0
 *
 * @license 해당 프로그램은 kodes에서 제작된 프로그램으로 저작원은 코드스(https://www.kode.co.kr)
 */
class Index
{
    /** @var const */
    const COID = 'sfn'; // 기업코드 (필수)

    /** @var Class */
    private $common;
    private $tpl;
    private $json;
    private $api;
    private $apiPage;

    /** @var variable */
    private $coId;
    private $action;
    private $method;
    private $id;
    private $deviceType = 'pc';
	private $isError = false;

    /** @var pageCache */
    private $usePageCache = false;                 // page cache 사용여부
    private $useMainPageCache = true;             // page cache 메인페이지 적용여부
    private $pageCacheUrls = ['/article/view'];   // page cache 적용 url

    /**
     * 생성자
     */
    public function __construct()
    {
        // coId 설정
        $GLOBALS['coId'] = $this->getCoId();
        $this->coId = $GLOBALS['coId'];
        $this->urlRedirect();

        $this->common = new \Kodes\Www\Common();
        $this->json = new \Kodes\Www\Json();
        $this->api = new \Kodes\Www\Api();


        $this->setDevice();
        $this->setParameter();
        if ($this->usePageCache) {
            $this->apiPage = new \Kodes\Www\Api();
            $this->printPageCache();
        }
        $this->setTemplate();
        $this->setConfig();
        $this->runClass();
        $this->commonArea();
        $this->print();
    }

    /**
     * Domain으로 coid 가지고 오기
     */
    private function getCoId()
    {   
        /* 테스트 서버 도메인 체크 */
        if(preg_match('/([a-z0-9]+)[-]test.kode.co.kr/i',$_SERVER['HTTP_HOST'],$tmp)){
            return $tmp[1];
        }

        /* 개별 서버 도메인 체크 */
        if(preg_match('/([a-z0-9]{3})[0-9]{0,2}.kode.co.kr/i',$_SERVER['HTTP_HOST'],$tmp)){
            return $tmp[1];
        }

        foreach(scandir("/webData") as $val){
            $companyFile = "/webData/".$val."/config/".$val."_company.json";
            if(is_file($companyFile)){
                $companyInfo=(json_decode(file_get_contents($companyFile),true));

                if(str_replace(['http://','https://'],'',$companyInfo['domain']['pc'])==$_SERVER['HTTP_HOST'] || str_replace(['http://','https://'],'',$companyInfo['domain']['mobile'])==$_SERVER['HTTP_HOST'] || str_replace(['http://','https://'],'',$companyInfo['domain']['rep'])==$_SERVER['HTTP_HOST']){
                    return $companyInfo['coId'];
                }
            }
        }
    }

    public function urlRedirect(){
        if(!empty($_GET['idxno'])){
            $aid = file_get_contents("/webData/".$this->coId."/oldId/".$_GET['idxno']);
            header("HTTP/1.1 303 See Other");
            header("Location: //$_SERVER[HTTP_HOST]"."/article/view/".$aid);
            die();
        }        
    }

    /**
     * 공통 영역 설정
     */
    private function commonArea()
    {
        // header
        $request['id'] = 'header';
        $request['contentType'] = 'layout';
        $request['dataType'] = 'html';
        $request['deviceType'] = $this->deviceType;
        $this->data['header'] = $this->api->data('getLayout', $request);
        
        // footer 20221025추가
        $request['id'] = 'footer';
        $request['contentType'] = 'layout';
        $request['dataType'] = 'html';
        $request['deviceType'] = $this->deviceType;
        $this->data['footer'] = $this->api->data('getLayout', $request);
    }
    
    /**
     * 디바이스 설정 : pc/mobile
     */
    private function setDevice()
    {
        // HTTP_USER_AGENT 추출
        $mobilePatten = '/(iphone|android|lgtelecom|skt|mobile|samsung|nokia|blackberry|sony|phone)/';

        $this->deviceType = preg_match($mobilePatten, strtolower($_SERVER['HTTP_USER_AGENT']))?'mobile':'pc';
        // if (empty($_COOKIE['device']) || $_COOKIE['device'] != 'pc') {
        //     $this->deviceType = preg_match($mobilePatten, strtolower($_SERVER['HTTP_USER_AGENT']))?'mobile':'pc';
        // }

	    // 모바일 도메인 체크 : m.
		if (strpos($_SERVER['HTTP_HOST'], 'm.') !== false) {
			$this->deviceType = 'mobile';
		}
        // 쿠키 체크
        // if (array_key_exists('device', $_COOKIE)) {
        //     if ($_COOKIE['device'] == 'mobile') {
        //         $this->deviceType = 'mobile';
        //     }
		// }
        // url 설정
        if (array_key_exists('d', $_GET)) {
            if (strtolower($_GET['d']) == 'pc') {
                $this->deviceType = 'pc';
            }else if(strtolower($_GET['d']) == "mobile"){
                $this->deviceType = 'mobile';
            }
            if (!empty($this->deviceType)) {
                setcookie('device', $this->deviceType, time() + 3600);
            }
        }

        $GLOBALS['deviceType'] = $this->deviceType;
    }

    /**
     * page cache 조회하여 출력
     */
    private function printPageCache()
    {
        // page cache 조회
        if (empty($_GET['noCache'])) {
            if ($this->usePageCache && ($this->common->strposArray($_SERVER['REQUEST_URI'], $this->pageCacheUrls) || ($this->useMainPageCache && $this->action=='main'))) {
                $_GET['device'] = $this->deviceType;
                $cacheData = $this->apiPage->getPageCache($_SERVER['REQUEST_URI']);
                if ($cacheData) {
                    die($cacheData);
                }
            }
        }
    }

    /**
     * 템플릿 클래스 설정
     * 
     * ex) www.mojaik.com/$action/$method/$id
     * ex) www.mojaik.com/category/$action/$method/$id
     */
    private function setTemplate()
    {
        $this->tpl = new \Kodes\Www\Template_();
        $this->tpl->compile_dir = '_compile/' . $this->deviceType;
        $this->tpl->template_dir = '_template/' . $this->deviceType;
    }

    /**
     * 디렉토리 주소를 parameter로 변경
     */
    private function setParameter()
    {
        if (array_key_exists('_url',$_GET)) {
            preg_match_all('/\/([^\/]+)/', $_GET['_url'], $tmp);
    
            if (count($tmp[1]) == 4) {
                $this->action = isset($tmp[1][0])?$tmp[1][1]:null;
                $this->method = isset($tmp[1][1])?$tmp[1][2]:null;
                $this->id = isset($tmp[1][2])?$tmp[1][3]:null;
            } else {
                $this->action = isset($tmp[1][0])?$tmp[1][0]:null;
                $this->method = isset($tmp[1][1])?$tmp[1][1]:null;
                $this->id = isset($tmp[1][2])?$tmp[1][2]:null;
            }
        } else {
            //$this->action = 'main';
            $this->action = 'datacenter';
        }
    }

    /**
     * 설정 정보
     */
    private function setConfig()
    {
        $filePath = "/webData/".$GLOBALS['coId'];
        $this->data['company'] = $GLOBALS['company'] = $this->json->readJsonFile($filePath.'/config', $GLOBALS['coId']."_company");
        $this->data['category'] = $GLOBALS['category'] = $this->json->readJsonFile($filePath, $GLOBALS['coId']."_category");
        $this->data['categoryTree'] = $GLOBALS['categoryTree'] = $this->json->readJsonFile($filePath, $GLOBALS['coId']."_categoryTree");
        $this->data['eventList'] = $this->json->readJsonFile($filePath.'/event', "eventList");
        $this->data['eventList_m'] = $this->json->readJsonFile($filePath.'/event', "eventList_m");
    }

    /**
     * class/method 실행
     */
    private function runClass()
    {
        // class
        $className = ucfirst($this->action);
        $class = null;
        if (class_exists($className)) {
            $className = $className;
        } elseif (class_exists('\Kodes\Www\\'.$className)) {
            $className = '\Kodes\Www\\'.$className;
        } else {
            $className = null;
            // 클래스 없음
            $this->isError = true;
        }
        if (!empty($className)) {
            $class = new $className($this->id);
        }

        // method
        if ($class) {
            $methodName = null;
            if ($this->method && method_exists($class, $this->method)) {
                $methodName = $this->method;
            // } elseif ($this->etc && method_exists($class, $this->etc)) {
            //     $methodName = $this->etc;
            } elseif ($this->action && method_exists($class, $this->action)) {
                $methodName = $this->action;
            } else {
                $methodName = null;
                // 메서드 없음
                $this->isError = true;
            }
            if (!empty($methodName)) {
                // 실행결과
                $this->data['result'] = $class->$methodName();
            }
        }
    }

    /**
     * 화면 출력
     */
	private function print()
    {
        $baseTemplate = $this->action.'/'.$this->method.'.html';
        if (!empty($this->data['result']['skin'])) {
            // 개별 skin을 설정한 경우
            $baseTemplate = $this->action.'/'.$this->data['result']['skin'].'.html';
        } elseif (empty($this->method)) {
            // $method 없으면 [$class].html
            $baseTemplate = $this->action.'/'.$this->action.'.html';
        }

        // returnType이 ajax 이면 json 출력
        $returnType = "";
        if(!empty($_GET['returnType'])){
            $returnType = $_GET['returnType'];
        }

		if ( $returnType == 'ajax') {
			echo json_encode($this->data, true);

        // template file 이 있으면 출력
		} elseif (is_file($this->tpl->template_dir.'/'.$baseTemplate)) {
            /**
             * baseTemplate에 포함된 서브 template을 추출하여 등록
                _template/pc/data/[coId]/template
                _template/pc/layout/template
                _template/pc/layout
                _template/pc/module
             */
            $baseTmp = file_get_contents($this->tpl->template_dir.'/'.$baseTemplate);
            preg_match_all('/\{#[ ]*([a-z0-9A-Z ]+)\}/', $baseTmp, $tmp);
            $template['index'] = $baseTemplate;
            foreach ($tmp[1] as $val) {
				if(is_file($_SERVER['DOCUMENT_ROOT'].'/'.$this->tpl->template_dir.'/data/'.$this->coId.'/template/'.$val.'.html')){
                    $template[$val] = 'data/'.$this->coId.'/template/'.$val.'.html';
                }else if(is_file($_SERVER['DOCUMENT_ROOT'].'/'.$this->tpl->template_dir.'/layout/template/'.$val.'.html')){
                    $template[$val] = 'layout/template/'.$val.'.html';
                }else if(is_file($_SERVER['DOCUMENT_ROOT'].'/'.$this->tpl->template_dir.'/layout/'.$val.'.html')){
                    $template[$val] = 'layout/'.$val.'.html';
				}else if(is_file($_SERVER['DOCUMENT_ROOT'].'/'.$this->tpl->template_dir.'/module/'.$val.'.html')){
	                $template[$val] = 'module/'.$val.'.html';
				}
            }
            $this->tpl->define($template);

            // html 출력
            if (!empty($this->data)) {
                $this->tpl->assign($this->data);
            }
            $this->tpl->print_('index');

            // page cache 저장
            if ($this->usePageCache && ($this->common->strposArray($_SERVER['REQUEST_URI'], $this->pageCacheUrls) || ($this->useMainPageCache && $this->action=='main'))) {
                $this->apiPage->setPageCache($this->tpl->fetch('index'));
            }

        } elseif ($this->isError) {
            // class, template 모두 없으면
            http_response_code(404);    // Not found
            exit;
        }
    }
}