<?php
include_once("../classes/autoload.php");

// session
session_set_save_handler(new \Kodes\Wcms\DBSessionHandler(), true); // 세션을 DB에 저장
session_start();

header('X-XSS-Protection: 0');

//phpinfo();

$idx = new Index();