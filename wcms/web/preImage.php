<?php
include_once("/webSiteSource/kodes/wcms/classes/Json.php");
ini_set('memory_limit','2048M');
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL & ~E_DEPRECATED & ~E_NOTICE);

$docRoot = $_SERVER["DOCUMENT_ROOT"];
$filePath = $docRoot.$_SERVER["REQUEST_URI"];
$cacheFilePath = str_replace("/image/","/cache/",$filePath);

$json = new \Kodes\Wcms\Json();

if (!is_file($filePath) && !is_file($cacheFilePath)) {
    $fileName = $_GET["fileName"];
    preg_match("/\/data\/([a-z]+)(([\/][^\/]+)+)[\/]([a-zA-Z0-9_]+)[.]([^.]+)[.]([^.]+)[.]([^.]+)$/i",$fileName,$matches);
    $site = $matches[1];

    $matchSize = sizeof($matches);
    $fileExt = $matches[($matchSize-1)];
    $watermarkPlace = intval($matches[($matchSize-2)]);
    $imgSize = explode("x",$matches[($matchSize-3)]);

    $orgFile=$docRoot.str_replace(['.'.$matches[($matchSize-3)],'.'.$watermarkPlace],'',$fileName);

    if (is_file($orgFile)) {
        $imageType = exif_imagetype($orgFile);
        header("Content-type: " . image_type_to_mime_type($imageType));

        // Get new sizes
        list($width, $height) = getimagesize($orgFile);
        
        $top = 0;
        $left = 0;
        if (empty($imgSize[0])) {
            $rate = $imgSize[1] / $height;
            $imgSize[0]=$width * $rate;
        } elseif (empty($imgSize[1])) {
            $rate = $imgSize[0] / $width;
            $imgSize[1]=$height * $rate;
        } else {
            $wRate = $imgSize[0] / $width;
            $hRate = $imgSize[1] / $height;

            if ($wRate > $hRate) {
                $rate =  $wRate;
                $top = ( ( $height * $rate ) - $imgSize[1] ) / 2;
            } else {
                $rate =  $hRate;
                $left = ( ( $width * $rate ) - $imgSize[0] ) / 2;
            }
        }
      
        // Load
        $thumb = imagecreatetruecolor($imgSize[0], $imgSize[1]);

        switch ($imageType) {
            case 1:
                $source = imagecreatefromgif ($orgFile);
                break;
            case 2:
                $source = imagecreatefromjpeg($orgFile);
                break;
            case 3:
                // png는 배경 투명하게 처리
                @imagealphablending($thumb, false);
                @imagesavealpha($thumb, true);
                $source = imagecreatefrompng($orgFile);
        }

        imagecopyresampled($thumb, $source, 0, 0, $left, $top, ($width*$rate) , ($height*$rate), $width, $height);

        // 워터마크 처리
        if ($watermarkPlace > 0) {
            $companyInfo = $json->readJsonFile("/webData/".$site."/config", $site."_company");
            // $companyInfo = $json->readJsonFile("/webSiteSource/wcms/config", $site."_company");
            $wartermakeImg = empty($companyInfo['image']['watermark'])?null:$docRoot.$companyInfo['image']['watermark'];
            if (!empty($wartermakeImg) && is_file($wartermakeImg)) {
                $defaultMargin = 10;

                $imgInfo = getimagesize($wartermakeImg);
                $tmp = explode("/", $imgInfo['mime']);
                $watermarkExt = $tmp[1];
                if($watermarkExt == "jpg" || $watermarkExt == "jpeg"){
                    $wmSource = imagecreatefromjpeg($wartermakeImg);
                }else if($watermarkExt == "png"){
                    $wmSource = imagecreatefrompng($wartermakeImg);
                }else if($watermarkExt == "bmp" || $watermarkExt == "wbmp"){
                    $wmSource = imagecreatefromwbmp($wartermakeImg);
                }else if($watermarkExt == "gif"){
                    $wmSource = imagecreatefromgif($wartermakeImg);
                }
                
                //$wmSource = imagecreatefrompng($wartermakeImg);
                
                list($wmWidth, $wmHeight) = getimagesize($wartermakeImg);
    
                // 가로 정렬
                switch ((int)(($watermarkPlace-1) / 3)) {
                    case 1: // 좌측 정렬
                        $top = $imgSize[1]/2 - $wmHeight/2;
                        break;
                    case 2: // 가운데 정렬
                        $top = $imgSize[1] - $wmHeight - $defaultMargin;
                        break;
                    case 0:  // 우측 정렬
                        $top = $defaultMargin;
                        break;
                }
                // 세로 정렬
                switch ($watermarkPlace % 3) {
                    case 1: // 상단 정렬
                        $left = $defaultMargin;            
                        break;
                    case 2: // 중앙 정렬
                        $left = $imgSize[0]/2 - $wmWidth/2;
                        break;
                    case 0:  // 하단 정렬
                        $left = $imgSize[0] - $wmWidth - $defaultMargin;    
                        break;
                }
                imagecopy($thumb, $wmSource, $left,  $top, 0, 0,$wmWidth, $wmHeight);
                imagedestroy($wmSource);
            }
        }

        // cache dir가 없는 경우 만들기
        preg_match("/(^[\/]([^\/]+[\/])+)/i",$cacheFilePath,$tmp);
        if (!is_dir($tmp[0])) {
            mkdir($tmp[0],0777,true);
        }

        switch ($imageType) {
            case 1:
                imagegif($thumb);
                imagegif($thumb,$cacheFilePath);
                break;
            case 2:
                imagejpeg($thumb,NULL,100);
                imagejpeg($thumb,$cacheFilePath,100);
                break;
            case 3:
                imagepng($thumb);
                imagepng($thumb,$cacheFilePath);
                break;
        }
        imagedestroy($source);
        imagedestroy($thumb);
    } else {
        header("HTTP/1.0 404 Not Found");
    }
} else {
    if (is_file($filePath)) {
        header("Content-type: " . image_type_to_mime_type( exif_imagetype($filePath) ) );
        checkHttpCache($filePath);
        readfile($filePath);
    } else {
        header("Content-type: " . image_type_to_mime_type( exif_imagetype($cacheFilePath) ) );
        checkHttpCache($cacheFilePath);
        readfile($cacheFilePath);
    }
}

// 브라우저 캐시 사용
function checkHttpCache($filePath)
{
    if (is_file($filePath)) {
        $response['Last-Modified'] = gmdate('D, d M Y H:i:s', filemtime($filePath)).' GMT';
        $response['ETag'] = md5($response['Last-Modified']);
        $ifModifiedSince = isset($_SERVER['HTTP_IF_MODIFIED_SINCE']) ? $_SERVER['HTTP_IF_MODIFIED_SINCE'] == $response['Last-Modified'] : false;
        $ifNoneMatch = isset($_SERVER['HTTP_IF_NONE_MATCH']) ? $_SERVER['HTTP_IF_NONE_MATCH'] == $response['ETag'] : false;
        header('Last-Modified: '.$response['Last-Modified']);
        header('ETag: '.$response['ETag']);
        if ($ifModifiedSince !== false || $ifNoneMatch !== false) {
            http_response_code(304);    // Not Modified
            exit;
        }
    }
}