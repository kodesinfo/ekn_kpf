let htmlCodeEditor;
$(()=>{
    htmlCodeEditor = CodeMirror.fromTextArea(document.getElementById("content"), {
        lineNumbers: false,
        mode: "application/x-ejs",
        indentUnit: 4,
        indentWithTabs: true,
        dragDrop: true,
        viewportMargin: 4,
        theme: '3024-night',
		autoRefresh: true
    });
    htmlCodeEditor.setSize('100%', '500px');
    htmlCodeEditor.on('drop', (data, e)=>{
        let datas = e.dataTransfer.files;
    
        const formData = new FormData();
        for(let cnt =0; cnt < datas.length; cnt++){
            files = datas[cnt];
            formData.append('file', files);
            
            $.ajax({
                url: '/file/save/ajax',
                type: 'POST',
                data: formData,
                async: true,
                success: (response)=>{
                    let result = JSON.parse(response);
                    let imgHtml ='<img src="'+result.result.path+'"/>';
        
                    insertText(imgHtml);
                },
                cache: false,
                contentType: false,
                processData: false
            });
        };
    });
});

function insertText(data) {
	//htmlCodeEditor.setValue(data);
	const doc = htmlCodeEditor.getDoc();
	const cursor = doc.getCursor();
	const line = doc.getLine(cursor.line);
	const pos = {
		line: cursor.line
	};
	if (line.length === 0) {
	 	doc.replaceRange(data, pos);
	} else {
	 	doc.replaceRange("\n" + data, pos);
	}
	setTimeout(function () {
		htmlCodeEditor.refresh()
	}, 500)
}


let htmlSettingData = (e) =>{
	const modal = $(e.target).closest('.modal-content');

	let dataObject = new Object();    
    dataObject['objId'] = modal.find("#objId").val();
	dataObject['title'] = modal.find('#title').val();
	dataObject['showPC'] = modal.find('#showpc .toggle').data('toggles').active?"Y":"N";
	dataObject['showMobile'] = modal.find('#showmobile .toggle').data('toggles').active?"Y":"N";
	dataObject['content'] = htmlCodeEditor.getValue();

	let selectBox = $('[data-objId="'+dataObject['objId']+'"]');
	selectBox.find('.card-header > h6').text(dataObject['title']);
	for (const [key, value] of Object.entries(dataObject)) {
		selectBox.attr('data-'+key.toLowerCase() , value);
	}

	dataObject['pageId'] = $("#layoutSelect").val();
    return dataObject;
}

// skin preview
let setPreview = (modal)=>{
	var modal = $(modal);

	// skin preview
	if(modal.find('#pcSkin').val()){
		modal.find("#pcSkinPreview").attr('src', modal.find('#pcSkin option:selected').data('json').thumbnail.pc.path);
	}else{
		modal.find("#pcSkinPreview").attr('src', '/img/skin/show-hidden-icons.png');
	}
	if(modal.find('#mobileSkin').val()){
		modal.find("#mobileSkinPreview").attr('src', modal.find('#mobileSkin option:selected').data('json').thumbnail.mobile.path!=""?modal.find('#mobileSkin option:selected').data('json').thumbnail.mobile.path:modal.find('#pcSkin option:selected').data('json').thumbnail.pc.path);
	}else{
		modal.find("#mobileSkinPreview").attr('src', '/img/skin/show-hidden-icons.png');
	}
}
