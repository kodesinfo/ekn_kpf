/** Dropzone 단일파일 업로드 **************************************************************************/
/**
 * dropzone init 단일파일용
 * 
 * uploadDir : 업로드 디렉토리
 * objId : #dropzone 상위요소
 * pathId : 변수 저장 input
 * path : 초기화 파일 경로
 */
function initSaveOneDropzone(uploadDir, objId, pathId, path) {
    var fileDropzone = new Dropzone(objId+" #dropzone", {
        url: "/file/save/ajax",
        maxFiles: 1,
        init: function() {
            setFileSaveOneDropzone(objId, pathId, path);
        },
    });
    // 업로드 시작
    fileDropzone.on('addedfile', (file)=>{
        // 미리보기 제거
        $(objId+' .dz-details').remove();
        if (fileDropzone.files.length > 1) {
            fileDropzone.removeFile(fileDropzone.files[0]);
        }
    });
    // 업로드 중
    fileDropzone.on('sending', (file, xhr, formData)=>{
        // 작업중 로딩 : 시작
        formData.append('path', uploadDir);
        $(objId+" #load-staus").empty().append('파일을 업로드중입니다.');
        $(objId+' #workingpopup').show();
    });
    // 업로드 완료
    fileDropzone.on("complete", (file)=>{
        uploadInfo = JSON.parse(file.xhr.responseText)['result'];
        console.log(uploadInfo);
        $(objId+' '+pathId).val(uploadInfo["path"]);
        setFileSaveOneDropzone(objId, pathId, uploadInfo["path"]);
        // 작업중 로딩 : 종료
        $(objId+' #workingpopup').hide();
    });
    // 파일 삭제
    $(document).on('click', objId+' #dropzone .trash-icon', function(){
        if(confirm('등록된 파일을 삭제하시겠습니까?')){
            $(objId+' #dropzone div').remove();
            $(objId+' '+pathId).val('');
            $(objId+' #dropzone').text('클릭 또는 파일을 올려주세요.');
        }
    });
    return fileDropzone;
}
// dropzone set file
function setFileSaveOneDropzone(objId, pathId, path) {
    if(path) {
        $(objId+' #dropzone').empty();
        $(objId+' '+pathId).val('');
        if(path){
            $(objId+' '+pathId).val(path);
            path = path.replace(/(.+)[.]([a-z]+)$/g,"$1.x138.0.$2"),
            $(objId+' #dropzone').append('<div class="dz-details"><img class="thumbnail" data-dz-thumbnail src="'+path+'" style="max-width: 100%;max-height: 100%;"/><i class="fa fa-trash trash-icon"></i></div><div class="dz-progress"><span class="dz-upload" data-dz-uploadprogress></span></div><div class="dz-error-message"><span data-dz-errormessage></span></div>');
        }
    }
}

function deleteFileOneDropzone(objId, pathId){
    $(objId+' #dropzone').empty();
    $(objId+' '+pathId).val('');
}
/** //Dropzone 단일파일 업로드 **************************************************************************/

/**
 * objectId : dropzone이 생성되는 div ID
 * path : 파일 업로드 url
 * preview : 이미지 로드 후 보여지는 이미지 구성화면
 * inputData : 파일 업로드 후 값을 세팅하는 요소
 * formData : 파일 업로드시 추가로 보내는 정보
 */
function fileUpload (objectId, path, preview, inputData, option, formData){
    this.objectId = objectId;
    this.path = path;
    this.preview = preview;
    this.inputData = inputData;
    this.option = option;
    this.formData = formData;
    this.fileDropzone = null;

    this.init = function(){
        let max = this.option["maxFile"]?this.option["maxFile"]:9999;
        this.fileDropzone = new Dropzone('#'+this.objectId+" #dropzone", {
            url: this.path,
            previewTemplate: this.preview,
            maxFiles : max,
            init: function() {
                
            },
        });

        if(this.option["maxFile"]){
            this.fileDropzone.on('addedfile', (file)=>{
                if (this.fileDropzone.files.length > this.option["maxFile"]) {
                    this.fileDropzone.removeFile(this.fileDropzone.files[this.option["maxFile"]-1]);
                }
            });
            this.fileDropzone.maxFiles = this.option["maxFile"];
        }


        this.fileDropzone.on("complete", (file)=>{
            uploadInfo = JSON.parse(file.xhr.responseText)['result'];

            for (const [key, value] of Object.entries(this.inputData)){
                $('#'+this.objectId+' #'+key).val(uploadInfo[value]);
            }
            // 작업중 로딩 : 종료
            $(this.objectId+' #workingpopup').hide();
        });

        if(this.formData){
            this.fileDropzone.on('sending', (file, xhr, formData)=>{
                // 작업중 로딩 : 시작
                for (const [key, value] of Object.entries(this.inputData)){
                    formData.append(key, value);
                }
                $('#'+this.objectId+" #load-staus").empty().append('파일을 업로드중입니다.');
                $('#'+this.objectId+' #workingpopup').show();
            });
        }

        $('#'+this.objectId).on('click', '.trash-icon', (e)=>{
            $(e.target).closest('.dz-image-preview').remove();
            for (const [key, value] of Object.entries(this.inputData)){
                $('#'+this.objectId+' #'+key).val('');
            }
        })
    }

    this.fileAdd = function(path, fileData){
        path = path.replace(/(.+)[.]([a-z]+)$/g,"$1.x138.0.$2")

        let mockFile = {
            "thumb" : path
        };
        this.fileDropzone.files.push(mockFile);
        this.fileDropzone.emit('addedfile', mockFile);
        this.fileDropzone.emit('thumbnail', mockFile, mockFile.thumb);
        //this.fileDropzone.emit("complete", mockFile);

        for (const [key, value] of Object.entries(fileData)){
            $('#'+this.objectId+' #'+key).val(value);
        }
    }

    this.fileAllDelete = function(){
        $('#'+this.objectId+' #dropzone').empty();
        for (const [key, value] of Object.entries(this.inputData)){
            $('#'+this.objectId+' #'+key).val('');
        }
    }
};