<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL & ~E_DEPRECATED & ~E_NOTICE);

include_once("/webSiteSource/wcms/classes/autoload.php");

$db = new \Kodes\Wcms\DB();
$db->chageDatabase("api");

$nowWeek = date('w');
$nowTime = date("H:i");
$nowMinute = date("i");

if($_SERVER['argv'][1]==""){
	$curDate = strtotime(date("Y-m-d"));
}else{
	echo $_SERVER['argv'][1];
	$curDate = strtotime($_SERVER['argv'][1]);
}

$filter=[];

$filter['startTime']=['$lte'=>$nowTime];
$filter['endTime']=['$gte'=>$nowTime];

$options=[];
$data = $db->list("api", $filter, $options);

foreach($data as $val){
		echo "----- ".$val["id"]." ".$val['title']." -------------------\n";
		
		if(in_array($nowWeek, $val["week"]) && ( $val['efm']=="" || $nowMinute%$val['efm']==0 ) ){
			echo $val["id"]."\n";
			if($val['dateChar']!=""){
				eval('$inputDate ='.$val['dateChar'].";");
			}

			$url=str_replace(array('{date}','{key}'),array($inputDate ,$val['key']),$val['url']);
			
			$text = file_get_contents($url);
			$json = json_decode($text,true);

			if($val['listTag']!=""){
				eval('$json = $json'.$val['listTag'].';');
			}
			$allData=[];
			
			foreach($json as $jIdx => $jVal){					
				$jsonData=[];
				$dataFilter=[];

				foreach($val["items"] as $key2 =>$val2){				
					if(empty($val2['field'])){
						continue;
					}

					if(empty($val2['value'])){
						$jsonData[$val2['field']] = $json[$jIdx][$val2['tag']];
					} else {
						if(preg_match('/[\(][^\(]*[)]/',$val2['value'])){
							$val2['value']=str_replace('$this','"'.$json[$jIdx][$val2['tag']].'"',$val2['value']);
							eval('$runValue = '.$val2['value'].';');
							$jsonData[$val2['field']] = $runValue;
						}else{
							$jsonData[$val2['field']] = $val2['value'];
						}
					}
					
					if(!empty($val2['keyField'])){
						$dataFilter[$val2['field']] = $jsonData[$val2['field']];
					}
				}

				//print_r($dataFilter);
				$lastItemDate=$jsonData['DATE'];

				$db->upsert($val['collection'], $dataFilter, $jsonData);
				
				$allData[] = $jsonData;
				unset($jsonData,$dataFilter);
			}

			// record cache file
			$txtData = json_encode($allData);
			if(strlen($txtData) > 40){
				file_put_contents("/webData/api/list/api/".$val["id"].".json",json_encode($allData));
			}
			unset($allData);

			// record item update Date 
			$grpVal['lastItemDate']=$lastItemDate;
			$grpVal['lastItemUpdate']=date("Y-m-d H:i:s");
			$filter2["id"]=$val["id"];
			$db->upsert("api", $filter2, ['$set'=>$grpVal]);
		}
}
?>
